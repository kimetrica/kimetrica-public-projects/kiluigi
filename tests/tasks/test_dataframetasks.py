import datetime
import os
import unittest
from io import BytesIO
from unittest import skipUnless

import luigi
import pandas as pd
from pandas.testing import assert_frame_equal

from kiluigi.format import Feather, Parquet, Pickle
from kiluigi.targets import (
    CkanTarget,
    ExpiringS3Target,
    FinalTarget,
    IntermediateTarget,
    MemoryTarget,
)
from kiluigi.tasks import ReadDataFrameTask
from kiluigi.utils import with_config


class ReadDataFrameTestCase(unittest.TestCase):
    def setUp(self):
        self.df = pd.DataFrame(
            {"user": ["Bob", "Jane", "Alice"], "income": [40000, 50000, 42000]}
        )

    def tearDown(self):
        pass

    def create_dataframe(self, format=None):  # noqa: A002
        return luigi.LocalTarget(self.path, format=format)

    def test_open_by_name(self):
        target = luigi.LocalTarget(path="/tmp/dummy.csv")  # NOQA: S108
        self.df.to_csv(target.path, index=False)

        class TestTask(ReadDataFrameTask):
            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_open_csv_by_file(self):
        target = luigi.LocalTarget(path="/tmp/dummy.csv")  # NOQA: S108
        self.df.to_csv(target.path, index=False)

        class TestTask(ReadDataFrameTask):
            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_get_csv_string(self):
        target = MemoryTarget(name="test")
        target.put(self.df.to_csv(index=False))

        class TestTask(ReadDataFrameTask):

            read_method = "read_csv"

            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_get_csv_bytes(self):
        target = MemoryTarget(name="test")
        target.put(self.df.to_csv(index=False).encode())

        class TestTask(ReadDataFrameTask):

            read_method = "read_csv"

            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_open_excel_by_file(self):
        target = luigi.LocalTarget(path="/tmp/dummy.xlsx")  # NOQA: S108
        self.df.to_excel(target.path, index=False, engine="openpyxl")

        class TestTask(ReadDataFrameTask):
            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_get_excel_bytes(self):
        target = MemoryTarget(name="test")
        content = BytesIO()
        self.df.to_excel(content, index=False, engine="openpyxl")
        content.seek(0)
        target.put(content)

        class TestTask(ReadDataFrameTask):

            read_method = "read_excel"

            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    @skipUnless(
        os.environ.get("S3_TEST_BUCKET"),
        "S3_TEST_BUCKET environment variable is not set",
    )
    @with_config(
        {
            "s3": {"endpoint_url": os.environ.get("S3_ENDPOINT_URL")}
            if os.environ.get("S3_ENDPOINT_URL")
            else {}
        }
    )
    def test_get_csv_s3(self):
        target = IntermediateTarget(
            path="test.csv",
            backend_class=ExpiringS3Target,
            root_path=os.environ.get("S3_TEST_BUCKET"),
        )
        with target.temporary_path() as f:
            self.df.to_csv(f, index=False)

        class TestTask(ReadDataFrameTask):

            read_method = "read_csv"

            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    @skipUnless(
        os.environ.get("S3_TEST_BUCKET"),
        "S3_TEST_BUCKET environment variable is not set",
    )
    @with_config(
        {
            "s3": {"endpoint_url": os.environ.get("S3_ENDPOINT_URL")}
            if os.environ.get("S3_ENDPOINT_URL")
            else {}
        }
    )
    def test_get_excel_s3(self):
        target = IntermediateTarget(
            path="test.xlsx",
            backend_class=ExpiringS3Target,
            root_path=os.environ.get("S3_TEST_BUCKET"),
        )
        with target.temporary_path() as f:
            self.df.to_excel(f, index=False, engine="openpyxl")

        class TestTask(ReadDataFrameTask):

            read_method = "read_excel"

            def requires(self):
                return target

        task = TestTask()
        df = task.read_target(target)
        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    @skipUnless(
        os.environ.get("CKAN_TEST_ORGANIZATION"),
        "CKAN_TEST_ORGANIZATION environment variable is not set",
    )
    @with_config(
        {
            "ckan": {
                "token": os.environ.get("CKAN_TOKEN"),
            }
            if os.environ.get("CKAN_TOKEN")
            else {}
        }
    )
    def test_get_excel_ckan(self):

        target = FinalTarget(
            path="test.xlsx",
            backend_class=CkanTarget,
            root_path=os.path.join(
                os.environ.get("CKAN_TEST_ORGANIZATION", ""),
                os.environ.get("TOX_ENV_NAME", ""),
                self.id().split(".")[-1],
            ),
        )
        if target.exists():
            target.remove()

        try:
            with target.temporary_path() as f:
                self.df.to_excel(f, index=False, engine="openpyxl")

            class TestTask(ReadDataFrameTask):

                read_method = "read_excel"

                def requires(self):
                    return target

            task = TestTask()
            df = task.read_target(target)
            assert_frame_equal(
                df.reset_index(drop=True), self.df.reset_index(drop=True)
            )

            # Also test the legacy target definition
            resource = target.fs.get_resource_from_path(target.path)
            params = {
                "address": target.fs.client.address,
                "dataset": {"id": resource["package_id"]},
                "resource": {"id": resource["id"]},
            }
            legacy_target = CkanTarget(**params)

            class LegacyTestTask(ReadDataFrameTask):

                read_method = "read_excel"

                def requires(self):
                    return legacy_target

            task = LegacyTestTask()
            df = task.read_target(legacy_target)
            assert_frame_equal(
                df.reset_index(drop=True), self.df.reset_index(drop=True)
            )

        finally:
            if target.exists():
                target.remove()


class DataFrameFormatTestCase(unittest.TestCase):
    def setUp(self):
        self.df = pd.DataFrame(
            {
                "user": ["Bob", "Jane", "Alice"],
                "income": [40000, 50000, 42000],
                "date_of_birth": [
                    datetime.date(1970, 4, 1),
                    datetime.date(1980, 7, 1),
                    datetime.date(1995, 10, 1),
                ],
            }
        )

    def tearDown(self):
        pass

    def test_pickle(self):
        target = luigi.LocalTarget(
            path="/tmp/dummy.pickle", format=Pickle  # NOQA: S108
        )
        with target.open("w") as output:
            output.write(self.df)

        self.assertTrue(target.exists())

        with target.open() as input:
            df = input.read()

        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_feather(self):
        target = luigi.LocalTarget(
            path="/tmp/dummy.feather", format=Feather  # NOQA: S108
        )
        with target.open("w") as output:
            output.write(self.df)

        self.assertTrue(target.exists())

        with target.open() as input:
            df = input.read()

        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))

    def test_parquet(self):
        target = luigi.LocalTarget(
            path="/tmp/dummy.parquet", format=Parquet  # NOQA: S108
        )
        with target.open("w") as output:
            output.write(self.df)

        self.assertTrue(target.exists())

        with target.open() as input:
            df = input.read()

        assert_frame_equal(df.reset_index(drop=True), self.df.reset_index(drop=True))
