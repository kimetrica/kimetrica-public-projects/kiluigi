from unittest import mock
from unittest.case import skip

import luigi

from kiluigi.targets import MemoryTarget

from .test_base import BaseTargetTestCase, GetPutTargetTestMixin


class MemoryTargetTestCase(GetPutTargetTestMixin, BaseTargetTestCase):

    __test__ = True

    target_class = MemoryTarget

    def test_exists(self):
        t = self.create_target()
        p = t.open("w")
        self.assertEqual(t.exists(), self.path in t.fs._data)
        p.close()
        self.assertEqual(t.exists(), self.path in t.fs._data)

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_path_write(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_temporary_path_write(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_write_cleanup_no_close(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_atomicity_with_directory(self):
        pass

    @skip("Can't use Pathlib paths with MemoryTarget")
    def test_pathlib(self):
        pass

    @skip("Can't use formats that use subprocesses with MemoryTarget")
    def test_bzip2(self):
        pass

    @skip("Can't use formats that use subprocesses with MemoryTarget")
    def test_gzip(self):
        pass

    @skip("Can't use formats that use subprocesses with MemoryTarget")
    def test_gzip_with_module(self):
        pass

    @skip("Can't use formats that use subprocesses with MemoryTarget")
    def test_gzip_works_and_cleans_up(self):
        pass

    @skip("Can't use formats that use subprocesses with MemoryTarget")
    def test_format_chain(self):
        pass

    @skip("Can't use formats that use subprocesses with MemoryTarget")
    def test_format_chain_reverse(self):
        pass

    @mock.patch("os.linesep", "\r\n")
    def test_format_newline(self):
        t = self.target_class(self.path, luigi.format.SysNewLine)

        with t.open("w") as f:
            f.write(b"a\rb\nc\r\nd")

        with t.open("r") as f:
            b = f.read()

        # Get the data straight from the memory store, bypassing Formats
        c = t.fs.get_all_data()[t.path]

        self.assertEqual(b"a\nb\nc\nd", b)
        self.assertEqual(b"a\r\nb\r\nc\r\nd", c)
