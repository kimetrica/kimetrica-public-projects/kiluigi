"""
Unit tests for S3Target.

These tests are skipped unless there is an S3_TEST_BUCKET environment variable with a value like "s3://test-bucket".

The bucket must be accessible to the account specified by the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
environment variables used by boto3 to authenticate.

MinIO can be used as a substitute for S3 by setting the S3_ENDPOINT_URL to http://localhost:9000 and running a command
like:

    docker run -it --rm -p 9000:9000 -e MINIO_ROOT_USER=${AWS_ACCESS_KEY_ID} -e MINIO_ROOT_PASSWORD=${AWS_SECRET_ACCESS_KEY} minio/minio server /mnt/data --console-address ":9001"  # NOQA: E501

and then creating the bucket:

    aws s3 mb ${S3_TEST_BUCKET} --endpoint-url ${S3_ENDPOINT_URL}
"""

import datetime
import os
import time
from unittest import skip, skipUnless

from kiluigi.targets import TaggableS3Target
from kiluigi.utils import with_config

from .test_base import BaseTargetTestCase


@skipUnless(
    os.environ.get("S3_TEST_BUCKET"), "S3_TEST_BUCKET environment variable is not set"
)
@with_config(
    {
        "s3": (
            {
                "endpoint_url": os.environ.get("S3_ENDPOINT_URL"),
                "connect_timeout": "5",
                "max_retries": "0",
            }
            if os.environ.get("S3_ENDPOINT_URL")
            else {}
        )
    }
)
class TaggableS3TargetTestCase(BaseTargetTestCase):
    __test__ = True

    target_class = TaggableS3Target

    PATH_PREFIX = os.path.join(
        os.environ.get("S3_TEST_BUCKET", ""),
        os.environ.get("TOX_ENV_NAME", ""),
    )

    @skip("Can't use Pathlib paths with S3Target")
    def test_pathlib(self):
        pass

    @skip("Can't use S3Target for temporary-only files")
    def test_open_modes(self):
        pass

    def test_get_last_modified_date(self):
        dt = datetime.datetime.now(datetime.timezone.utc)
        time.sleep(1)
        t = self.create_target()
        if t.exists():
            t.remove()
        with t.open("w") as f:
            f.write("hej\n")
        self.assertTrue(t.exists())
        modified_date = t.last_modified_datetime
        self.assertTrue(modified_date > dt)
        time.sleep(1)
        with t.open("w") as f:
            f.write("我éçф")
        self.assertTrue(t.last_modified_datetime > modified_date)
