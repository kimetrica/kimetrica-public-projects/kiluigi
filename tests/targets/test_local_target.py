import datetime
import time

from kiluigi.targets.local_target import LocalTarget

from .test_base import BaseTargetTestCase


class LocalTargetTestCase(BaseTargetTestCase):
    __test__ = True

    target_class = LocalTarget

    def test_get_last_modified_date(self):
        dt = datetime.datetime.now(datetime.timezone.utc)
        time.sleep(1)
        t = self.create_target()
        if t.exists():
            t.remove()
        self.assertFalse(t.exists())
        with t.open("w") as f:
            f.write("hej\n")
        self.assertTrue(t.exists())
        modified_date = t.last_modified_datetime
        self.assertGreater(modified_date, dt)
        time.sleep(1)
        with t.open("w") as f:
            f.write("我éçф")
        self.assertGreater(t.last_modified_datetime, modified_date)
