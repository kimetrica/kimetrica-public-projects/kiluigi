import datetime
import math
import os
import platform
import unittest
from time import sleep
from unittest import skip, skipUnless

import luigi

from kiluigi.format import Pickle
from kiluigi.targets import (
    CkanTarget,
    ExpiringLocalTarget,
    ExpiringMemoryTarget,
    ExpiringS3Target,
    FinalTarget,
    GoogleDriveTarget,
    IntermediateTarget,
    LocalTarget,
)
from kiluigi.utils import with_config

from ..utils import FakeTask, TestException, timeout
from .test_base import BaseTargetTestCase


class UpstreamLocalTargetTest(unittest.TestCase):
    """
    A minimal version of the upstream LocalTargetTest to demonstrate that
    a vanilla LocalTarget does not work on Windows because when an exception
    is raised the temporary file cannot be removed.
    """

    PATH_PREFIX = "test_file_or_dir"

    def setUp(self):
        self.path = self.PATH_PREFIX + "-" + str(self.id())
        if os.path.exists(self.path):
            os.remove(self.path)

    def tearDown(self):
        if os.path.exists(self.path):
            os.remove(self.path)

    def create_target(self, format=None):  # noqa: A002
        return LocalTarget(self.path, format=format)

    def assertCleanUp(self, tmp_path=""):
        self.assertFalse(os.path.exists(tmp_path))

    @skipUnless(
        platform.system() == "Windows",
        "only demonstrate LocalTarget failure on Windows",
    )
    def test_with_exception(self):
        target = self.create_target()

        a = {}

        def foo():
            with target.open("w") as fobj:
                fobj.write("hej\n")
                a["tp"] = getattr(fobj, "tmp_path", "")
                raise TestException("Test triggered exception")

        self.assertRaises(TestException, foo)
        # The temporary file should have been removed by AtomicLocalFile.__del__
        # but in fact it won't have been on Windows, because the call to `os.remove(self.tmp_path)`
        # will have generated another exception:
        #     PermissionError: [WinError 32] The process cannot access the file because it is being used by another process  # NOQA:E501
        with self.assertRaises(AssertionError):
            self.assertCleanUp(a["tp"])
        # Remove the dangling temporary file
        os.remove(a["tp"])
        self.assertFalse(target.exists())


class LocalTargetFinalTargetTestCase(BaseTargetTestCase):

    __test__ = True

    frontend_class = FinalTarget
    backend_class = LocalTarget

    def create_target(self, path=None, format=None):  # noqa: A002
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        return self.frontend_class(
            path=path, format=format, backend_class=self.backend_class
        )

    def create_directory_target(self, path=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        if path[-1] != os.path.sep:
            path += os.path.sep
        return self.frontend_class(path=path, backend_class=self.backend_class)

    def remove_target(self, path):
        target = self.create_target(path)
        return target._backend.fs.remove(target.path)

    def target_exists(self, path):
        target = self.create_target(path)
        return target._backend.fs.exists(target.path)

    @skip("Not supported by Delegating Target")
    def test_exists(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_pathlib(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_del(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_write_cleanup_no_close(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_text(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_format_newline(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_del_with_Text(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_format_injection(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_binary_write(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_writelines(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_read_iterator(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_gzip(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_gzip_works_and_cleans_up(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_bzip2(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_gzip_with_module(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_copy(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_move(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_move_on_fs(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_rename_dont_move_on_fs(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_format_chain(self):
        pass

    @skip("Not supported by Delegating Target")
    def test_format_chain_reverse(self):
        pass

    @skip("Can't use Delegating Targets for temporary-only files")
    def test_open_modes(self):
        pass

    def test_relative_path(self):
        with self.assertRaises(AssertionError):
            self.frontend_class(path=os.path.abspath("."))

    def test_posix_path(self):
        with self.assertRaises(AssertionError):
            self.frontend_class(path=r"mydir\myfile.txt")

    def test_implicit_binary_write(self):
        t = self.create_target()
        with t.open("w") as f:
            f.write(b"a\xf2\xf3\r\nfd")

        with t.open("r") as f:
            c = f.read()

        self.assertEqual(c, b"a\xf2\xf3\r\nfd")

    def test_pickle(self):
        t = self.create_target(format=Pickle)
        with t.open("w") as f:
            data = {"item1": ["a", "b", "c"], "item2": luigi.Task()}
            f.write(data)

        with t.open("r") as f:
            data = f.read()

        self.assertTrue(isinstance(data, dict))
        self.assertTrue(isinstance(data["item2"], luigi.Task))

    def test_pickle_default_extension(self):
        target = self.frontend_class(
            path="dummy", format=Pickle, backend_class=self.backend_class, root_path="/"
        )
        self.assertEqual(target.path, "/dummy.pickle")

    def test_default_format(self):
        target = self.frontend_class(
            path="dummy", backend_class=self.backend_class, root_path="/"
        )
        self.assertEqual(target.format, Pickle)

        default_formats = {
            luigi.format.Text: [".log", ".csv", ".json", ".geojson", ".txt"],
            luigi.format.Nop: [
                ".dbf",
                ".hdf",
                ".jpg",
                ".jpeg",
                ".pdf",
                ".shp",
                ".tif",
                ".tiff",
                ".xls",
                ".xlsx",
                ".zip",
            ],
        }
        for expected_format, extensions in default_formats.items():
            for ext in extensions:
                target = self.frontend_class(
                    path=f"dummy{ext}", backend_class=self.backend_class, root_path="/"
                )
                self.assertEqual(
                    target.format,
                    expected_format,
                    f"Target {target.path} has format {target.format} instead of {expected_format}",
                )

    def test_task_without_path(self):
        target1 = self.frontend_class(
            task=FakeTask(param=1), backend_class=self.backend_class, root_path="output"
        )
        target2 = self.frontend_class(
            task=FakeTask(param=2), backend_class=self.backend_class, root_path="output"
        )
        self.assertNotEqual(target1.path, target2.path)
        self.assertEqual(target1.path.split(os.path.sep)[-2], "FakeTask")
        self.assertRegex(
            target1.path.split(os.path.sep)[-1],
            r"FakeTask_1_[a-z0-93]{10}.pickle",
            "Task must not obscure file name",
        )

    def test_task_and_path(self):
        target1 = self.frontend_class(
            path="dummy.csv",
            task=FakeTask(param=1),
            backend_class=self.backend_class,
            root_path="output",
        )
        target2 = self.frontend_class(
            path="dummy.csv",
            task=FakeTask(param=2),
            backend_class=self.backend_class,
            root_path="output",
        )
        self.assertNotEqual(
            target1.path,
            target2.path,
            "Paths must be distinct for different Task instances",
        )
        self.assertEqual(
            os.path.splitext(target1.path)[1],
            ".csv",
            "Targets with Tasks must preserve the file extension",
        )
        self.assertRegex(
            target1.path.split(os.path.sep)[-1],
            r"dummy_[a-z0-93]{10}.csv",
            "Task must not obscure file name",
        )

    def test_path_without_task(self):
        # If the user passes in a path and doesn't pass in a task
        # then we assume that is a deliberate choice and we can
        # reuse the Target
        target1 = self.frontend_class(
            path="dummy.csv", backend_class=self.backend_class, root_path="/"
        )
        target2 = self.frontend_class(
            path="dummy.csv", backend_class=self.backend_class, root_path="/"
        )
        self.assertEqual(target1.path, target2.path)


class ExpiringLocalTargetIntermediateTargetTestCase(LocalTargetFinalTargetTestCase):

    frontend_class = IntermediateTarget
    backend_class = ExpiringLocalTarget

    # Timeout must be set long enough that the initial call to exists()
    # after we write the Target returns before the timeout expires the Target;
    timeout = 1

    # And wait must be set long enough that the second call to exists()
    # will be after the timeout on the Target has expired
    wait = 3

    def create_target(self, path=None, format=None, timeout=None):  # noqa: A002
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        return self.frontend_class(
            path=path,
            format=format,
            timeout=timeout,
            backend_class=self.backend_class,
        )

    def create_directory_target(self, path=None, timeout=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        if path[-1] != os.path.sep:
            path += os.path.sep
        return self.frontend_class(
            path=path, timeout=timeout, backend_class=self.backend_class
        )

    def test_expiry(self):
        target = self.create_target(timeout=self.timeout)
        with target.open("w") as fobj:
            tp = getattr(fobj, "tmp_path", "")
            fobj.write("hej\n")
        self.assertTrue(target.exists())
        try:
            with timeout(seconds=self.wait):
                while target.exists():
                    sleep(1)
        except TimeoutError:
            self.fail(f"Target '{target.path}' still exists after {self.wait} seconds")
        self.assertCleanUp(tp)

    def test_expiry_with_directory(self):
        target = self.create_target(path="expiring_directory/", timeout=self.timeout)
        if target.exists():
            target.remove()
        with target.temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            with open(os.path.join(tmpdir, "dummy1.txt"), "w") as fobj:
                fobj.write("lol1\n")
        # Wait so that non-ACID remote filesystems (e.g. S3) can catch up
        try:
            with timeout(seconds=self.timeout):
                while not target.exists():
                    sleep(1)
        except TimeoutError:
            self.fail(f"Target '{target.path}' did not exist after {self.wait} seconds")
        try:
            with timeout(seconds=self.wait):
                while target.exists():
                    sleep(1)
        except TimeoutError:
            self.fail(f"Target '{target.path}' still exists after {self.wait} seconds")

    def test_prune(self):
        target = self.create_target(self.path + "2", timeout=self.timeout)
        with target.open("w") as fobj:
            fobj.write("hej\n")
        self.assertTrue(target.exists())
        # Wait until the target has expired
        wait = (
            math.ceil(
                (
                    target.get_expiry() - datetime.datetime.now(datetime.timezone.utc)
                ).total_seconds()
            )
            + 1
        )
        sleep(wait)
        new_target = self.create_target()
        with new_target.open("w") as fobj:
            fobj.write("hej\n")
        self.assertTrue(new_target.exists())
        # Also check that the expired_target has been removed
        self.assertCleanUp(target)

    def test_prune_with_directory(self):
        target = self.create_target(path="expired_directory/", timeout=self.timeout)
        if target.exists():
            target.remove()
        with target.temporary_path() as tmpdir:
            os.makedirs(tmpdir)
            with open(os.path.join(tmpdir, "dummy1.txt"), "w") as fobj:
                fobj.write("lol1\n")
        # Wait so that non-ACID remote filesystems (e.g. S3) can catch up
        try:
            with timeout(seconds=self.timeout):
                while not target.exists():
                    sleep(1)
        except TimeoutError:
            self.fail(f"Target '{target.path}' did not exist after {self.wait} seconds")
        # Wait until the target has expired
        wait = (
            math.ceil(
                (
                    target.get_expiry() - datetime.datetime.now(datetime.timezone.utc)
                ).total_seconds()
            )
            + 1
        )
        sleep(wait)
        new_target = self.create_target()
        with new_target.open("w") as fobj:
            fobj.write("hej\n")
        self.assertTrue(new_target.exists())
        # Also check that the expired_target has been removed
        self.assertFalse(target.exists())
        self.assertCleanUp(target)
        self.assertCleanUp(self.create_target(path="expired_directory/dummy1.txt"))


class ExpiringMemoryTargetIntermediateTargetTestCase(
    ExpiringLocalTargetIntermediateTargetTestCase
):

    frontend_class = IntermediateTarget
    backend_class = ExpiringMemoryTarget

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_path_write(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_temporary_path_write(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_atomicity_with_directory(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_expiry_with_directory(self):
        pass

    @skip("MemoryTarget doesn't write any files to the filesystem")
    def test_prune_with_directory(self):
        pass


@skipUnless(
    os.environ.get("S3_TEST_BUCKET"), "S3_TEST_BUCKET environment variable is not set"
)
@with_config(
    {
        "s3": {
            "endpoint_url": os.environ.get("S3_ENDPOINT_URL"),
            "connect_timeout": "5",
            "max_retries": "0",
        }
        if os.environ.get("S3_ENDPOINT_URL")
        else {}
    }
)
class ExpiringS3TargetIntermediateTargetTestCase(
    ExpiringLocalTargetIntermediateTargetTestCase
):

    frontend_class = IntermediateTarget
    backend_class = ExpiringS3Target

    # Timeout must be set long enough that the initial call to exists()
    # after we write the Target returns before the timeout expires the Target
    # For S3 this will be longer than normal because of the network round trips
    # to upload the file and check the exists and the metadata.
    timeout = 6

    # And wait must be set long enough that the second call to exists()
    # will be after the timeout on the Target has expired
    wait = 15

    def create_target(self, path=None, format=None, timeout=None):  # noqa: A002
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )

        return self.frontend_class(
            path=path,
            format=format,
            timeout=timeout,
            backend_class=self.backend_class,
            root_path=os.path.join(
                os.environ.get("S3_TEST_BUCKET"), os.environ.get("TOX_ENV_NAME", "")
            ),
        )

    def create_directory_target(self, path=None, timeout=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        if path[-1] != os.path.sep:
            path += os.path.sep
        return self.frontend_class(
            path=path,
            timeout=timeout,
            backend_class=self.backend_class,
            root_path=os.path.join(
                os.environ.get("S3_TEST_BUCKET"), os.environ.get("TOX_ENV_NAME", "")
            ),
        )

    def test_flag_file(self):
        target = self.create_target(format=luigi.format.Nop)
        with self.assertRaises(AssertionError):
            with target.temporary_path() as tmpdir:
                os.makedirs(tmpdir)
                with open(os.path.join(tmpdir, "dummy1.txt"), "w") as fobj:
                    fobj.write("lol1\n")


@skipUnless(
    os.environ.get("CKAN_TEST_ORGANIZATION"),
    "CKAN_TEST_ORGANIZATION environment variable is not set",
)
@with_config(
    {
        "ckan": {
            "token": os.environ.get("CKAN_TOKEN"),
        }
        if os.environ.get("CKAN_TOKEN")
        else {}
    }
)
class CkanTargetFinalTargetTestCase(LocalTargetFinalTargetTestCase):

    frontend_class = FinalTarget
    backend_class = CkanTarget

    def create_target(self, path=None, format=None, timeout=None):  # noqa: A002
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        return self.frontend_class(
            path=path,
            format=format,
            backend_class=self.backend_class,
            root_path=os.path.join(
                os.environ.get("CKAN_TEST_ORGANIZATION", ""),
                os.environ.get("TOX_ENV_NAME", ""),
            ),
        )

    def create_directory_target(self, path=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        if path[-1] != os.path.sep:
            path += os.path.sep
        return self.frontend_class(
            path=path,
            backend_class=self.backend_class,
            root_path=os.path.join(
                os.environ.get("CKAN_TEST_ORGANIZATION", ""),
                os.environ.get("TOX_ENV_NAME", ""),
            ),
        )


@skipUnless(
    os.environ.get("GOOGLE_DRIVE_TEST_FOLDER"),
    "GOOGLE_DRIVE_TEST_FOLDER environment variable is not set",
)
@with_config(
    {
        # Need to double and % characters to avoid ValueError("invalid interpolation syntax") from ConfigParser
        "google_drive": {
            "oauth_credentials": os.environ.get("GOOGLE_CREDENTIALS").replace("%", "%%")
        }
        if os.environ.get("GOOGLE_CREDENTIALS")
        else {}
    }
)
class GoogleDriveTargetFinalTargetTestCase(LocalTargetFinalTargetTestCase):

    frontend_class = FinalTarget
    backend_class = GoogleDriveTarget

    def create_target(self, path=None, format=None, timeout=None):  # noqa: A002
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        return self.frontend_class(
            path=path,
            format=format,
            backend_class=self.backend_class,
            root_path=os.environ.get("GOOGLE_DRIVE_TEST_FOLDER"),
        )

    def create_directory_target(self, path=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        if path[-1] != os.path.sep:
            path += os.path.sep
        return self.frontend_class(
            path=path,
            backend_class=self.backend_class,
            root_path=os.environ.get("GOOGLE_DRIVE_TEST_FOLDER"),
        )
