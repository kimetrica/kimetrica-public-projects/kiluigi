"""
Unit tests for GoogleDriveTarget.

These tests are skipped unless there is an GOOGLE_DRIVE_TEST_FOLDER environment variable containing the id for a valid
Google Drive folder, e.g. 1uzTGwPe1cirJeFWszx1y1FcpaI8QGOFY

The folder must be accessible to the service account specified by the GOOGLE_CREDENTIALS environment variable.

"""
import datetime
import os
import time
from unittest import skip, skipUnless

import luigi
import pandas as pd

from kiluigi.targets import GoogleDriveTarget
from kiluigi.utils import with_config

from .test_base import BaseTargetTestCase


@skipUnless(
    os.environ.get("GOOGLE_DRIVE_TEST_FOLDER"),
    "GOOGLE_DRIVE_TEST_FOLDER environment variable is not set",
)
@with_config(
    {
        # Need to double and % characters to avoid ValueError("invalid interpolation syntax") from ConfigParser
        "google_drive": {
            "oauth_credentials": os.environ.get("GOOGLE_CREDENTIALS").replace("%", "%%")
        }
        if os.environ.get("GOOGLE_CREDENTIALS")
        else {}
    }
)
class GoogleDriveTargetTestCase(BaseTargetTestCase):

    __test__ = True

    target_class = GoogleDriveTarget

    PATH_PREFIX = os.path.join(
        os.environ.get("GOOGLE_DRIVE_TEST_FOLDER", ""),
        os.environ.get("TOX_ENV_NAME", ""),
    )

    @skip("Can't use Pathlib paths with GoogleDriveTarget")
    def test_pathlib(self):
        pass

    @skip("Can't use GoogleDriveTarget for temporary-only files")
    def test_open_modes(self):
        pass

    def test_with_file_id(self):
        t = self.create_target()
        if t.exists():
            t.remove()

        with t.open("w") as f:
            f.write("hej\n")

        self.assertTrue(t.exists())

        # Access the file with a target path that points directly to the file,
        # rather than a containing folder and name.
        file_id = t.fs.get_id_from_path(t.path)
        t = self.target_class(file_id)
        self.assertTrue(t.exists())
        a = "我éçф"
        with t.open("w") as f:
            f.write(a)
        with t.open("r") as f:
            b = f.read()
        self.assertEqual(a, b)

    def test_as_google_doc(self):
        t = self.target_class(
            self.path,
            format=luigi.format.Nop,
            mimetype="application/x-vnd.oasis.opendocument.spreadsheet",
            as_google_doc=True,
        )
        if t.exists():
            t.remove()

        df = pd.DataFrame(
            {"user": ["Bob", "Jane", "Alice"], "income": [40000, 50000, 42000]}
        )

        with t.open("w") as f:
            df.to_excel(f, index=False, engine="odf")
        self.assertTrue(t.exists())

        # Check that the file was uploaded as a Google Sheet
        file_id = t.fs.get_id_from_path(t.path)
        file_obj = t.fs.client.files().get(fileId=file_id).execute()
        self.assertTrue(
            file_obj.get("mimeType"), "application/vnd.google-apps.spreadsheet"
        )

        # Check that the file downloads as a spreadsheet
        with t.open() as f:
            df = pd.read_excel(f, engine="odf")
        self.assertEqual(len(df), 3)

        # Check that an existing file can be updated
        df = df.drop([0])
        with t.open("w") as f:
            df.to_excel(f, index=False, engine="odf")
        self.assertTrue(t.exists())
        file_id = t.fs.get_id_from_path(t.path)
        file_obj = t.fs.client.files().get(fileId=file_id).execute()
        self.assertTrue(
            file_obj.get("mimeType"), "application/vnd.google-apps.spreadsheet"
        )
        with t.open() as f:
            df = pd.read_excel(f, engine="odf")
        self.assertEqual(len(df), 2)

    def test_excel(self):
        t = self.target_class(
            self.path,
            format=luigi.format.Nop,
            mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            as_google_doc=True,
        )
        if t.exists():
            t.remove()

        df = pd.DataFrame(
            {"user": ["Bob", "Jane", "Alice"], "income": [40000, 50000, 42000]}
        )

        with t.open("w") as f:
            df.to_excel(f, index=False, engine="openpyxl")
        self.assertTrue(t.exists())

        # Check that the file was uploaded as a Google Sheet
        file_id = t.fs.get_id_from_path(t.path)
        file_obj = t.fs.client.files().get(fileId=file_id).execute()
        self.assertTrue(
            file_obj.get("mimeType"), "application/vnd.google-apps.spreadsheet"
        )

        # Check that the file downloads as a spreadsheet, even without specifying as_google_doc
        t = self.target_class(
            self.path,
            format=luigi.format.Nop,
            mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        )
        with t.open() as f:
            # @TODO remove engine specification after we drop support for Pandas 1.1
            df = pd.read_excel(f, engine="openpyxl")
        self.assertEqual(len(df), 3)

        # Check that the same target definition can also download an actual Excel file
        t.remove()
        df = df.drop([0])
        with t.open("w") as f:
            df.to_excel(f, index=False, engine="openpyxl")
        self.assertTrue(t.exists())
        with t.open() as f:
            # @TODO remove engine specification after we drop support for Pandas 1.1
            df = pd.read_excel(f, engine="openpyxl")
        self.assertEqual(len(df), 2)

    def test_get_last_modified_date(self):
        dt = datetime.datetime.now(datetime.timezone.utc)
        time.sleep(1)
        t = self.create_target()
        if t.exists():
            t.remove()

        with t.open("w") as f:
            f.write("hej\n")

        self.assertTrue(t.exists())
        modified_date = t.last_modified_datetime
        self.assertTrue(modified_date > dt)
        time.sleep(1)
        with t.open("w") as f:
            f.write("我éçф")

        self.assertTrue(t.last_modified_datetime > modified_date)
