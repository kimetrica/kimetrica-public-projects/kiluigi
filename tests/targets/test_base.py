import bz2
import gzip
import io
import itertools
import os
import platform
import random
import unittest
from errno import EEXIST
from unittest import mock, skipIf

import luigi
import pandas as pd
from luigi import LocalTarget

from ..utils import FakeTask, TestException


class FileSystemTargetTestMixin:
    """
    All Target that take bytes should pass these tests.

    In addition, a test to verify the method `exists` should be added to subclasses

    Copied from luigi/test/target_test.py
    """

    def create_target(self, path=None, format=None):
        raise NotImplementedError

    def create_directory_target(self, path=None):
        raise NotImplementedError

    def assertCleanUp(self, tmp_path=""):
        pass

    def test_atomicity(self):
        target = self.create_target()
        self.assertFalse(target.exists())
        fobj = target.open("w")
        self.assertFalse(target.exists())
        fobj.write("lol\n")
        self.assertFalse(target.exists())
        fobj.close()
        self.assertTrue(target.exists())

    def test_readback(self):
        target = self.create_target()
        origdata = "lol\n"
        fobj = target.open("w")
        fobj.write(origdata)
        fobj.close()

        fobj = target.open("r")
        data = fobj.read()
        fobj.close()
        self.assertEqual(origdata, data)

    def test_unicode_obj(self):
        target = self.create_target()

        origdata = "lol\n"
        fobj = target.open("w")
        fobj.write(origdata)
        fobj.close()

        fobj = target.open("r")
        data = fobj.read()
        fobj.close()
        self.assertEqual(origdata, data)

    def test_with_close(self):
        target = self.create_target()
        if target.exists():
            target.remove()

        with target.open("w") as fobj:
            tp = getattr(fobj, "tmp_path", "")
            fobj.write("hej\n")

        self.assertCleanUp(tp)
        self.assertTrue(target.exists())

    @skipIf(
        platform.system() == "Windows", "temporary files are not deleted on Windows"
    )
    def test_with_exception(self):
        target = self.create_target()

        a = {}

        def foo():
            with target.open("w") as fobj:
                fobj.write("hej\n")
                a["tp"] = getattr(fobj, "tmp_path", "")
                raise TestException("Test triggered exception")

        self.assertRaises(TestException, foo)
        self.assertCleanUp(a["tp"])
        self.assertFalse(target.exists())

    def test_del(self):
        t = self.create_target()
        p = t.open("w")
        print("test", file=p)
        tp = getattr(p, "tmp_path", "")
        del p

        self.assertCleanUp(tp)
        self.assertFalse(t.exists())

    def test_write_cleanup_no_close(self):
        t = self.create_target()

        def context():
            f = t.open("w")
            f.write("stuff")
            return getattr(f, "tmp_path", "")

        tp = context()
        import gc

        gc.collect()  # force garbage collection of f variable
        self.assertCleanUp(tp)
        self.assertFalse(t.exists())

    def test_text(self):
        t = self.create_target(format=luigi.format.UTF8)
        a = "我éçф"
        with t.open("w") as f:
            f.write(a)
        with t.open("r") as f:
            b = f.read()
        self.assertEqual(a, b)

    def test_del_with_Text(self):
        t = self.create_target(format=luigi.format.UTF8)
        p = t.open("w")
        print("test", file=p)
        tp = getattr(p, "tmp_path", "")
        del p

        self.assertCleanUp(tp)
        self.assertFalse(t.exists())

    def test_format_injection(self):
        class CustomFormat(luigi.format.Format):
            def pipe_reader(self, input_pipe):
                input_pipe.foo = "custom read property"
                return input_pipe

            def pipe_writer(self, output_pipe):
                output_pipe.foo = "custom write property"
                return output_pipe

        t = self.create_target(format=CustomFormat())
        with t.open("w") as f:
            self.assertEqual(f.foo, "custom write property")

        with t.open("r") as f:
            self.assertEqual(f.foo, "custom read property")

    # @skipOnTravisAndGithubActions('https://travis-ci.org/spotify/luigi/jobs/73693470')
    def test_binary_write(self):
        t = self.create_target(format=luigi.format.Nop)
        with t.open("w") as f:
            f.write(b"a\xf2\xf3\r\nfd")

        with t.open("r") as f:
            c = f.read()

        self.assertEqual(c, b"a\xf2\xf3\r\nfd")

    def test_writelines(self):
        t = self.create_target()
        with t.open("w") as f:
            f.writelines(["a\n", "b\n", "c\n"])

        with t.open("r") as f:
            c = f.read()

        self.assertEqual(c, "a\nb\nc\n")

    def test_read_iterator(self):
        t = self.create_target()
        with t.open("w") as f:
            f.write("a\nb\nc\n")

        c = []
        with t.open("r") as f:
            for x in f:
                c.append(x)

        self.assertEqual(c, ["a\n", "b\n", "c\n"])

    def test_gzip(self):
        t = self.create_target(format=luigi.format.Gzip)
        if t.exists():
            t.remove()
        p = t.open("w")
        test_data = b"test"
        p.write(test_data)
        tp = getattr(p, "tmp_path", "")
        self.assertFalse(t.exists())
        p.close()
        self.assertCleanUp(tp)
        self.assertTrue(t.exists())

    def test_gzip_works_and_cleans_up(self):
        t = self.create_target(format=luigi.format.Gzip)
        if t.exists():
            t.remove()

        test_data = b"123testing"
        with t.open("w") as f:
            tp = getattr(f, "tmp_path", "")
            f.write(test_data)

        self.assertCleanUp(tp)
        with t.open() as f:
            result = f.read()

        self.assertEqual(test_data, result)

    def test_move_on_fs(self):
        # We're cheating and retrieving the fs from target.
        # TODO: maybe move to "filesystem_test.py" or something
        t = self.create_target()
        other_path = t.path + "-" + str(random.randint(0, 999999999))  # NOQA: S311
        t._touchz()
        fs = t.fs
        self.assertTrue(t.exists())
        fs.move(t.path, other_path)
        self.assertFalse(t.exists())
        self.assertTrue(t.fs.exists(other_path))
        t.fs.remove(other_path)
        self.assertFalse(t.fs.exists(other_path))

    def test_rename_dont_move_on_fs(self):
        # We're cheating and retrieving the fs from target.
        # TODO: maybe move to "filesystem_test.py" or something
        t = self.create_target()
        other_path = t.path + "-" + str(random.randint(0, 999999999))  # NOQA: S311
        t._touchz()
        fs = t.fs
        self.assertTrue(t.exists())
        fs.rename_dont_move(t.path, other_path)
        self.assertFalse(t.exists())
        self.assertTrue(t.fs.exists(other_path))
        self.assertRaises(
            luigi.target.FileAlreadyExists,
            lambda: fs.rename_dont_move(t.path, other_path),
        )
        t.fs.remove(other_path)
        self.assertFalse(t.fs.exists(other_path))

    def test_explicit_binary_write(self):
        t = self.create_target(path="dummy.dbf", format=luigi.format.Nop)
        if t.exists():
            t.remove()
        with t.open("w") as f:
            f.write(b"a\xf2\xf3\r\nfd")

        with t.open("r") as f:
            c = f.read()

        self.assertEqual(c, b"a\xf2\xf3\r\nfd")
        t.remove()

    def test_path_write(self):
        t = self.create_target(path="dummy.xlsx", format=luigi.format.Nop)
        if t.exists():
            t.remove()
        data = [10, 20, 30, 20, 15, 30, 45]
        df = pd.DataFrame({"Data": data})
        with t.open("w") as f:
            tp = getattr(f, "tmp_path", "")
            # Some libraries do not accept a buffer as an output destination
            # and insist on receiving a path/filename. Recent ExcelWriter
            # versions no longer suffer from this problem, but the test is
            # still valid because ExcelWriter is never given the file handle
            writer = pd.ExcelWriter(f.tmp_path, engine="xlsxwriter")
            df.to_excel(writer, sheet_name="Sheet1")
            writer.close()

        self.assertTrue(t.exists())
        self.assertCleanUp(tp)

        with t.open("r") as f:
            try:
                df = pd.read_excel(
                    f,
                    engine="openpyxl",  # Force openpyxl because pandas<1.2.0 incorrectly uses xlrd
                )
            except AttributeError:
                # Pandas uses seek(0) on the file, but ReadableS3File isn't seekable.
                # This workaround may not be required on later versions of Pandas
                df = pd.read_excel(
                    f.read(),
                    engine="openpyxl",  # Force openpyxl because pandas<1.2.0 incorrectly uses xlrd
                )

        self.assertEqual(df["Data"].tolist(), data)
        t.remove()

    def test_temporary_path_write(self):
        t = self.create_target("dummy.txt")
        if t.exists():
            t.remove()
        origdata = "lol\n"
        with t.temporary_path() as tmpfile:
            # Some libraries do not accept a buffer as an output destination
            # and insist on receiving a path/filename.
            with open(tmpfile, "w") as f:
                f.write(origdata)
            tp = tmpfile

        self.assertTrue(t.exists())
        self.assertCleanUp(tp)
        with t.open() as fobj:
            data = fobj.read()
            self.assertEqual(origdata, data)
        t.remove()

    def test_atomicity_with_directory(self):
        # temporary_path is the preferred way to ensure atomic writes where the Target is a directory containing
        # multiple files
        t = self.create_directory_target()
        if t.exists():
            t.remove()
        self.assertFalse(t.exists())

        with t.temporary_path() as temp_output_path:
            os.makedirs(os.path.join(temp_output_path, "subfolder"))
            with open(os.path.join(temp_output_path, "subfolder", "1.txt"), "w") as f:
                f.write("File 1")
            with open(os.path.join(temp_output_path, "2.txt"), "w") as f:
                f.write("File 2")
            self.assertFalse(t.exists())

        self.assertTrue(t.exists())
        self.assertTrue(t.fs.exists(os.path.join(t.path, "subfolder", "1.txt")))
        self.assertFalse(t.fs.exists(os.path.join(t.path, "1.txt")))
        self.assertTrue(t.fs.exists(os.path.join(t.path, "2.txt")))
        self.assertFalse(os.path.exists(temp_output_path))

        with self.create_target(os.path.join("subfolder", "1.txt")).open() as f:
            contents = f.read()
            self.assertEqual(contents, "File 1")
        with self.create_target("2.txt").open() as f:
            contents = f.read()
            self.assertEqual(contents, "File 2")

        t.remove()
        self.assertFalse(t.exists())


class BaseTargetTestCase(unittest.TestCase, FileSystemTargetTestMixin):
    """
    Based on luigi/test/local_target_test.py
    """

    __test__ = False  # Must be set to True on subclasses.

    PATH_PREFIX = os.environ.get("TOX_ENV_NAME", ".")

    target_class = None  # Subclasses must set this
    directory_target_class = None  # Subclasses must set this

    def setUp(self):
        self.path = os.path.join(
            self.PATH_PREFIX,
            self.__class__.__name__,
            self.id().split(".")[-1],
            "test_file_or_dir",
        )
        if self.target_exists(self.path):
            self.remove_target(self.path)

    def tearDown(self):
        if self.target_exists(self.path):
            self.remove_target(self.path)

    def create_target(self, path=None, format=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        return self.target_class(path, format=format)

    def create_directory_target(self, path=None):
        path = (
            os.path.join(self.path, path) if path and path != self.path else self.path
        )
        if path[-1] != os.path.sep:
            path += os.path.sep
        return self.target_class(path)

    def remove_target(self, path):
        self.create_target(path).fs.remove(path)

    def target_exists(self, path):
        return self.create_target(path).fs.exists(path)

    def assertCleanUp(self, tmp_path=""):
        """
        Check that the file has been removed
        """
        if isinstance(tmp_path, luigi.Target):
            # Check that the file is actually removed from the filesystem,
            # and not just that that Target will report that it doesn't exist
            self.assertFalse(
                tmp_path.fs.exists(tmp_path.path),
                f"File '{tmp_path.path}' still exists",
            )
        else:
            self.assertFalse(
                os.path.exists(tmp_path), f"File '{tmp_path}' still exists"
            )

    def test_exists(self):
        t = self.create_target()
        p = t.open("w")
        self.assertEqual(t.exists(), self.target_exists(self.path))
        p.close()
        self.assertEqual(t.exists(), self.target_exists(self.path))

    def test_pathlib(self):
        """Test work with pathlib.Path"""
        import pathlib

        path = pathlib.Path(self.path)
        self.assertFalse(path.exists())
        target = self.create_target(path)
        self.assertFalse(target.exists())
        with target.open("w") as stream:
            stream.write("test me")
        self.assertTrue(target.exists())

    def test_gzip_with_module(self):
        t = self.create_target(format=luigi.format.Gzip)
        if t.exists():
            t.remove()
        p = t.open("w")
        test_data = b"test"
        p.write(test_data)
        self.assertFalse(t.exists())
        p.close()
        self.assertTrue(t.exists())

        # Using gzip module as validation
        try:
            f = gzip.open(self.path, "r")
            self.assertTrue(test_data == f.read())
            f.close()
        except FileNotFoundError:
            # Subclasses may not have a local file that can be accessed
            pass

        # Verifying our own gzip reader
        f = self.create_target(format=luigi.format.Gzip).open("r")
        self.assertTrue(test_data == f.read())
        f.close()

    def test_bzip2(self):
        t = self.create_target(format=luigi.format.Bzip2)
        if t.exists():
            t.remove()
        p = t.open("w")
        test_data = b"test"
        p.write(test_data)
        self.assertFalse(t.exists())
        p.close()
        self.assertTrue(t.exists())

        # Using bzip module as validation
        try:
            f = bz2.BZ2File(self.path, "r")
            self.assertTrue(test_data == f.read())
            f.close()
        except FileNotFoundError:
            # Subclasses may not have a local file that can be accessed
            pass

        # Verifying our own bzip2 reader
        f = self.create_target(format=luigi.format.Bzip2).open("r")
        self.assertTrue(test_data == f.read())
        f.close()

    def test_copy(self):
        t = self.create_target("original")
        if t.exists():
            t.remove()
        c = self.create_target("copy")
        if c.exists():
            c.remove()
        f = t.open("w")
        test_data = "test"
        f.write(test_data)
        f.close()
        self.assertTrue(t.exists())
        self.assertFalse(c.exists())
        t.copy(c.path)
        self.assertTrue(t.exists())
        self.assertTrue(c.exists())
        self.assertEqual(t.open("r").read(), c.open("r").read())
        t.remove()
        c.remove()

    def test_move(self):
        t = self.create_target("original")
        if t.exists():
            t.remove()
        c = self.create_target("moved")
        if c.exists():
            c.remove()
        f = t.open("w")
        test_data = "test"
        f.write(test_data)
        f.close()
        self.assertTrue(t.exists())
        self.assertFalse(c.exists())
        t.move(c.path)
        self.assertFalse(t.exists())
        self.assertTrue(c.exists())
        self.assertEqual(c.open("r").read(), test_data)
        c.remove()

    def test_format_chain(self):
        UTF8WIN = luigi.format.TextFormat(encoding="utf8", newline="\r\n")
        t = self.create_target(format=UTF8WIN >> luigi.format.Gzip)
        a = "我é\nçф"

        with t.open("w") as f:
            f.write(a)

        with self.create_target(format=luigi.format.Nop).open("r") as f:
            raw_data = f.read()

        with gzip.open(io.BytesIO(raw_data), "rb") as f:
            b = f.read()

        self.assertEqual(b"\xe6\x88\x91\xc3\xa9\r\n\xc3\xa7\xd1\x84", b)

    def test_format_chain_reverse(self):
        t = self.create_target(format=luigi.format.UTF8 >> luigi.format.Gzip)

        buffer = io.BytesIO()
        with gzip.open(buffer, "wb") as f:
            f.write(b"\xe6\x88\x91\xc3\xa9\r\n\xc3\xa7\xd1\x84")

        with self.create_target(format=luigi.format.Nop).open("w") as f:
            f.write(buffer.getvalue())

        with t.open("r") as f:
            b = f.read()

        self.assertEqual("我é\nçф", b)

    @mock.patch("os.linesep", "\r\n")
    def test_format_newline(self):
        t = self.create_target(format=luigi.format.SysNewLine)

        with t.open("w") as f:
            f.write(b"a\rb\nc\r\nd")

        with t.open("r") as f:
            b = f.read()

        self.assertEqual(b"a\nb\nc\nd", b)

        try:
            with open(self.path, "rb") as f:
                c = f.read()

            self.assertEqual(b"a\r\nb\r\nc\r\nd", c)
        except FileNotFoundError:
            # Subclasses may not have a local file that can be accessed
            pass

    def theoretical_io_modes(self, rwax="rwax", bt=["", "b", "t"], plus=["", "+"]):
        p = itertools.product(rwax, plus, bt)
        return {
            "".join(c)
            for c in list(
                itertools.chain.from_iterable([itertools.permutations(m) for m in p])
            )
        }

    def valid_io_modes(self, *a, **kw):
        modes = set()
        # Test io modes based on a LocalTarget
        t = LocalTarget(is_tmp=True)
        t.open("w").close()
        for mode in self.theoretical_io_modes(*a, **kw):
            try:
                io.FileIO(t.path, mode).close()
            except ValueError:
                pass
            except IOError as err:
                if err.errno == EEXIST:
                    modes.add(mode)
                else:
                    raise
            else:
                modes.add(mode)
        return modes

    def valid_write_io_modes_for_luigi(self):
        return self.valid_io_modes("w", plus=[""])

    def valid_read_io_modes_for_luigi(self):
        return self.valid_io_modes("r", plus=[""])

    def invalid_io_modes_for_luigi(self):
        return self.valid_io_modes().difference(
            self.valid_write_io_modes_for_luigi(), self.valid_read_io_modes_for_luigi()
        )

    def test_open_modes(self):
        t = self.target_class(is_tmp=True)
        print("Valid write mode:", end=" ")
        for mode in self.valid_write_io_modes_for_luigi():
            print(mode, end=" ")
            p = t.open(mode)
            p.close()
        print()
        print("Valid read mode:", end=" ")
        for mode in self.valid_read_io_modes_for_luigi():
            print(mode, end=" ")
            p = t.open(mode)
            p.close()
        print()
        print("Invalid mode:", end=" ")
        for mode in self.invalid_io_modes_for_luigi():
            print(mode, end=" ")
            self.assertRaises(Exception, t.open, mode)
        print()


class GetPutTargetTestMixin:
    def test_get_put(self):
        target = self.target_class("dummy")
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n"
        target.put(origdata)
        data = target.get()
        self.assertEqual(origdata, data)

    def test_get_put_with_path(self):
        target = self.target_class(path="dummy")
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n"
        target.put(origdata)
        data = target.get()
        self.assertEqual(origdata, data)

    def test_get_put_with_name(self):
        target = self.target_class(name="dummy")  # Deprecated usage, but still works
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n"
        target.put(origdata)
        data = target.get()
        self.assertEqual(origdata, data)

    def test_get_put_with_task(self):
        task = FakeTask(param=1)
        target = self.target_class(
            "dummy", task=task
        )  # Deprecated usage, but still works
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n"
        target.put(origdata)
        data = target.get()
        self.assertEqual(origdata, data)

    def test_get_put_with_unicode(self):
        target = self.target_class("dummy")
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "Côte d'Ivoire\n"
        target.put(origdata)
        data = target.get()
        self.assertEqual(origdata, data)
