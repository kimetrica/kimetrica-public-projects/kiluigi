"""
Unit tests for CkanTarget.

These tests are skipped unless there is an CKAN_TEST_ORGANIZATION environment variable with
a value like "https://data.kimetrica.com/test-organization".

The organization must be accessible to the account specified by the CKAN_TOKEN environment variable which must
have permission to create and delete packages and resources. I.e. the account must be an Editor for the organization.

"""
import datetime
import os
import time
from tempfile import NamedTemporaryFile
from unittest import skip, skipUnless

import luigi
import requests

from kiluigi.targets import CkanTarget
from kiluigi.utils import with_config

from .test_base import BaseTargetTestCase


@skipUnless(
    os.environ.get("CKAN_TEST_ORGANIZATION"),
    "CKAN_TEST_ORGANIZATION environment variable is not set",
)
@with_config(
    {
        "ckan": {
            "token": os.environ.get("CKAN_TOKEN"),
        }
        if os.environ.get("CKAN_TOKEN")
        else {}
    }
)
class CkanTargetTestCase(BaseTargetTestCase):
    __test__ = True

    target_class = CkanTarget

    PATH_PREFIX = os.path.join(
        os.environ.get("CKAN_TEST_ORGANIZATION", ""),
        os.environ.get("TOX_ENV_NAME", ""),
    )

    def test_exists(self):
        target = self.create_target()
        with target.open("w") as fh:
            self.assertEqual(target.exists(), False)
            fh.write("Test")
        self.assertEqual(target.exists(), True)
        resource = target.fs.get_resource_from_path(target.path)
        self.assertEqual(requests.get(resource["url"]).status_code, 200)

    @skip("Can't use Pathlib paths with CkanTarget")
    def test_pathlib(self):
        pass

    @skip("Can't use CkanTarget for temporary-only files")
    def test_open_modes(self):
        pass

    @skip("CkanTarget doesn't support server-side copy")
    def test_copy(self):
        pass

    @skip("CkanTarget doesn't support server-side move")
    def test_move(self):
        pass

    @skip("CkanTarget doesn't support server-side move")
    def test_move_on_fs(self):
        pass

    @skip("CkanTarget doesn't support server-side move")
    def test_rename_dont_move_on_fs(self):
        pass

    def test_get_put_with_local_temporary_file(self):
        target = self.create_target()
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n".encode()
        with NamedTemporaryFile() as fa:
            fa.write(origdata)
            fa.flush()
            target.put(fa.name)

            local_path = target.get()
            with open(local_path, mode="rb") as fb:
                data = fb.read()
            os.remove(local_path)
            self.assertEqual(origdata, data)

    def test_get_put_with_local_file(self):
        target = self.create_target()
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n".encode()
        with NamedTemporaryFile() as fa:
            fa.write(origdata)
            fa.flush()
            target.put(fa.name)
            with NamedTemporaryFile() as fb:
                target.get(fb.name)
                fb.flush
                fb.seek(0)
                data = fb.read()
                self.assertEqual(origdata, data)

    def test_put_to_package_id_and_resource_name_with_local_file(self):
        target = self.create_target()
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())
        with target.open("w") as fh:
            fh.write("this text will be overwritten")

        resource = target.fs.get_resource_from_path(target.path)
        dataset = {"id": resource["package_id"]}
        resource = {"name": resource["name"]}

        legacy_target = CkanTarget(dataset=dataset, resource=resource, client=target.fs)
        self.assertTrue(legacy_target.exists())

        origdata = "lol\n".encode()
        legacy_target.remove()
        with NamedTemporaryFile() as fa:
            fa.write(origdata)
            fa.flush()
            legacy_target.put(fa.name)
            with NamedTemporaryFile() as fb:
                target.get(fb.name)
                fb.flush
                fb.seek(0)
                data = fb.read()
                self.assertEqual(origdata, data)

    def test_get_from_resource_name_with_local_file(self):
        target = self.create_target(format=luigi.format.Nop)
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n".encode()
        with target.open("w") as fh:
            fh.write(origdata)
        self.assertTrue(target.exists())

        resource = target.fs.get_resource_from_path(target.path)
        dataset = {"id": resource["package_id"]}
        resource = {"name": resource["name"]}

        legacy_target = CkanTarget(dataset=dataset, resource=resource, client=target.fs)
        self.assertTrue(legacy_target.exists())

        with NamedTemporaryFile() as fh:
            target.get(fh.name)
            fh.flush
            fh.seek(0)
            data = fh.read()
            self.assertEqual(origdata, data)

    def test_get_from_resource_id_with_local_file(self):
        target = self.create_target(format=luigi.format.Nop)
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        origdata = "lol\n".encode()
        with target.open("w") as fh:
            fh.write(origdata)
        self.assertTrue(target.exists())

        resource = target.fs.get_resource_from_path(target.path)
        dataset = {"id": resource["package_id"]}
        resource = {"id": resource["id"]}

        legacy_target = CkanTarget(dataset=dataset, resource=resource, client=target.fs)
        self.assertTrue(legacy_target.exists())

        with NamedTemporaryFile() as fh:
            target.get(fh.name)
            fh.flush
            fh.seek(0)
            data = fh.read()
            self.assertEqual(origdata, data)

        # We can also specify a legacy CkanTarget with just a resource
        legacy_target = CkanTarget(resource=resource, client=target.fs)
        self.assertTrue(legacy_target.exists())

        with NamedTemporaryFile() as fh:
            target.get(fh.name)
            fh.flush
            fh.seek(0)
            data = fh.read()
            self.assertEqual(origdata, data)

    def test_exists_for_dataset_id(self):
        target = self.create_target()
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())

        with target.temporary_path() as temp_output_path:
            os.makedirs(os.path.join(temp_output_path, "subfolder"))
            with open(os.path.join(temp_output_path, "subfolder", "1.txt"), "w") as f:
                f.write("File 1")
            with open(os.path.join(temp_output_path, "2.txt"), "w") as f:
                f.write("File 2")
        self.assertTrue(target.exists())

        dataset = target.fs.get_dataset_from_path(target.path)
        dataset = {"id": dataset["id"]}

        legacy_target = CkanTarget(dataset=dataset, client=target.fs)
        self.assertTrue(legacy_target.exists())

    def test_get_last_modified_date(self):
        dt = datetime.datetime.now(datetime.timezone.utc)
        time.sleep(5)
        target = self.create_target(
            format=luigi.format.Nop, path="test_get_last_modified_date"
        )
        if target.exists():
            target.remove()
        self.assertFalse(target.exists())
        origdata = "lol\n".encode()
        with target.open("w") as fh:
            fh.write(origdata)
        self.assertTrue(target.exists())
        modified_date = target.last_modified_datetime
        self.assertTrue(modified_date > dt)
        time.sleep(5)
        resource_id = target.fs.get_id_from_path(target.path)[1]
        target.fs.client.action.resource_update(
            id=resource_id,
            data_dict={
                "description": "updated description at {}".format(
                    datetime.datetime.now()
                )
            },
        )
        # Changing the metadata does not affect the last_modified datetime - only changing the file contents does.
        self.assertEqual(target.last_modified_datetime, modified_date)
