import unittest

from kiluigi.utils import submit_task
from tests.utils import FakeChildTask, FakeStandaloneChildTask, FakeTask


class SubmitTaskTestCase(unittest.TestCase):
    def test_submit_task(self):
        task = FakeTask(5)
        result = submit_task(task, local_scheduler=True)
        self.assertTrue(isinstance(result, dict))
        self.assertEqual(
            set(result.keys()),
            {
                "worker",
                "summary_text",
                "status",
                "one_line_summary",
                "scheduling_succeeded",
                "command",
                "start",
                "end",
                "elapsed",
                "task_id",
                "output",
            },
        )

    def test_submit_task_from_class(self):
        task = FakeTask
        params = {"param": 5}
        result = submit_task(task, local_scheduler=True, **params)
        self.assertTrue(isinstance(result, dict))
        self.assertEqual(
            set(result.keys()),
            {
                "worker",
                "summary_text",
                "status",
                "one_line_summary",
                "scheduling_succeeded",
                "command",
                "start",
                "end",
                "elapsed",
                "task_id",
                "output",
            },
        )

    def test_submit_task_from_str(self):
        task = "tests.utils.FakeTask"
        params = {"param": 5}
        result = submit_task(task, local_scheduler=True, **params)
        self.assertTrue(isinstance(result, dict))
        self.assertEqual(
            set(result.keys()),
            {
                "worker",
                "summary_text",
                "status",
                "one_line_summary",
                "scheduling_succeeded",
                "command",
                "start",
                "end",
                "elapsed",
                "task_id",
                "output",
            },
        )

    def test_submit_task_from_str_with_global_params(self):
        task = "tests.utils.FakeTask"
        params = {"param": 5, "scheduler_retry_count": 3}
        result = submit_task(task, local_scheduler=True, **params)
        self.assertTrue(isinstance(result, dict))
        self.assertEqual(
            set(result.keys()),
            {
                "worker",
                "summary_text",
                "status",
                "one_line_summary",
                "scheduling_succeeded",
                "command",
                "start",
                "end",
                "elapsed",
                "task_id",
                "output",
            },
        )

    def test_submit_task_with_force_parm(self):
        task = FakeChildTask()
        task.output().fs.clear()
        result = submit_task(task, local_scheduler=True)
        # For the first time run, both tasks are expected to run
        self.assertIn("2 ran successfully:", result.get("summary_text"))
        # Re-submit the child task with force=False
        result = submit_task(task, local_scheduler=True)
        self.assertIn("1 complete ones were encountered", result.get("summary_text"))
        self.assertIn("Did not run any tasks", result.get("summary_text"))

        # Re-submit the child task now with force=True
        result = submit_task(task, force=True, cascade=True, local_scheduler=True)
        self.assertIn("2 ran successfully:", result.get("summary_text"))
        self.assertNotIn("Did not run any tasks", result.get("summary_text"))

    def test_submit_child_task(self):
        task = FakeChildTask(param=10)
        task.output().fs.clear()
        result = submit_task(task, local_scheduler=True)
        self.assertIn("2 ran successfully:", result.get("summary_text"))
        # Check that the parameter was passed to the upstream task
        self.assertIn("for param=10", task.input().open().read())

    def test_submit_child_task_with_upstream_parameter(self):
        task = FakeStandaloneChildTask
        task().output().fs.clear()
        params = {"tests.utils.FakeParentTask_param": 20, "workers": 3}
        result = submit_task(task, local_scheduler=True, **params)
        self.assertIn("2 ran successfully:", result.get("summary_text"))
        # Check that the parameter was passed to the upstream task
        self.assertIn("for param=20", result["output"].open().read())
        # Check that the global parameter was also applied
        self.assertEqual(result["worker"].worker_processes, 3)
