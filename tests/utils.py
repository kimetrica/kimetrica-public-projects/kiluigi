import base64
import contextlib
import errno
import logging
import os
import signal

import luigi
from luigi.mock import MockTarget
from luigi.util import requires

logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT_MESSAGE = os.strerror(errno.ETIME)


class timeout(contextlib.ContextDecorator):
    """Easily put time restrictions on things

    See https://gist.github.com/TySkby/143190ad1b88c6115597c45f996b030c

    Note: Requires Python 3.x

    Usage as a context manager:
    ```
    with timeout(10):
        something_that_should_not_exceed_ten_seconds()
    ```

    Usage as a decorator:
    ```
    @timeout(10)
    def something_that_should_not_exceed_ten_seconds():
        do_stuff_with_a_timeout()
    ```

    Handle timeouts:
    ```
    try:
       with timeout(10):
           something_that_should_not_exceed_ten_seconds()
    except TimeoutError:
        log('Got a timeout, couldn't finish')
    ```

    Suppress TimeoutError and just die after expiration:
    ```
    with timeout(10, suppress_timeout_errors=True):
        something_that_should_not_exceed_ten_seconds()

    print('Maybe exceeded 10 seconds, but finished either way')
    ```
    """

    def __init__(
        self,
        seconds,
        *,
        timeout_message=DEFAULT_TIMEOUT_MESSAGE,
        suppress_timeout_errors=False,
    ):
        self.seconds = int(seconds)
        self.timeout_message = timeout_message
        self.suppress = bool(suppress_timeout_errors)

    def _timeout_handler(self, signum, frame):
        raise TimeoutError(self.timeout_message)

    def __enter__(self):
        signal.signal(signal.SIGALRM, self._timeout_handler)
        signal.alarm(self.seconds)

    def __exit__(self, exc_type, exc_val, exc_tb):
        signal.alarm(0)
        if self.suppress and exc_type is TimeoutError:
            return True


def get_auth_for_user(user, password="password"):  # noqa: S107
    user.set_password(password)
    user.save()
    return "Basic {0}".format(
        base64.b64encode("{0}:{1}".format(user.username, password).encode()).decode()
    )


class TestException(Exception):
    pass


class FakeTask(luigi.Task):

    param = luigi.IntParameter()

    _complete = False

    def complete(self):
        return self._complete

    def run(self):
        self._complete = True


class FakeParentTask(luigi.Task):

    param = luigi.IntParameter(default=5)

    def output(self):
        return MockTarget("FakeParentTask")

    def run(self):
        data = f"some output from {self} for param={self.param}"
        with self.output().open("w") as output:
            output.write(data)


@requires(FakeParentTask)
class FakeChildTask(luigi.Task):
    def output(self):
        return MockTarget("FakeChildTask")

    def run(self):
        parent_data = self.input().open().read()
        data = f"some output from {self} dependent on {parent_data}"
        with self.output().open("w") as output:
            output.write(data)


class FakeStandaloneChildTask(luigi.Task):
    def requires(self):
        return FakeParentTask()

    def output(self):
        return MockTarget("FakeChildTask")

    def run(self):
        parent_data = self.input().open().read()
        data = f"some output from {self} dependent on {parent_data}"
        with self.output().open("w") as output:
            output.write(data)
