import os
import unittest

import geojson

from kiluigi.parameter import GeoParameter

BASE_DIR = os.path.dirname(__file__)


class GeoParameterTestCase(unittest.TestCase):
    def setUp(self):
        self.geo_parameter = GeoParameter()
        self.geojson_sample_file = os.path.join(BASE_DIR, "data/output.geojson")
        self.kml_sample_file = os.path.join(BASE_DIR, "data/example.kml")
        # we use geojson from our sample file in several places, so loading it here
        with open(self.geojson_sample_file) as f:
            self.geo_json_from_geojson_file = geojson.load(f)

    def test_parse(self):
        """Test parse method, the method basically only calls get_geojson_from_input."""
        with open(self.kml_sample_file) as kml_file:
            kml_data = kml_file.read()
        geo_json_from_kml_string = self.geo_parameter.parse(kml_data)
        self.assertEqual(geo_json_from_kml_string, self.geo_json_from_geojson_file)

    def test_get_geojson_from_kml_file(self):
        geo_json_from_kml_file = self.geo_parameter.get_geojson_from_kml_file(
            self.kml_sample_file
        )
        self.assertEqual(geo_json_from_kml_file, self.geo_json_from_geojson_file)

    def test_get_geojson_from_kml_string(self):
        with open(self.kml_sample_file) as kml_file:
            kml_data = kml_file.read()
        geo_json_from_kml_file = self.geo_parameter.get_geojson_from_kml_string(
            kml_data
        )
        self.assertEqual(geo_json_from_kml_file, self.geo_json_from_geojson_file)

    def test_get_name_of_kml_data(self):
        with open(self.kml_sample_file) as kml_file:
            kml_data = kml_file.read()
        kml_data_name = self.geo_parameter.get_name_of_kml_data(kml_data)
        self.assertEqual(kml_data_name, "example")

    def test_get_geojson_from_input(self):
        """Test if we can get a GeoJSON string from input.

        Input is either a

        - path of a geojson file
        - path of a kml file
        - string with geojson
        - string with kml
        """
        # test path of a geojson file
        test_input = self.geo_parameter.get_geojson_from_input(self.geojson_sample_file)
        self.assertEqual(test_input, self.geo_json_from_geojson_file)
        # test path of a kml file
        test_input = self.geo_parameter.get_geojson_from_input(self.kml_sample_file)
        self.assertEqual(test_input, self.geo_json_from_geojson_file)
        # test string with geojson
        test_input = self.geo_parameter.get_geojson_from_input(
            geojson.dumps(self.geo_json_from_geojson_file)
        )
        self.assertEqual(test_input, self.geo_json_from_geojson_file)
        # test string with kml
        with open(self.kml_sample_file) as f:
            kml_sample_string = f.read()
        test_input = self.geo_parameter.get_geojson_from_input(kml_sample_string)
        self.assertEqual(test_input, self.geo_json_from_geojson_file)


if __name__ == "__main__":
    unittest.main()
