import os
import time

import luigi
from luigi.contrib.redis_store import RedisTarget


class Bar(luigi.Task):
    num = luigi.IntParameter()

    def run(self):
        time.sleep(0.1)
        self.output().touch()

    def output(self):
        """
        Returns the target output for this task.
        :return: the target output for this task.
        :rtype: object (:py:class:`~luigi.contrib.redis_store.RedisTarget`)
        """
        target = RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"],
            port=6379,
            db=1,
            expire=60,
            update_id=self.num,
        )
        target.marker_prefix = self.task_id
        return target


class Foo(luigi.WrapperTask):
    start = luigi.IntParameter(default=1)
    count = luigi.IntParameter(default=10)

    def run(self):
        print("Running Foo")

    def requires(self):
        for i in range(self.start, self.start + self.count):
            yield Bar(i)


class MultipleInputs(luigi.Task):
    bool_param = luigi.BoolParameter()
    choice_param = luigi.ChoiceParameter(
        choices=["chicken", "beef", "fish"], var_type=str
    )
    date_param = luigi.DateParameter()
    float_param = luigi.FloatParameter()
    int_param = luigi.IntParameter()
    year_param = luigi.YearParameter(default=1981)
    numerical_param_int = luigi.NumericalParameter(
        var_type=int, min_value=10, max_value=100
    )
    numerical_param_float = luigi.NumericalParameter(
        var_type=float, min_value=10.00, max_value=100.00
    )
    second_year_param = luigi.YearParameter(default=2018)
    # geo_param = GeoParameter()
