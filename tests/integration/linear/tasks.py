import os
import time

import luigi
from luigi.contrib.redis_store import RedisTarget
from luigi.util import requires


class Src2(luigi.Task):

    src_num = luigi.IntParameter(default=1)

    def run(self):
        time.sleep(1)
        print("Running Src")
        self.output().touch()

    def output(self):
        target = RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"],
            port=6379,
            db=1,
            expire=60,
            update_id=self.src_num,
        )
        target.marker_prefix = self.task_id
        return target


@requires(Src2)
class Mid2(luigi.Task):

    mid_num = luigi.IntParameter(default=2)

    def run(self):
        print(
            "RUNNING Mid2 **********************************************************************"
        )
        time.sleep(1)
        self.output().touch()

    def output(self):
        target = RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"],
            port=6379,
            db=1,
            expire=60,
            update_id=self.mid_num,
        )
        target.marker_prefix = self.task_id
        return target


@requires(Mid2)
class Dest2(luigi.Task):
    def run(self):
        time.sleep(1)
        self.output().touch()

    def output(self):
        target = RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"], port=6379, db=1, expire=60, update_id=1
        )
        target.marker_prefix = self.task_id
        return target
