import pandas as pd
from luigi import IntParameter
from luigi.util import requires

import kiluigi


class TrainingTask(kiluigi.VariableInfluenceMixin, kiluigi.Task):
    """This task outputs an influence grid, but not a variable.
    ML training tasks that calculate influence should be implemented like this.
    """

    training_param = IntParameter(
        default=5, description="Test to check parameters work OK."
    )

    def run(self):
        self.output()["my_trained_model"].open("w").close()

        if self.calculate_influence:
            influence_grid = pd.DataFrame(
                [
                    ["indian_ocean_surface_temperature", 0.70],
                    ["atlantic_ocean_surface_temperature", 0.10],
                    ["variabletask_generated", 0.20],
                ],
                columns=[self.COL_SOURCE_VARIABLE, self.COL_INFLUENCE_PERCENT],
            )
            self.store_influence_grid("trainer_generated", influence_grid)

    def output(self):
        outputs = {
            "my_trained_model": kiluigi.targets.LocalTarget(
                f"vcag_test/TrainingTask{self.training_param}.ext"
            )
        }
        if self.calculate_influence:
            # Needn't be a dict, can be a list of targets or any other Luigi-compatible structure
            outputs["my_influence_grid_key"] = self.get_influence_grid_target()
        return outputs


@requires(TrainingTask)
class SimpleVariableTask(kiluigi.VariableInfluenceMixin, kiluigi.VariableTask):
    """This generates both a variable and if calculate_influence, also an influence grid."""

    def get_variable_metadata(self):
        return {
            "variabletask_generated": {
                self.DATA_TARGET: self.output()["my_output_key"],
                self.MEASURE: "Kilograms",
            }
        }

    def run(self):
        self.output()["my_output_key"].open("w").close()

        if self.calculate_influence:
            influence_grid = pd.DataFrame(
                [["precipitation", 0.40], ["region_avg_nvdi", 0.60]],
                columns=[self.COL_SOURCE_VARIABLE, self.COL_INFLUENCE_PERCENT],
            )
            self.store_influence_grid("variabletask_generated", influence_grid)

    def output(self):
        outputs = {
            "my_output_key": kiluigi.targets.LocalTarget("vcag_test/SimpleVarTask.ext")
        }
        if self.calculate_influence:
            outputs["my_great_influence_grid"] = self.get_influence_grid_target()
        return outputs


@requires(SimpleVariableTask)
class PredictorVariableTask(kiluigi.VariableTask):
    """This generates the variable for the above training task, but it does not generate an influence grid so doesn't
    need to inherit from kiluigi.VariableInfluenceMixin.
    """

    # How can a singleton Register track simultaneous Scenarios?
    terminal_task_param = IntParameter(
        default=12, description="Test to check parameters work OK."
    )

    def run(self):
        self.output().open("w").close()

    def output(self):
        return kiluigi.targets.LocalTarget(
            f"vcag_test/PredictorVariableTask{self.terminal_task_param}.ext"
        )


if __name__ == "__main__":
    import luigi

    luigi.build([PredictorVariableTask(calculate_influence=True)], local_scheduler=True)
