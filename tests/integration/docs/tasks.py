import luigi

import kiluigi


class Bar(kiluigi.Task):
    """
    Sample Task docstring template.

    Description:
        Pull the raster data for independent variables.

    Inputs:
        accessibility (Raster): the accessibility data from the `Malaria Atlas Project
        <https://map.ox.ac.uk/wp-content/uploads/accessibility/accessibility_to_cities_2015_v1.0.zip>`_
        night_lights (Raster): the night lights data from `NOAA
        <https://data.ngdc.noaa.gov/instruments/remote-sensing/passive/spectrometers-radiometers/imaging/viirs/dnb_composites/v10/2015/>`_  # NOQA: E501
        landcover (Raster): the landcover data from `USGS <https://landcover.usgs.gov/global_climatology.php>`_
        population_2015 (Raster): the population data from the population model
        population_2016 (Raster): the population data from the population model
        population_2017 (Raster): the population data from the population model

    Outputs:
        Raster Filepaths (Dictionary of CkanTargets): local filepaths to all data pulled in from CKAN server.

    User Defined Parameters:
    References:
    Methods:
    """

    tags = {"ingestion", "bar"}

    num = luigi.IntParameter()
    # @TODO: re-enable this check / make config mocking work
    # luigi.configuration.get_config().get("this_tests_our_config_mocking", "param_name")

    def run(self):
        print("Bar.run")

    def output(self):
        """
        Sample output docstring, displayed on output nodes of task DAG.

        We could use standard RST docstrings:

            :param temp_df: The survey response DataFrame
            :type temp_df: DataFrame
            :param xyz_q_list: The :class:`list` of questions for xyz
            :type xyz_q_list: list
            :returns: A list of processed answers
            :rtype: list

        Or the Google style - more readable and less fiddly to type:

        Args:
            temp_df (DataFrame): The survey response DataFrame
            xyz_q_list (list): The :class:`list` of questions for xyz

        Returns:
            list: A :class:`list` of processed answers

        Or the Numpy style - more readable when we've lots of parameters, but takes more vertical space:

        Parameters
        ----------
        temp_df : DataFrame
            The survey response ::class::`DataFrame`
        xyz_q_list : list
            The :class:`list` of questions for xyz

        Returns
        -------
        list
            The :class:`list` of processed answers


        Possible section headers:

            * ``Args`` (alias of Parameters)
            * ``Arguments`` (alias of Parameters)
            * ``Attributes``
            * ``Example``
            * ``Examples``
            * ``Keyword Args`` (alias of Keyword Arguments)
            * ``Keyword Arguments``
            * ``Methods``
            * ``Note``
            * ``Notes``
            * ``Other Parameters``
            * ``Parameters``
            * ``Return`` (alias of Returns)
            * ``Returns``
            * ``Raises``
            * ``References``
            * ``See Also``
            * ``Todo``
            * ``Warning``
            * ``Warnings`` (alias of Warning)
            * ``Warns``
            * ``Yield`` (alias of Yields)
            * ``Yields``
        """
        return kiluigi.targets.LocalTarget("Bar%s.ext" % self.num)


class Foo(kiluigi.WrapperTask):
    """
    Sample task docstring for demonstrating (and testing) rendering of RST syntax.

    This is generated from the *docstring* of :class:`ClusterDataCkanExternal`.
    It contains examples of some of the syntax made available by the Sphinx RST language.
    Use the examples to build the ideal docstring format for Task classes.
    Also see this class's :func:`output` docstring for the three ways we could document methods:

    * Standard RST
    * The Google format - looks nicer
    * The Numpy format - looks nicer for large numbers of parameters, but takes more vertical space

    In addition to docstrings on Task classes and their methods, analysts will maintain RST files
    documenting the pipelines. See economic_pipeline.rst

    The source of the below demonstrates some of the syntax.

    It includes ``code`` inline, and as a block:

    .. code-block:: python
       :linenos:
       :emphasize-lines: 2,3
       :caption: this.py
       :name: this-py

        def output(self):
            l = [1, 2, 3, ]
            i = l + [4, 5, ]
            # This should be Python highlighted and lines 2 and 3 emphasised - these don't seem to work. Hey ho.
            return [LocalTarget(os.path.join(config.get("paths", "quarterly_data_path"), "Section_6__Household.csv"))]

    It includes a doctest section. These can be automatically tested on each commit to test the
    documentation is up to date. In PyCharm, right click the code fragment to run.

    >>> 1 + 1
    2

    Some tables:

    +------------------------+------------+----------+----------+
    | Header row, column 1   | Header 2   | Header 3 | Header 4 |
    | (header rows optional) |            |          |          |
    +========================+============+==========+==========+
    | body row 1, column 1   | column 2   | column 3 | column 4 |
    +------------------------+------------+----------+----------+
    | body row 2             | ...        | ...      |          |
    +------------------------+------------+----------+----------+

    Simpler:

    =====  =====  =======
    A      B      A and B
    =====  =====  =======
    False  False  False
    True   False  False
    False  True   False
    True   True   True
    =====  =====  =======

    A `link <https://www.example.com/>`_

    A `footnote too`_.

    .. _footnote too: https://example.com/

    Also, a footnoted academic reference [ref01]_

    Some text replacement |to_replace|

    .. |to_replace| replace:: replacement *replaced*

    An internal reference to our in-page :class:`RegridRasterForTrainingData` will convert into a hyperlink.

    An external reference to Luigi :class:`Task` will **not** convert into a hyperlink.
    You can use ``:class:``, ``:mod:`` and ``:func:`` similarly.

    .. warning::

        It also includes a warning.

    A version comment is added when a package has functionality added:

    .. versionadded:: 0.30

    An image:

    .. image:: ../_static/logo.*
        :alt: Logo


    Inline maths: :math:`a^2 + b^2 = c^2`.

    Maths sections, labelled for cross-referencing:

    .. math:: e^{i\pi} + 1 = 0
       :label: euler

    Euler's identity, equation :eq:`euler`.

    Nb. When a ``:label:`` is specified readthedocs currently renders oddly on the autodoc list,
    so don't label equations until readthedocs fixes this.

    There are more complex directives
    `documented here. <https://docutils.readthedocs.io/en/sphinx-docs/ref/rst/directives.html>`_

    This is what the pipeline DAG diagram produces for :class:`tests.integration.docs.tasks.Foo`.
    The pipeline diagram directive can merge multiple endpoint task DAGs into a single diagram.

    .. pipeline-diagram:: tests.integration.docs.tasks.Foo

    There is an inheritance diagram directive, here run on :class:`tests.integration.docs.tasks.Foo`:

    .. inheritance-diagram:: tests.integration.docs.tasks.Foo

    .. [ref01] https://www.example.com
    """  # noqa: W605

    start = luigi.IntParameter(default=1)
    count = luigi.IntParameter(default=10)

    def run(self):
        print(f"run: {self.start=}")
        print(f"run: {self.count=}")
        print("Foo.run(start=%s, count=%s)" % (self.start, self.count))

    def requires(self):
        print(f"{self.start=}")
        print(f"{self.count=}")
        return Bar(1), Bar(2)
