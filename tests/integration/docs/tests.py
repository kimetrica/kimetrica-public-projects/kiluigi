import logging
import unittest

from kiluigi.docs.utils import get_task_dag_as_dot

logger = logging.getLogger("kiluigi-unit-tests")


class DagDocumentationTestCase(unittest.TestCase):
    def test_dag_generated(self):
        # Note that this does not test the CI integration and Sphinx directive
        # This is done in-situ by `docs/developers/tests.rst`
        dot = get_task_dag_as_dot(["tests.integration.docs.tasks.Foo"])
        self.assertIn("Foo", dot)
        self.assertIn("Bar", dot)
        self.assertIn("LocalTarget", dot)
        self.assertIn("Sample task docstring", dot)
        self.assertIn("Sample output docstring", dot)
