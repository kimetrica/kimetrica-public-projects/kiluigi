import os
import random

import luigi
from luigi.contrib.redis_store import RedisTarget

import kiluigi


class SimpleLast(kiluigi.Task):
    start = luigi.IntParameter()
    end = luigi.IntParameter()
    retry_count = 50

    def requires(self):
        print("Running SimpleLast.requires(start=%s, end=%s)" % (self.start, self.end))
        # time.sleep(0.1)
        for i in range(self.start, self.end + 1):
            yield SimpleMid(i)

    def run(self):
        print("Running SimpleLast.run(start=%s, end=%s)" % (self.start, self.end))
        # time.sleep(1)
        self.output().touch()

    def output(self):
        print("Running SimpleLast.output(start=%s, end=%s)" % (self.start, self.end))
        # time.sleep(0.1)
        target = luigi.contrib.redis_store.RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"],
            port=6379,
            db=1,
            expire=360,
            update_id="%s_to_%s" % (self.start, self.end),
        )
        target.marker_prefix = self.task_id
        return target


class SimpleMid(kiluigi.Task):
    num = luigi.IntParameter()
    exception_probability = luigi.IntParameter(default=10)
    retry_count = 50

    def requires(self):
        print("Running SimpleMid.requires(%s)" % self.num)
        # time.sleep(0.1)
        return SimpleSrc(self.num % 4)

    def run(self):
        print("Running SimpleMid.run(%s)" % self.num)
        rnd = random.randint(0, 99)  # NOQA: S311
        if rnd < self.exception_probability:
            raise Exception(
                f"{self} raising exception. Excep_prob={self.exception_probability}, rand={rnd}"
            )
        self.output().touch()

    def output(self):
        print("Running SimpleMid.output(%s)" % self.num)
        # time.sleep(0.1)
        target = luigi.contrib.redis_store.RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"],
            port=6379,
            db=1,
            expire=360,
            update_id=self.num,
        )
        target.marker_prefix = self.task_id
        return target


class SimpleSrc(kiluigi.Task):
    num = luigi.IntParameter()

    def run(self):
        print("Running SimpleSrc.run(%s)" % self.num)
        # time.sleep(1)
        self.output().touch()

    def output(self):
        print("Running SimpleSrc.output(%s)" % self.num)
        # time.sleep(0.1)
        target = RedisTarget(
            host=os.environ["REDIS_CACHE_HOST"],
            port=6379,
            db=1,
            expire=180,
            update_id=self.num,
        )
        target.marker_prefix = self.task_id
        return target
