#!/usr/bin/env python

"""The setup script."""

from setuptools import find_packages, setup

with open("README.md") as readme_file:
    readme = readme_file.read()

with open("requirements/base.pip") as requirements_file:
    requirements = [
        # we don't want trailing whitespace, no empty lines, comments, includes
        line.rstrip()
        for line in requirements_file
        if not line.startswith(("#", "-", "\n"))
    ]

with open("requirements/test.pip") as requirements_file:
    test_requirements = [
        # we don't want trailing whitespace, no empty lines, comments, includes
        line.rstrip()
        for line in requirements_file
        if not line.startswith(("#", "-", "\n"))
    ]

setup(
    author="Roger Hunwicks",
    author_email="roger.hunwicks@kimetrica.com",
    python_requires=">=3.8",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
    ],
    description="Useful additions to Luigi framework",
    install_requires=requirements,
    license="MIT license",
    long_description=readme,
    include_package_data=True,
    keywords="kiluigi",
    name="kiluigi",
    packages=find_packages(include=["kiluigi", "kiluigi.*"]),
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/kimetrica/kimetrica-public-projects/kiluigi",
    version="24.02.1.dev0",
    zip_safe=False,
)
