## Kiluigi API

The *kiluigi* API allows to list and trigger Luigi tasks. It also offers a convenient interface to the Luigi task history.


### /task/ endpoint

The task endpoint can be used to get a list of tasks known to Luigi and trigger tasks.


#### Getting a list of tasks

A `GET` request to the `/task/` endpoint will return a paginated list, e.g.

```
{
  "count": 15,
  "next": "https://darpa-api.kimetrica.com/task/?page=2",
  "previous": null,
  "results": [
    {
      "name": "tests.integration.linear.tasks.Dest2",
      "task_url": "tests__integration__linear__tasks__Dest2",
      "parameters": [
        {
          "name": "src_num",
          "description": null,
          "default": null,
          "parameter_class": "luigi.parameter.IntParameter"
        },
        {
          "name": "mid_num",
          "description": null,
          "default": null,
          "parameter_class": "luigi.parameter.IntParameter"
        }
      ]
    },
    "[...]"
  ]
}
```

The `task_url` key of each task will contain a URL-safe string, that you can use to retrieve the detail view for a given task. The list of tasks already contains all necessary parameters though that you need to trigger a task.


#### Getting the details for a given task

A `GET` request to the detail endpoint of a given task, e.g. `/task/tests__integration__linear__tasks__Dest2/` endpoint will return the details of that task, e.g.

```JSON
{
  "name": "tests.integration.linear.tasks.Dest2",
  "task_url": "tests__integration__linear__tasks__Dest2",
  "parameters": [
    {
      "name": "src_num",
      "description": null,
      "default": null,
      "parameter_class": "luigi.parameter.IntParameter"
    },
    {
      "name": "mid_num",
      "description": null,
      "default": null,
      "parameter_class": "luigi.parameter.IntParameter"
    }
  ]
}
```

#### Triggering a task

To trigger a task, a `POST` request has to be made to the `/task/` endpoint, including a JSON payload with a `name` key for the task_name, and a `parameters` key:

```JSON
{
  "name": "tests.integration.linear.tasks.Dest2",
  "parameters": [
    {
      "name": "src_num",
      "value": "1"
    },
    {
      "name": "mid_num",
      "value": "2"
    }
  ]
}
```


Example curl request:

`curl -H "content-type: application/json" -X POST -d '{"name": "tests.integration.linear.tasks.Dest2", "parameters": [{"name": "src_num", "value": "1"}, {"name": "mid_num", "value": "2"}]}' https://darpa-api.kimetrica.com/task/`

The API will return the Luigi `task_id` if the task has been triggered successfully, e.g.

```JSON
{
  "task_id": "tests.integration.linear.tasks.Dest2_2_1_9f984564fd"
}
```

This `task_id` can be used to get information about the task run from the `task_run` endpoint. Note that the `task_id` is not unique, all task runs that were triggered with the same parameters share it.



### /task_run/ endpoint

The `/task_run/` endpoint can be used to get information about task runs. Making a request to `/task_run/` will return a paginated list of all task runs, so it should be filtered down.
The endpoint can be filtered by `task_id`. `task_id`s are not unique, they are identifying all tasks run for a given task triggered with the same parameters.
To get a list of task run instances for a specific `task_id`, you can filter the list like this:

`/task_run/?search=tests.integration.linear.tasks.Dest2_2_1_9f984564fd`

This will return a paginated result list, with all task run instances. The latest one will be the first element in the result list.

If you want to get the latest task run automatically, you can call the detail endpoint, with a URL safe task_name where `.` have been replaced by `__`, e.g.

`/task_run/tests__integration__linear__tasks__Dest2_2_1_9f984564fd/`

This endpoint will return only the latest task run instance. A response might look like this:

```JSON
{
  "task_id": "tests.integration.linear.tasks.Dest2_2_1_9f984564fd",
  "id": 69,
  "name": "tests.integration.linear.tasks.Dest2",
  "host": null,
  "status": "PENDING",
  "created": "2019-05-06T11:26:58",
  "status_changed": "2019-05-06T11:29:14",
  "elapsed": "02:31:21.122938",
  "events": [
    {
      "id": 122,
      "event_name": "PENDING",
      "ts": "2019-05-06T11:26:58",
      "task": 69
    },
    {
      "id": 131,
      "event_name": "PENDING",
      "ts": "2019-05-06T11:29:14",
      "task": 69
    }
  ],
  "parameters": [
    {
      "code": "69.mid_num",
      "name": "mid_num",
      "value": "2",
      "task": 69
    },
    {
      "code": "69.src_num",
      "name": "src_num",
      "value": "1",
      "task": 69
    }
  ],
  "outputs": [
    {
      "name": "<luigi.contrib.redis_store.RedisTarget object at 0x7fd7601c5a20>",
      "url": "Output not available for download - please contact the Helpdesk"
    },
    {
      "name": "<luigi.contrib.redis_store.RedisTarget object at 0x7fd75bf55e80>",
      "url": "Output not available for download - please contact the Helpdesk"
    },
    {
      "name": "<luigi.contrib.redis_store.RedisTarget object at 0x7fd76001bcf8>",
      "url": "Output not available for download - please contact the Helpdesk"
    }
  ],
  "superset_links": [
    {
      "superset_dashboard_link": "https://darpa-analytics.kimetrica.com/superset/dashboard/6/?preselect_filters=%7B%2223%22%3A%7B%22ADMIN1NAME%22%3A%5B%22Unity%22%2C%22Northern%20Bahr%20el%20Ghazal%22%5D%7D%7D",
      "superset_dashboard_name": "Malnutrition Model"
    },
    {
      "superset_dashboard_link": "https://darpa-analytics.kimetrica.com/superset/dashboard/3/?preselect_filters=%7B%2227%22%3A%7B%22State%22%3A%5B%22Northern%20Bahr%20el%20Ghazal%22%5D%2C%22Commodity%22%3A%5B%22Maize%22%5D%7D%7D",
      "superset_dashboard_name": "Crop Production"
    }
  ]
}
```
