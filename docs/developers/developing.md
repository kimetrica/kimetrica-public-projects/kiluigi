# Developing Data Pipelines using KiLuigi

## Ensuring that pipelines can be deployed on a server

### Introduction

Frequently we need to deploy a finished pipeline on a server. For example if we want to give non-analysts the ability to run the pipeline, or if we want to run the pipeline on a regular schedule, or because the pipeline needs more processing resources than are available on a single workstation.

In order to write pipelines that can be deployed on a server it is important to remember that the processing may be split across many Luigi worker nodes, and that a Task might not run on the same worker node as its upstream requirements. In particular this means that using *Local Target* to pass data between Tasks will not work. The recommended solution is to use *Intermediate Target* and *Final Target* instead.

### Using *Intermediate Target* and *Final Target*

*Intermediate Target* and *Final Target* allow analysts to write pipelines that are agnostic about the final deployment infrastructure.  They provide a common API that allows the pipeline to write to a file on disk in the analyst's workstation while the pipeline is being developed, but to write to a networked file store when the finished pipeline is deployed on a server.

*Intermediate Target* is used to pass data between Tasks. It is designed to be easy to use and to make sensible default choices about serialization formats, etc. It is generally used when the contents of the *Target* will not need to be inspected by the user. It is a replacement for *Serializer Target* or *Expiring Local Target* in existing pipelines. On a workstation it is normally backed by an *Expiring Local Target* and on a server it is normally backed by an *Expiring S3 Target*.

*Final Target* is used to output data that does not expire and will be made available to the users. A *Final Target* generally has a file name that is meaningful to the end users. It is a replacement for *Local Target* or some uses of *CKAN Target* in existing pipelines. On a workstation it is normally backed by a *Local Target*. On a server it is normally backed by a *Taggable S3 Target* or a *CKAN Target* (note that using *CKAN Target* as a *Final Target* backend requires changes to *CKAN Target* to support the `open().write()` and `open().read()` API rather than the existing `get()` and `put()` API).

Note that *Intermediate Target* and *Final Target* write all data under a root path set in the `luigi.cfg`. On a workstation the default location is an `output/` directory under the project root that is ignored by both Docker and Git. See the *Configuring Intermediate Target and Final Target* section below for instructions on how to configure the root path.

#### Using *Intermediate Target* and *Final Target* in a *Task*

An *Intermediate Target* or *Final Target* will guess a suitable output format (text or binary) depending on the extension of the `path` passed to the *Target* and will default to `pickle` if no path, or a path without an extension, is specified.

If a `path` is provided it can be just a filename, or a combination of a directory location and a filename. If a directory is specified it must be relative, i.e. it must not have a leading path separator. It is our convention to use / as the directory separator when specifying `IntermediateTarget` or `FinalTarget` paths in Tasks regardless of the operating system.  `/` will automatically be replaced with `os.path.sep` to ensure Windows compatibility.

If a *Task* instance is passed to the *Target* it will be used to ensure that the output location is unique for the `luigi.Task.task_id`. (i.e. for that unique set of significant parameters for the Task, assuming that `TASK_ID_TRUNCATE_HASH` and `TASK_ID_TRUNCATE_PARAMS` are set appropriately).

Consequently, an *Intermediate Target* can be used without any additional configuration beyond passing the `task` instance and an optional timeout:

```python

class SaveTemporaryData(Task):

    def requires(self):
        return UpstreamTask()

    def output(self):
        return IntermediateTarget(task=self, timeout=3600)

    def run(self):
        with self.input().open("r") as f:
            df = self.get_df_from_input(f.read())

        # Save the resulting dataframe
        with self.output().open("w") as output:
            output.write(df)
```

In this configuration, without a filename being set the *Intermediate Target* will be named automatically from the `task` instance including all the significant parameters. It will serialize whatever Python object is written to it using `pickle`. Most uses of *Intermediate Target* will benefit from passing the `task`, even if the *Task* is also setting a `path` to give the output file or directory a meaningful name.

A *Final Target* normally requires a name that will be meaningful to the end user.

```python

class SaveFinalOutput(DataFrameOutputToExcelTask):

    def output(self):
        return FinalTarget(path="final_pipeline_output.xlsx")
```

If we need to ensure that multiple runs produce separate outputs and there are only a few significant parameters, then we can specify them in the output path:

```python

class SaveParameterizedFinalOutput(DataFrameOutputToExcelTask):

    parameter_x = luigi.Parameter()

    def output(self):
        return FinalTarget(f"final_pipeline_output_{self.parameter_x}.xlsx")
```

If we need to ensure that multiple runs produce separate outputs and there are many significant parameters, then we should pass the task instance to the *Target* so that it can include the hash of the `task_id` in the output filename:

```python

@requires(ManyParameterUpstreamTask)
class SaveVeryParameterizedFinalOutput(DataFrameOutputToExcelTask):

    def output(self):
        return FinalTarget(f"economic_pipeline/final_pipeline_output.xlsx", task=self)
```

This will create a file on disk like `output/economic_pipeline/final_pipeline_output_abcde12345.xlsx`. The Valve API and user interface can be used to retrieve the exact output for a particular *Task* run.

Note that whether or not the `task` is required for *Final Targets* is likely to be depend on the context. For projects that produce large numbers of *Task Runs* with many significant parameters on the terminal tasks including the `task_id` will normally be best practice.

#### Configuring *Intermediate Target* and *Final Target*

The actual backends used are set in the `luigi.cfg` (or defaulted from `luigi.cfg.example`), where they are inherited from environment variables. In most case the `luigi.cfg.example` and the `env.example` for the project should be set up so that analysts don't need to change anything in order to start using *Intermediate Target* and *Final Target*.

Typically, `luigi.cfg.example` contains:

```
[kiluigi.targets.delegating.IntermediateTarget]
backend_class=$KILUIGI_INTERMEDIATETARGET_BACKEND_CLASS
root_path=$KILUIGI_INTERMEDIATETARGET_ROOT_PATH

[kiluigi.targets.delegating.FinalTarget]
backend_class=$KILUIGI_FINALTARGET_BACKEND_CLASS
root_path=$KILUIGI_FINALTARGET_ROOT_PATH
```

And `env.example` contains:

```
KILUIGI_INTERMEDIATETARGET_BACKEND_CLASS=kiluigi.ExpiringLocalTarget
KILUIGI_INTERMEDIATETARGET_ROOT_PATH=output/intermediate_targets
KILUIGI_FINALTARGET_BACKEND_CLASS=kiluigi.LocalTarget
KILUIGI_FINALTARGET_ROOT_PATH=output/final_targets
```

In a server deployment the environment variables would be set differently, for example:

```
KILUIGI_INTERMEDIATETARGET_BACKEND_CLASS=kiluigi.ExpiringS3Target
KILUIGI_INTERMEDIATETARGET_ROOT_PATH=s3://maaklgmaa/intermediate_targets
KILUIGI_FINALTARGET_BACKEND_CLASS=kiluigi.TaggableS3Target
KILUIGI_FINALTARGET_ROOT_PATH=s3://maaklgmaa/final_targets
```

### Using directories as a *Target*

Some *Tasks* will output multiple files into a single directory. In this situation it is important that Luigi does not think that the *Target* exists before the upstream *Task*
has finished writing to it.

If we simply use `LocalTarget(path='output/mydir/')` and start writing files into it, then as soon as the directory is created the Luigi Scheduler may consider the upstream *Task* to be complete and schedule a downstream *Task* that then reads the incomplete files in the directory.

In order to ensure that the downstream *Task* doesn't run until the upsteam *Task* is complete we need to write the files into a temporary directory and then rename the directory to the target path once all the files have been written.  Luigi provides the `Target.temporary_path()` context manager for this purpose. The correct way to use a *Target* that is a directory of files is:

```python
class DirectoryOutputTask(Task):

    def output(self):
        # Use a trailing / to force Luigi to treat the path as a directory
        return IntermediateTarget(path=f"{self.task_id}/", is_dir=True, timeout=3600)

    def run(self):
        # Save the files to the Target directory, via a temporary_path()
        with self.output().temporary_path() as tmpdir:
            # If the main command doesn't create the output directory automatically
            # then create it manually first.
            os.makedirs(tmpdir)
            # Then pass the temporary directory path to the command that outputs the files
            self.output_files_to_directory(tmpdir)

            # Alternatively, write multiple files directly
            with open(os.path.join(tmpdir, "file1.txt"), "w") as f:
                f.write(self.get_first_output())
            with open(os.path.join(tmpdir, "file2.xlsx"), "wb") as f:
                f.write(self.get_second_output())
            with open(os.path.join(tmpdir, "file3.pdf"), "wb") as f:
                self.get_third_output(f)
```

By default, the `temporary_path()` for a *Local Target* will return a temporary directory in the same location as the final output, in order to allow an atomic rename to place the files in the final location. In some situations, it may be preferable to force the `temporary_path()` into a different location. For example, if the output is being created by a *Docker Task* that runs a Docker container then it is likely that the output directory will need to be mapped to a subdirectory underneath `/tmp`:

```python
class GenerateInputFilesUsingDocker(DockerTask):

    def output(self):
        # Note use of trailing slash to indicate a directory
        path = f"{self.task_id}/"
        # Make sure that temporary_path() is in /tmp so that the DockerTask can mount it
        backend_class = luigi.configuration.get_config().get("kiluigi.targets.delegating.IntermediateTarget", "backend_class")
        root_path = os.path.join(tempfile.gettempdir(), self.task_id) if backend_class == "kiluigi.ExpiringLocalTarget" else None
        return IntermediateTarget(path=path, root_path=root_path)

    def run(self):
        with.self.output().temporary_path() as tmpdir:
            self._volumes.append([tmpdir, "out_dir"])
            super().run()
```

## Running a Task

In an Python shell, preferably in a virtual env, run:

```python
import kiluigi
output = kiluigi.run("tasks.ScrapeFacebook", local_scheduler=True)
df = output.open().read()
df.head()
```

Parameters can also be passed:

```python
import kiluigi
df = kiluigi.run("tasks.ScrapeFacebook", since="2018-10-01", local_scheduler=True).open().read()
```

## Tips and Tricks

* If you are using the *Jupyter* shell, use autoreloading while developing code:

```
jupyter console
```

Then:

```
%load_ext autoreload
%autoreload 2
import datetime
import luigi

# Set configuration, including for parent tasks
config = luigi.configuration.get_config()
config.set('core', 'local_scheduler', 'True')
config.set('SurveyMonthlyR7Task', 'date', '2017-07-05')

# Import the Task
from  remote_monitoring.monthly.tasks.output import FacilityDisruptionGraph

# And run it
luigi.build([FacilityDisruptionGraph(area=4)])

```
