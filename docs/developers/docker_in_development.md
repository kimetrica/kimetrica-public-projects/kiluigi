# Using Docker in development

This document provides basic instructions to setup and use Docker containers to run and develop models.
Before starting, make sure your local git clone of the [darpa repo](https://gitlab.kimetrica.com/DARPA/darpa) is up to date with the master branch.

If you are not already familiar with Docker, please read through the Brief Introduction to understand the different functionalities to ensure you can work with it effectively.


## Brief Introduction to Docker
<div style="text-align:center" markdown="1"><img src="https://us.v-cdn.net/5019796/uploads/editor/6r/y5a5yat0scob.png" width="50%" height="50%"></div>

Docker is built on the concept of "containerization", which performs [operating-system-level virtualization](https://medium.freecodecamp.org/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b) similar to a Virtual Machine (but lighter weight), and isolates dependencies for a program or app. In order to launch a Docker container, it needs an *Image*, which is basically a collection of (re-usable) layers. Every Docker image is made up from a base image (for example Ubuntu or Debian), which layers added that change this base image. This way, common layers of different Docker images can be re-used. Docker images are most commonly built from a `Dockerfile`, that contains instructions for Docker how to build that specific image. Those instructions can add files, install packages  (i.e. Numpy, Pandas) or do other things that customize the base image, like creating users or setting environment variables. Usually every instruction results in a new layer.

## Running Docker containers

To view available images, run `docker images` in a terminal. To start running a container from an Image, use the command `docker run -ti <Image name>` (the `-ti` switches make sure that you can interact with the container, otherwise it wouldn't accept any input). If the image does not exist locally, Docker will automatically try to download (pull) it.
To stop a container, stop the process that is running inside it (by pressing `CTRL+c` or running `exit` if you are in a shell). The container will stick around until it's removed by `docker rm <container name>` (you can also pass `--rm` to `docker run` to have the container automatically be removed once the main process inside the container exits). To restart a stopped container, the user needs to execute `docker start <container name>` in the terminal. **Note that `docker run` creates a *new* container from the Image and starts the container, so you can create multiple copies of containers (with different names and IDs) from the same Image. On the other hand, `docker start` is used to re-launch a stopped container.**

If you want to override the process that a specific container is started with by default, for example to interact with the container, you can specify a different "entrypoint", by running e.g. `docker run -ti --rm --entrypoint=bash <Image name>`.

For more in-depth introduction to Docker, check out this [article](https://towardsdatascience.com/learn-enough-docker-to-be-useful-b7ba70caeb4b)

## Brief introduction to docker-compose

<img src="https://cdn-images-1.medium.com/max/800/1*LHq5mhynSjYBIhfgY3czkQ.png"  width="40%" height="40%">

`docker-compose` is a tool to configure and run multi-container Docker setups during development. It can take care of things like starting containers in the right order with the appropriate environment variables set and establishing links between them, all configured by a convenient file-based interface.
By convention, `docker-compose` looks for a file called `docker-compose.yml`, that contains instructions on how to run containers. If a file called `docker-compose.override.yml` exists, that file is also automatically loaded. This way it's possible to do things like mounting local folders into the containers during development by using the `docker-compose.override.yml` file, and running a version similar to production when this file is not used (by passing a configuration file explicitly using the `-f` switch).
`docker-compose` configuration files should contain a section for each service (container) that is supposed to run. Each section needs to contain either an `image` key or instructions how `docker-compose` can build the image for that container.
If a file called `.env` exists, it will automatically be loaded by `docker-compose` and you can then pass environment variables from this file to the containers that are being run.

For a more in-depth introduction to `docker-compose`, check out this [article](https://docs.docker.com/compose/overview/)

Usually you will want to run your containers with `docker-compose` and not `docker run` because it will take care of things like setting environment variables and setting up mounts. It will also simplify some things, for example you don't have to pass the `-ti` switches to be able to interact with a running container.

To attach to an already running container, you can use `docker ps` or `docker-compose ps`. To run bash in that running container execute either `docker exec -it <container_name> /bin/bash` or `docker-compose exec <service_name> /bin/bash`.

If the user wants to see ALL containers including stopped and running containers, use `docker ps -a` instead. If there's a need to remove an image, remove the container that runs on the image first (`docker rm <container ID> `), then do `docker image rm <image id>`.

## Setting up Docker and Docker compose

To install Docker, follow the [instructions](https://docs.docker.com/install/) on the Docker website according to your operating system.

Check the installation by running `docker run hello-world`. If you get a permission denied error try to follow [these instructions](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user) (for Ubuntu). After confirming that Docker is installed, install Docker Compose with `sudo apt get docker-compose` (for Ubuntu).

## Running the Darpa container

To run the Docker setup from the DARPA monorepo, first copy the `env.example` file in the darpa root directory, and save it as `.env`.  Make sure the fields `LUIGI_CKAN_USERNAME`, `LUIGI_CKAN_PASSWORD`, and `LUIGI_CKAN_ADDRESS` are updated in  `.env`. Environment variables and credentials set in this file will be available within `docker-compose` automatically. To have access to Jupyter Lab, in `.env`, set `COMPOSE_FILE=kiluigi/docker-compose.yml:kiluigi/docker-compose.override.yml:docker-compose.darpa.yml:docker-compose.jupyter.yml`.

Note that there's a file called `luigi.cfg.example` in the darpa root directory. This file should be automatically copied as `luigi.cfg` when the container is being started. If the user doesn't intend to start up the containers normally but instead wants to use `docker-compose run`, then a copy called `luigi.cfg` should be created manually before running a Docker container.  It's recommended to use the default values as they are designed specifically for the darpa container. However, if the user decides to run the darpa repo locally (i.e, conda env), then `luigi.cfg` would need to be modified, where `${}` values would need to be changed to individual credentials. For running in local environment, please refer to this link [HERE](https://gitlab.kimetrica.com/data-lab/kiluigi/blob/master/docs/developers/installing.md).

To start the whole setup including the UI, the current best practice is as follows (in the termina)l:
 1. `docker-compose -f kiluigi/docker-compose.yml build controller && docker-compose build --no-cache controller` first. This will first bypass the `docker-compose.darpa.yml` file to build the kiluigi image without modifications, and then build it again with the darpa specific Dockerfile included. 
 2. If the `.env` file is set up for Jupyter Lab, then navigate to the notebook [repo](https://gitlab.com/kimetrica/darpa/notebooks/-/tree/master/) directory, and execute `COMPOSE_PROJECT_NAME=drp docker-compose build`. If `.env` is not set up for Jupyter Lab, then skip this step and move on to step 3.
 3. Return to `DARPA/darpa root`, and start the image by`docker-compose up -d`.
 4. Check the names of the running container using `docker-compose ps` (usually the container to run luigi module is named "drp_controller_1"), then enter the container by executing `docker exec -ti <container_name> bash`. This should start container for the service named *controller* (e.g "drp_controller_1"). 
 5. To spin up Jupyter lab, look for `drp_jupyter_1` under `docker-compose ps`, and find the IP portal it's mapped to. In a browser, enter url `localhost:<IPportal>` (e.g. localhost:12345).

In the Docker container of the Darpa repo, the `PYTHONPATH` is already set when `docker-compose` is being used, and  every change you make in your editor is immediately reflected inside the container (and vice versa) because the user's local darpa directory is mounted into the container (as specified in the `docker-compose.override.yml` file).

If you have started an interactive container using the command above, you can now interact with the environment, run pipelines etc.

To run a pipeline, enter into the container "drp_controller_1", and you can for example execute `luigi --module models.malnutrition_model.tasks models.malnutrition_model.tasks.MalnutritionInferenceGeoJSON --local-scheduler` inside the root darpa directory of the container. To exit out of the Docker container, simply use `exit` in the container environment terminal. 

To attach to an already running container and re-enter a container, you can use `docker ps` or `docker-compose ps` to find your container. If the user wants to see ALL containers including stopped and running containers, use `docker ps -a` instead. If there's a need to remove an image, remove the container that runs on the image first (`docker rm <container ID> `), then do `docker image rm <image id>`.

To stop and remove all docker containers, execute ` docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)` in the terminal. To remove networks, build cache, and volume (optional), use `docker system prune` which will remove all images (both dangling and unreferenced). If the package requirements or Dockerfiles have been updated, it is a good practice to manually remove Docker images by calling `docker rmi $(docker images)`, then rebuild from scratch.

## Installing new packages

To view list of packages in Conda base environment (there's only one environment now), use `conda list`. The user can edit scripts either inside the container by using `vi` or `nano`, or just use their normal editor outside of the container because the source code files in the darpa repo are mounted into the container.

If the user wants to add a new package inside the container, they can use `conda install` or `pip install` only if a package is not available via conda. The newly installed package will persist and be available in this local container until it is removed by` docker[-compose] rm` or `docker-compose down`, so you can work with the package immediately in the current container. Freshly spawned containers will not have the new package installed, so to update the requirement/spec sheet for the Image,  run `pip freeze|grep -i <package_name>`, copy and paste the returned package name +version into  *base.conda*  inside `kiluigi/requirements `. If the package was installed via `pip install`, then the package name + version should be added to *base.pip*. The updated requirements can then be part of a merge request targeting the master branch.

**As mentioned on the previous discussion, every time a data scientist adds or upgrades a package to *base.conda*  or *base.pip* , he/she should mention that on Podio and tag Jesaja Everling.** This practice will help other data scientist/programmers to stay current with changes made to the Docker Image.

 If the user wants to have the package available in freshly spawned containers, do a rebuild with `docker-compose -f kiluigi/docker-compose.yml build controller && docker-compose build --no-cache controller`. After rebuilding, run it again following the steps mentioned above, and it will generate a **new** container from the updated image.


## Issues/bugs

This section is for reporting issues and bugs with the usage of Darpa docker.
After working inside Docker container, the user can exit and stop the running container before git pushing to the remote darpa repo. If there's a permission error in the local environment, run `sudo chown -Rc $UID .git/` before doing `git add` and `git commit`.

When editing files inside Docker container, user can access the files outside of the container by running this command after existing the container: `sudo chown -vR <username>:<username> ~/darpa`. Also use this command if there's error[13] 
on permission to write to file. For example, if user is running in a cloud instance, some files are being created by root, and the user might not have permission to those files. `sudo chown -vR <username>:<username> <dir>` will take care of that problem.