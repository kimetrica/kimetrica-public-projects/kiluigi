Tests
-----

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :caption: Diagram of the task and target test DAG.

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :omit_tasks:
    :caption: Diagram of the task and target DAG omitting tasks.

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :omit_outputs:
    :caption: Diagram of the task and target DAG omitting output nodes.

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :tags: foo bar
    :caption: Diagram including all nodes with the tag ``foo`` or ``bar``.

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :bases: kiluigi.tasks.Task kiluigi.targets.LocalTarget
    :caption: Diagram including only subclasses of :class:`kiluigi.tasks.Task` or :class:`LocalTarget`.

.. inheritance-diagram:: tests.integration.docs.tasks.Foo
    :caption: An inheritance diagram for :class:`tests.integration.docs.tasks.Foo`

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :bases: kiluigi.tasks.WrapperTask kiluigi.targets.Target
    :tags: foo bar
    :caption: A diagram combining base class and tag filters.

.. pipeline-diagram:: tests.integration.docs.tasks.Foo
    :bases: kiluigi.tasks.WrapperTask kiluigi.targets.Target
    :tags: foo bar
    :omit_outputs:
    :caption: A diagram combining base class, tag and omit_outputs filters.

.. automodule:: tests.integration.docs.tasks
    :members:
