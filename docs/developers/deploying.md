# Deployment

## Introduction

Projects that use kiluigi can be deployed to a centralized server using Docker.

The kiluigi library itself contains a set of Docker Compose files that can be used to
deploy a group of Docker Containers consisting of a Postgresql database server (to
host the Luigi Task History), a Scheduler (to run the central Luigid scheduler) and
one or more Workers (to actually run the Tasks).

The Scheduler container runs the `luigid` process, which includes the scheduler and
the Luigi web interface that allows the environment to be monitored.

The Worker containers run `luigi Task --assistant` in order to create a pool of
Workers capable of executing any Task submitted to the Luigi Scheduler.

The Controller runs the *Valve* Django application which provides a REST API
for controlling Pipeline runs.

The Celery containers provide a mechanism for the Valve application to execute Luigi
Tasks asynchronously.

## Configuration

Normally, the Docker Compose files live in the project root. However, for
kiluigi-based projects the Docker Compose files live in the `/kiluigi`
sub-module so that they can be standardized across projects.

Docker Compose can be forced to use the non-standard location using the `COMPOSE_FILE`
environment variable. The `COMPOSE_PROJECT_NAME` can also be set in order to
allow multiple kiluigi-based projects to co-exist.

Normally this will be set in a `.env` file stored in the project root and used to
configure the Docker Compose environment.

For example:

```
APP=ppl
ENV=dev
COMPOSE_PROJECT_NAME=ppl
COMPOSE_FILE=kiluigi/docker-compose.yml:kiluigi/docker-compose.ports.yml
```

All projects should contain a `env.example` file in the project root. Copying this file
to `.env` and editing the contents as required should enable the correct Docker Compose
setup.
