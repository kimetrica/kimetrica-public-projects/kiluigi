## Adding new dependencies

The project uses [conda](https://docs.continuum.io/anaconda/), so the preferred way to add new dependencies is to add them to `base.conda` - or `base.pip` if the package is not available via _conda_ (if it's a testing dependency `test.pip` is the appropriate place).

Packages installed into a running Docker container via `conda install` or `pip install` will persist and be available in this local container until it is removed by`docker[-compose] rm` or `docker-compose down`, so you can work with the package immediately in the current container. Freshly spawned containers will not have the new package installed, so it has to be added to the appropriate requirements file and the images have to be rebuilt.
To add a new dependency to the requirements files, it's important to create a clean branch that only adds that dependency, so it can be merged into `master` as soon as possible, to make it available to everybody that might need the same library.

To do this, the following approach should work:

To add a new dependency, create a fresh branch only for that purpose based on `master` in `kiluigi`, with an appropriate name, e.g. `git checkout -b 13123-add-behave-package`.

Determine the correct version of your dependency and add it to the appropriate file.

- Start a bash shell inside of the `controller` Docker container: `docker-compose run --entrypoint=bash controller`

- Search if the package you are looking for is available via _conda_, e.g. `conda search --channel defaults --channel conda-forge behave`

If it is, add the dependency with version specification to `base.conda`, e.g. `behave==1.2.6`.
If not, try to search it via _pip_, e.g. `pip search behave`, and add it to the appropriate _pip_ requirements file. Each dependency should only exist either in the _conda_ requirements (preferably) or in _pip_, but never in both.

You could already install and use the dependency in this container, to have it installed into freshly spawned containers you need to rebuild your Docker images by running `docker-compose build` (after stepping out of the running container by pressing `CTRL+d`)

Now you can commit to your dependency branch and push:

`git add requirements/base.conda`
`git commit -m "Added behave package to base.conda - see #13123"`
`git push --set-upstream origin feature/13123-add-behave-package`

Gitlab will show you an URL that you can use to create a merge request. Assign this merge request to someone and post a note on Podio about the new dependency and why it was added once it's merged.

You can now use the new dependency (even before it is merged to master) in your feature/defect branch, whether it already exists or is new, by checking out that branch and then merging the "dependency addition" branch like this: `git merge feature/13123-add-behave-package`.

Using this approach, the package will be both in a clean branch that can easily and quickly be merged into master, and in your branch that you can continue to work on with the new dependency in place.

Every time a new package has been added and the relevant commit has been merged to master, a post on Podio should be created that explains which package was added and why. This practice will help other data scientist/programmers to stay current with changes made to the Docker Image.
