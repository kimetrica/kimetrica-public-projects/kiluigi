|ProjectName| Contents
======================

.. toctree::
   :maxdepth: 2

   index
   standards/index
   developers/index

   faq


Indices and tables
==================

.. only:: builder_html

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

.. only:: not builder_html

   * :ref:`modindex`
