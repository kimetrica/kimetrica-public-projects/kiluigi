import itertools
import logging
import os
import tempfile
from contextlib import contextmanager
from io import TextIOWrapper
from typing import Dict, Optional

import botocore
from awscli.clidriver import create_clidriver
from luigi.contrib.s3 import (
    S3_DIRECTORY_MARKER_SUFFIX_0,
    S3_DIRECTORY_MARKER_SUFFIX_1,
    AtomicS3File,
    FileNotFoundException,
    ReadableS3File,
    S3Client,
    S3Target,
)
from luigi.format import BaseWrapper
from luigi.target import FileAlreadyExists

from ..utils import name_from_class
from .local_target import NamedAtomicLocalFile

logger = logging.getLogger("luigi-interface")


def aws_cli(*cmd):
    """
    Run a command using the AWS CLI

    There are some operations that are supported by the AWS CLI that are not
    supported directly by boto3, notably directory sync.

    See https://github.com/boto/boto3/issues/358#issuecomment-372086466
    """
    old_env = dict(os.environ)
    try:
        # Environment
        env = os.environ.copy()
        env["LC_CTYPE"] = "en_US.UTF"
        os.environ.update(env)
        # Run awscli in the same process
        exit_code = create_clidriver().main(cmd)

        # Deal with problems
        if exit_code > 0:
            raise RuntimeError("AWS CLI exited with code {}".format(exit_code))
    finally:
        os.environ.clear()
        os.environ.update(old_env)


def set_s3_tags(client, bucket, key, tags):
    logger.debug(f"Setting tags on {bucket}:{key} to {tags}")

    # Get any existing tags
    tagset = client.s3.meta.client.get_object_tagging(Bucket=bucket, Key=key)["TagSet"]
    # Convert from a list of Key, Value dicts provided by boto3 to a single dict
    tagset = {tag["Key"]: tag["Value"] for tag in tagset}

    # Update with changes
    tagset.update(tags)

    # Convert from a single dict to a list of Key, Value dicts as required by boto3
    tagset = [{"Key": k, "Value": v} for k, v in tagset.items()]
    client.s3.meta.client.put_object_tagging(
        Bucket=bucket, Key=key, Tagging={"TagSet": tagset}
    )


class EnhancedS3Client(S3Client):
    """
    Enhanced S3Client that has improved compatibility with MinIO and the LocalTarget API.
    """

    DEFAULT_PART_SIZE = 8388608
    DEFAULT_THREADS = 100

    @staticmethod
    def _get_s3_config(key=None):
        """
        Some options need special configuration.
        """
        # Need arguments to super() because this is a staticmethod
        section_only = super(EnhancedS3Client, EnhancedS3Client)._get_s3_config(key)

        # Some options need to be passed as a specific Config object
        config = {}
        if "connect_timeout" in section_only:
            config["connect_timeout"] = int(section_only.pop("connect_timeout"))
        if "max_retries" in section_only:
            config["retries"] = {"max_attempts": int(section_only.pop("max_retries"))}
        if config:
            section_only["config"] = botocore.client.Config(**config)

        return section_only

    def isdir(self, path) -> bool:
        """
        Is the parameter S3 path a directory?

        Fix S3Client.isdir so that it works with MinIO, which raises an Exception when
        the path contains consecutive directory separators, by only appending a directory
        separator suffix to the path if it doesn't already end with one.
        """
        (bucket, key) = self._path_to_bucket_and_key(path)

        s3_bucket = self.s3.Bucket(bucket)

        # root is a directory
        if self._is_root(key):
            return True

        for suffix in (S3_DIRECTORY_MARKER_SUFFIX_0, S3_DIRECTORY_MARKER_SUFFIX_1):
            try:
                self.s3.meta.client.get_object(
                    Bucket=bucket,
                    # Only add the suffix to the current key if it doesn't already end in the suffix,
                    # because MinIO raises an exception if there are duplicate path separators, whereas
                    # S3 itself doesn't mind.
                    Key=key + (suffix if not key.endswith(suffix) else ""),
                )
            except botocore.exceptions.ClientError as e:
                if not e.response["Error"]["Code"] in ["NoSuchKey", "404"]:
                    logger.exception(
                        "Raising exception: %s getting %s from %s",
                        e,
                        key + suffix,
                        bucket,
                    )
                    raise
            else:
                return True

        # files with this prefix
        key_path = self._add_path_delimiter(key)
        s3_bucket_list_result = list(
            itertools.islice(s3_bucket.objects.filter(Prefix=key_path), 1)
        )
        if s3_bucket_list_result:
            return True

        return False

    def move(self, source_path, destination_path, raise_if_exists=False, **kwargs):
        """
        Rename/move an object from one S3 location to another.

        Add support for raise_if_exists for compatiblity with the LocalTarget API

        :param source_path: The `s3://` path of the directory or key to copy from
        :param destination_path: The `s3://` path of the directory or key to copy to
        :param kwargs: Keyword arguments are passed to the boto3 function `copy`
        """
        if raise_if_exists and self.exists(destination_path):
            raise FileAlreadyExists()
        return super().move(source_path, destination_path, **kwargs)

    def copy(
        self,
        source_path,
        destination_path,
        raise_if_exists=False,
        threads=DEFAULT_THREADS,
        start_time=None,
        end_time=None,
        part_size=DEFAULT_PART_SIZE,
        **kwargs,
    ):
        """
        Add support for raise_if_exists for compatiblity with the LocalTarget API
        """
        if raise_if_exists and self.exists(destination_path):
            raise FileAlreadyExists()
        return super().copy(
            source_path,
            destination_path,
            threads,
            start_time,
            end_time,
            part_size,
            **kwargs,
        )


class TaggableAtomicS3File(NamedAtomicLocalFile, AtomicS3File):
    """
    An S3 file that writes to a temp file and puts to S3 on close and that can also save metadata as tags

    :param kwargs: Keyword arguments are passed to the boto function `initiate_multipart_upload`
    """

    tags: Dict[str, str] = {}

    def move_to_final_destination(self):
        """
        Upload the file and then set the Tags on an S3 object from a dict
        """
        try:
            logger.debug(f"Uploading '{self.tmp_path}' to {self.path}")
            super().move_to_final_destination()

            if self.tags:
                bucket, key = self.s3_client._path_to_bucket_and_key(self.path)
                # Because S3 is distributed, the object may not be retrievable yet, even though
                # move_to_final_destination worked.
                waiter = self.s3_client.s3.meta.client.get_waiter("object_exists")
                waiter.wait(
                    Bucket=bucket, Key=key, WaiterConfig={"Delay": 1, "MaxAttempts": 20}
                )
                set_s3_tags(self.s3_client, bucket, key, self.tags)

        except Exception as e:
            logger.exception(
                "Failed uploading '%s' to %s: %s" % (self.tmp_path, self.path, e)
            )
            raise

        self.directory.cleanup()


class NewlineCapableReadableS3File(ReadableS3File):
    """
    An ReadableS3 file that can handle negative arguments to read, as provided by luigi.format.NewlineWrapper
    """

    def read(self, size=None):
        if isinstance(size, int) and size < 0:
            # Boto3 doesn't support negative sizes the same that io.BytesIO, etc. do
            size = None

        return super().read(size)

    def flush(self):
        pass


class TaggableS3Target(S3Target):
    """
    A better Luigi S3Target that supports additional metadata and a flag-file similar to luigi.contrib.s3.S3FlagTarget
    """

    PREFIX = "kiluigi"

    tags: Dict[str, str] = {}

    flag: Optional[str] = None

    def __str__(self):
        return f"{name_from_class(self).split('.')[-1]}('{self.path}')"

    def __init__(self, path, format=None, client=None, **kwargs):  # noqa: A002
        # If the path is a directory, then define a flag-file to indicate when the Target exists
        if path[-1] == "/":
            self.flag = kwargs.pop("flag", "_SUCCESS")

        # Use the enhanced S3Client if one hasn't been provided
        client = client or EnhancedS3Client()

        # Ensure we have an instance variable
        self.tags = {}

        super().__init__(path, format, client, **kwargs)

    @property
    def last_modified_datetime(self):
        """
        Gets the last modified date of a TaggableS3Target
        """
        (bucket, key) = self.fs._path_to_bucket_and_key(self.path)
        return self.fs.s3.meta.client.head_object(Bucket=bucket, Key=key)[
            "LastModified"
        ]

    def exists(self):
        if self.fs.isdir(self.path):
            semaphore = self.path + self.flag
            return self.fs.exists(semaphore)
        # No flag attribute, so just a regular file and not a directory
        return super().exists()

    def open(self, mode="r"):  # noqa: A003
        if mode not in ("r", "w"):
            raise ValueError("Unsupported open mode '%s'" % mode)

        logger.debug("Getting S3 pipe")
        if mode == "r":
            s3_key = self.fs.get_key(self.path)
            if not s3_key:
                raise FileNotFoundException("Could not find file at %s" % self.path)

            fileobj = NewlineCapableReadableS3File(s3_key)
            return self.format.pipe_reader(fileobj)
        else:
            return self.format.pipe_writer(
                TaggableAtomicS3File(self.path, self.fs, **self.s3_options)
            )

    def copy(self, new_path, raise_if_exists=False):
        self.fs.copy(self.path, new_path, raise_if_exists)

    def move(self, new_path, raise_if_exists=False):
        self.fs.move(self.path, new_path, raise_if_exists=raise_if_exists)

    def get_tags(self, path):
        tags = self.tags.get(path, {})
        if not tags:
            try:
                (bucket, key) = self.fs._path_to_bucket_and_key(path)
                tagset = self.fs.s3.meta.client.get_object_tagging(
                    Bucket=bucket, Key=key
                )["TagSet"]
                # Convert from a list of Key, Value dicts provided by boto3 to a single dict
                tags = self.tags[path] = {tag["Key"]: tag["Value"] for tag in tagset}
            except self.fs.s3.meta.client.exceptions.NoSuchKey:
                tags = {}

        return tags

    def get_metadata_name(self, name: str):
        """
        Get the full name of an metadata attribute.
        """
        name = ".".join([self.PREFIX, name])
        return name

    def set_metadata(self, path, name: str, value: str):
        """
        Set the metadata using S3 Tags
        """
        if isinstance(path, (BaseWrapper, TextIOWrapper)):
            path = path._stream
        if isinstance(path, TaggableAtomicS3File):
            path.tags[self.get_metadata_name(name)] = value
        else:
            # Not a file, so assume a path to an existing S3 object
            if self.fs.isdir(path):
                # S3 doesn't have real folders, so we need to set the metadata
                # on an real S3 object in the folder
                path += "_METADATA"
                if not self.fs.exists(path):
                    self.fs.put_string("", path)
            bucket, key = self.fs._path_to_bucket_and_key(path)
            set_s3_tags(self.fs, bucket, key, {self.get_metadata_name(name): value})
            self.tags = {}  # Force refresh of tags the next time they are read.

    def get_metadata(self, path: str, name: str) -> Optional[str]:
        """
        Get the metadata from S3 Tags
        """
        assert isinstance(path, str)
        if self.fs.isdir(path):
            # S3 doesn't have real folders, so we need to set the metadata
            # on an real S3 object in the folder
            path += "_METADATA"
        try:
            return self.get_tags(path)[self.get_metadata_name(name)]
        except KeyError:
            return None

    @contextmanager
    def temporary_path(self):
        """
        A context manager that enables a reasonably short, general and
        magic-less way to solve the :ref:`AtomicWrites`.
         * On *entering*, it will create a local temporary directory and return a path
           in that directory that can be used as a file or as a subdirectory
         * On *exiting*, it will move the temporary file or directory to S3. If the temporary
           path is a directory then it will write the flag file after transferring all the other files
        """
        with tempfile.TemporaryDirectory() as tmpdirname:
            _temp_path = os.path.join(tmpdirname, "luigi-tmp")
            yield _temp_path

            # We won't reach here if there was an user exception.
            if os.path.isdir(_temp_path):
                assert (
                    self.flag
                ), f"Can't sync from '{_temp_path}' to '{self.path}' because {name_from_class(self)} is not a directory target"  # NOQA: E501
                cmd = ["s3", "sync", _temp_path, self.path, "--delete"]
                # Add S3 configuration options, such as a custom endpoint or credentials
                # but excluding complex objects
                for param, value in self.fs._options.items():
                    if not isinstance(value, botocore.client.Config):
                        cmd.append(f'--{param.replace("_", "-")}')
                        cmd.append(value)
                aws_cli(*cmd)
                # S3 doesn't have real folders that we can atomically rename, so to show completeness
                # we use a real S3 object in the folder as a flag file.
                self.fs.put_string("", self.path + self.flag)
            else:
                self.fs.put(_temp_path, self.path, **self.s3_options)

        # After we return, then set the expiry metadata on the final path
        if hasattr(self, "set_expiry"):
            self.set_expiry(self.path)
