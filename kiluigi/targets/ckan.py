"""
Luigi FileSystem and Target for accessing files stored on CKAN.

Follows a similar logic to S3Target and GoogleDriveTarget.

Also supports get/put operations for compatiblity with legacy Tasks.
"""

import datetime
import io
import logging
import mimetypes
import os
import posixpath
import random
import tempfile
from contextlib import contextmanager
from typing import Optional, Tuple
from urllib.parse import urlsplit, urlunsplit
from uuid import UUID

import luigi
import pytz
import requests
from luigi.format import FileWrapper
from luigi.target import (
    FileAlreadyExists,
    FileSystem,
    FileSystemException,
    FileSystemTarget,
    MissingParentDirectory,
)
from slugify import slugify

from ..utils import get_config_value, timeit
from .local_target import NamedAtomicLocalFile

logger = logging.getLogger("luigi-interface")

try:
    from ckanapi import RemoteCKAN
    from ckanapi.errors import NotFound, ValidationError
except ImportError:
    logger.warning(
        "Loading ckan module without the python package ckanapi. \
        This will crash at runtime if CKAN functionality is used."
    )

# Number of times to retry failed downloads.
NUM_RETRIES = 5

# Number of bytes to send/receive in each request.
CHUNKSIZE = 10 * 1024 * 1024

# Mimetype to use if one can't be guessed from the file extension.
DEFAULT_MIMETYPE = "application/octet-stream"


def is_valid_uuid(uuid_to_test, version=4):
    """
    Check if uuid_to_test is a valid UUID.

     Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

     Returns
    -------
    `True` if uuid_to_test is a valid UUID, otherwise `False`.

     Examples
    --------
    >>> is_valid_uuid('c9bf9e57-1685-4c89-bafb-ff5af830be8a')
    True
    >>> is_valid_uuid('c9bf9e58')
    False
    """

    try:
        uuid_obj = UUID(uuid_to_test, version=version)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test


class InvalidDeleteException(FileSystemException):
    pass


class _DeleteOnCloseFile(io.FileIO):
    _delete_on_close = True

    def close(self):
        super().close()
        if self._delete_on_close:
            try:
                os.remove(self.name)
            except OSError:
                # Catch a potential threading race condition and also allow this
                # method to be called multiple times.
                pass

    def readable(self):
        return True

    def writable(self):
        return False

    def seekable(self):
        return True


class CKANClient(FileSystem):
    """
    An implementation of a Luigi FileSystem over the CKAN API.

    Credentials for authentication can be supplied when the class is instantiated. If credentials are not supplied then
    this class will look for an API token in a [ckan] section in the global configuration (e.g. luigi.cfg). If the
    credentials are available in an environment variable then they can be made available via the luigi.cfg with a
    section like:

    [ckan]
    address=${CKAN_URL}
    token=${CKAN_TOKEN}

    If the CKAN server is version 2.9+ then an API token should be used. For earlier server versions the API key can be
    used.
    """

    _organization_hierarchy = {}

    def __init__(
        self,
        address=None,
        token=None,
        chunksize=CHUNKSIZE,
        **kwargs,
    ):
        self.chunksize = chunksize

        # Default configuration from environment and override as necessary
        config = get_config_value("ckan")

        # Use config values if nothing was passed in
        address = address or config.get("address")
        token = token or config.get("token")

        self.client = RemoteCKAN(address, token)

    def exists(self, path):
        return any(
            [
                bool(self.get_resource_from_path(path)),
                bool(self.get_dataset_from_path(path)),
                bool(self.get_organization_from_path(path)),
            ]
        )

    @property
    def organization_hierarchy(self):
        """
        Get the organization hierarchy for the CKAN server.

        Organizations in CKAN exist in a hierarchy.  However, the existing API doesn't return the parent or child
        organizations as part of the organization list responses. Thereore we build the hierarchy programmatically,
        using the Groups (which is the same model used for Organizations) that the organization is a member of.
        """
        if not self._organization_hierarchy:
            # Get the organization list, including group memberships
            with timeit() as t:
                organizations = self.client.action.organization_list(
                    all_fields=True, include_groups=True
                )
            logger.debug(
                "Fetched %d organizations from %s in %ss",
                len(organizations),
                self.client.address,
                t.elapsed.total_seconds(),
            )
            # Convert the list to a dict
            organizations = {
                organization["name"]: organization for organization in organizations
            }
            # Save an empty children attribute
            for name in organizations:
                organizations[name]["children"] = []
            # Populate the children / parent attributes, using group memberships
            for name, organization in organizations.items():
                try:
                    parent = organization["groups"][0]["name"]
                    organizations[name]["parent"] = parent
                    organizations[parent]["children"].append(name)
                except IndexError:
                    # The organization isn't a member of any Group
                    organizations[name]["parent"] = None
            self._organization_hierarchy = organizations
        return self._organization_hierarchy

    def get_organization_from_path(self, path: str, create_folders: bool = False):
        """
        Convert a path potentially containing subfolders to an organization hierarchy in CKAN.

        By default find an existing organization, but optionally (if create_folders=True) create an organization
        to match the path.
        """
        # Remove the server address from the path.
        if path.startswith(self.client.address):
            path = path[(len(self.client.address)) :]
        components = path.strip(posixpath.sep).split(posixpath.sep)
        parent = None
        while components:
            component = components.pop(0)
            organization_name = next(
                iter(
                    [
                        organization["name"]
                        for organization in self.organization_hierarchy.values()
                        if (
                            organization["display_name"] == component
                            or organization["name"] == component
                        )
                        and organization["parent"] == parent
                    ]
                ),
                None,
            )
            if not organization_name:
                # There is no organization for this parent with the correct display name, so create one
                if create_folders:
                    params = {
                        "name": slugify(component, max_length=100),
                        "title": component,
                        "groups": (
                            [{"capacity": "public", "name": parent}] if parent else []
                        ),
                    }
                    with timeit() as t:
                        try:
                            organization = self.client.action.organization_create(
                                **params
                            )
                        except ValidationError:
                            # A group with the same slug already exists, possibly with a different parent organization,
                            # so append a timestamp to create a unique name
                            params["name"] += "-" + datetime.datetime.now(
                                datetime.timezone.utc
                            ).strftime("%Y%m%d%H%M%S%f")
                            organization = self.client.action.organization_create(
                                **params
                            )
                        except Exception:
                            logger.exception(
                                "Failed to create organization %s for %s/%s",
                                params["name"],
                                self.client.address,
                                path,
                            )
                            raise
                    logger.debug(
                        "Created organization %s at %s in %ss",
                        params["name"],
                        self.client.address,
                        t.elapsed.total_seconds(),
                    )
                    # Save the organization in the local hierarchy
                    organization["children"] = []
                    organization["parent"] = parent
                    self.organization_hierarchy[organization["name"]] = organization
                    self.organization_hierarchy[parent]["children"].append(
                        organization["name"]
                    )
                    organization_name = organization["name"]
                else:
                    # No matching organization and we aren't allowed to create one.
                    return None
            parent = organization_name

        if parent:
            parent = self.organization_hierarchy[parent]

        return parent

    def get_dataset_from_path(self, path: str, create_folders: bool = False):
        """
        Convert a path potentially containing subfolders to a CKAN dataset id (also known as a package id).

        By default find an existing dataset id, but optionally (if create_folders=True) create a dataset to match
        the path.

        The components in the path map to an organization hierarchy in CKAN, with the final segment mapping
        to the dataset id.
        """
        organization_path, dataset_title_or_id = posixpath.split(
            path.strip(posixpath.sep)
        )
        if not dataset_title_or_id:
            raise ValueError("Cannot identify dataset from path '%s'" % path)
        query = []
        if is_valid_uuid(dataset_title_or_id):
            query.append(f"id:{dataset_title_or_id}")
        else:
            query.append(f'title:"{dataset_title_or_id}"')
        query.append("state:active")
        organization = None
        if organization_path:
            organization = self.get_organization_from_path(
                organization_path, create_folders=create_folders
            )
            if not organization:
                # We were expecting an organization from the path, but didn't find one and didn't create one.
                return None
            query.append(f'organization:{organization["name"]}')
        with timeit() as t:
            datasets = self.client.action.package_search(
                fq_list=query, include_private=True
            )
        logger.debug(
            "Found %d datasets for query %s at %s in %ss",
            len(datasets),
            query,
            self.client.address,
            t.elapsed.total_seconds(),
        )

        # action.package_search matches datasets where the search string is a substring,
        # due to SOLR.
        # e.g. `sudan-acute-food-insecurity-classification` matches
        # `south-sudan-acute-food-insecurity-classification`
        # We need to remove inexact matches
        datasets["results"] = [
            dataset
            for dataset in datasets["results"]
            if (
                dataset["title"] == dataset_title_or_id
                or dataset["id"] == dataset_title_or_id
            )
        ]
        logger.debug(
            "%d exact dataset matches for query %s after cleaning",
            len(datasets["results"]),
            query,
        )

        if len(datasets["results"]) == 0:
            if create_folders:
                params = {
                    "name": slugify(dataset_title_or_id, max_length=100),
                    "title": dataset_title_or_id,
                    "private": True,
                }
                if organization:
                    params["owner_org"] = organization["name"]

                with timeit() as t:
                    try:
                        dataset = self.client.action.package_create(**params)
                    except ValidationError:
                        # A dataset with the same name already exists, possibly with a different parent organization,
                        # so append a timestamp to create a unique name
                        params["name"] += "-" + datetime.datetime.now(
                            datetime.timezone.utc
                        ).strftime("%Y%m%d%H%M%S%f")
                        dataset = self.client.action.package_create(**params)
                    except Exception:
                        logger.exception(
                            "Failed to create dataset %s for %s",
                            params["name"],
                            path,
                        )
                        raise

                logger.debug(
                    "Created dataset %s (%s) at %s in %ss",
                    dataset["name"],
                    dataset["id"],
                    self.client.address,
                    t.elapsed.total_seconds(),
                )
                return dataset
            else:
                return None
        else:
            # One or more datasets found, return the first exact match in the results
            # (cleaned of substring matches above)
            return datasets["results"][0]

    def get_resource_from_path(self, path: str, create_folders: bool = False):
        """
        Convert a path potentially containing subfolders to a CKAN resource.

        The components in the path map to an organization hierarchy in CKAN, with the final two segments mapping
        to the dataset (also known as the package) and the resource.
        """
        dataset_path, resource_name_or_id = posixpath.split(path.strip(posixpath.sep))
        if not dataset_path and is_valid_uuid(resource_name_or_id):
            # Accessing a Resource directly by its uuid
            try:
                resource = self.client.action.resource_show(id=resource_name_or_id)
                return resource
            except NotFound:
                return None
        while dataset_path:
            dataset = self.get_dataset_from_path(
                dataset_path, create_folders=create_folders
            )
            if dataset:
                for resource in dataset["resources"]:
                    if (
                        resource["name"] == resource_name_or_id
                        or resource["id"] == resource_name_or_id
                    ):
                        return resource
            # We didn't find a dataset containing the resource, so maybe the final segment of the path
            # is actually part of the resource name rather than the dataset title.
            dataset_path, subfolder = posixpath.split(dataset_path)
            resource_name_or_id = posixpath.join(subfolder, resource_name_or_id)

        return None

    def get_object_from_path(self, path: str) -> Tuple[str, dict]:
        """
        Find a CKAN organization, dataset or resource matching a path and return a tuple of the item type
        ( organization, dataset or resource) and the object dict.
        """
        resource = self.get_resource_from_path(path)
        if resource:
            return ("resource", resource)
        dataset = self.get_dataset_from_path(path)
        if dataset:
            return ("package", dataset)
        organization = self.get_organization_from_path(path)
        if organization:
            return ("organization", organization)
        raise FileSystemException(
            "Cannot find CKAN organization, dataset or resource matching %s" % path
        )

    def get_id_from_path(self, path: str) -> Tuple[str, str]:
        ckan_type, ckan_object = self.get_object_from_path(path)
        return (ckan_type, ckan_object["id"])

    def put(
        self,
        local_path: str,
        destination_path: str,
        private: bool = True,
        mimetype: Optional[str] = None,
    ):
        if self.exists(destination_path):
            raise FileAlreadyExists()
        if os.path.isdir(local_path):
            # Make sure the destination path doesn't have a trailing directory separator, to that the
            # dataset title is identified correctly
            destination_path = destination_path.strip(posixpath.sep)
            # Build a list of resources to be uploaded
            resources = []
            upload = {}
            # Add the individual file to the resources list
            for root, dirs, files in os.walk(local_path):
                for name in files:
                    # Remove the root to find the subfolder path, and then strip the leading /
                    # because otherwise posixpath.join treats it as an absolute path
                    prefix_length = len(local_path)
                    subfolders = root[prefix_length:].strip(posixpath.sep)
                    resources.append(
                        {
                            "name": posixpath.join(subfolders, name),
                            "mimetype": mimetype
                            or mimetypes.guess_type(name)[0]
                            or DEFAULT_MIMETYPE,
                        }
                    )
                    upload[posixpath.join(subfolders, name)] = os.path.join(root, name)
        else:
            destination_path, name = posixpath.split(destination_path)
            # Keep the original name if the destination is a directory
            name = name or os.path.basename(local_path)
            resources = [
                {
                    "name": name,
                    "mimetype": mimetype
                    or mimetypes.guess_type(name)[0]
                    or DEFAULT_MIMETYPE,
                }
            ]
            upload = {name: local_path}

        dataset = self.get_dataset_from_path(destination_path)
        if dataset:
            if len(resources) > 1:
                # This point should never be reached because the check for self.exists(destination_path) at the top
                # of this function should have found an existing Dataset
                raise FileAlreadyExists
            # Package already exists, so update existing dataset with new resource
            resource = resources[0]
            resource["package_id"] = dataset["id"]
            with open(upload[resource["name"]], "rb") as fh:
                resource["upload"] = fh
                with timeit() as t:
                    resource = self.client.action.resource_create(**resource)
                logger.debug(
                    "Created resource %s (%s) at %s in %ss",
                    resource["name"],
                    resource["id"],
                    self.client.address,
                    t.elapsed.total_seconds(),
                )
        else:
            # There is no existing dataset, so create it with a temporary name and then rename to ensure atomicity
            path, dataset_title = posixpath.split(destination_path)
            # Create the organization(s) if necessary
            organization = None
            if path:
                organization = self.get_organization_from_path(
                    path, create_folders=True
                )
                if not organization:
                    # We were expecting an organization from the path, but didn't find one and didn't create one.
                    raise MissingParentDirectory(
                        "Cannot find or create organization matching %s" % path
                    )
            temp_title = dataset_title
            temp_title += "-luigi-tmp-%09d" % random.randint(0, 999999999)  # NOQA: S311
            params = {
                "name": slugify(dataset_title, max_length=100),
                "title": temp_title,
                "private": private,
            }
            if organization:
                params["owner_org"] = organization["name"]
            with timeit() as t:
                try:
                    dataset = self.client.action.package_create(**params)
                except ValidationError:
                    # A dataset with the same slug already exists, possibly with a different parent organization,
                    # so append a timestamp to create a unique name
                    params["name"] = (
                        params["name"][:85]
                        + "-"
                        + datetime.datetime.now(datetime.timezone.utc).strftime(
                            "%Y%m%d%H%M%S%f"
                        )
                    )
                    dataset = self.client.action.package_create(**params)
            logger.debug(
                "Created dataset %s (%s) at %s in %ss",
                dataset["name"],
                dataset["id"],
                self.client.address,
                t.elapsed.total_seconds(),
            )

            # Create the resources
            for resource in resources:
                resource["package_id"] = dataset["id"]
                with open(upload[resource["name"]], "rb") as fh:
                    resource["upload"] = fh
                    with timeit() as t:
                        resource = self.client.action.resource_create(**resource)
                logger.debug(
                    "Created resource %s (%s) at %s in %ss",
                    resource["name"],
                    resource["id"],
                    self.client.address,
                    t.elapsed.total_seconds(),
                )
            # Rename the dataset to the correct title
            with timeit() as t:
                self.client.action.package_patch(id=params["name"], title=dataset_title)
            logger.debug(
                "Patched dataset %s (%s) at %s in %ss",
                dataset["name"],
                dataset["id"],
                self.client.address,
                t.elapsed.total_seconds(),
            )

    def download(
        self,
        path,
        local_path: Optional[str] = None,
        chunksize: Optional[int] = None,
        chunk_callback=lambda _: False,
        mimetype: Optional[str] = None,
    ):
        """
        Downloads a file from CKAN given the file_id to the given local file, or to a temporary local file,
        and returns the local file for processing.

        Optionally stops after the first chunk for which chunk_callback returns True.
        """
        chunksize = chunksize or self.chunksize
        resource = self.get_resource_from_path(path)
        if not resource:
            raise FileSystemException("File %s not found" % path)
        with timeit() as t:
            response = requests.get(
                resource["url"],
                headers={"Authorization": self.client.apikey},
                stream=True,
            )
            response.raise_for_status()
            if local_path:
                with open(local_path, "wb") as fh:
                    for chunk in response.iter_content(chunk_size=chunksize):
                        fh.write(chunk)
                        if chunk_callback(fh):
                            break
                return fh

            # No local path provided, so use a temporary file. Note that we don't know how the calling process
            # will handle the file, so we use `delete=False` and leave it to the calling process to delete the file
            # when it is finished with it.
            # No local path provided, so use a temporary file.
            # Make sure to use the correct file extension if one is available
            suffix = os.path.splitext(resource["url"])[1] or None
            with tempfile.NamedTemporaryFile(suffix=suffix, delete=False) as fh:
                return_fh = _DeleteOnCloseFile(fh.name, "r")
                for chunk in response.iter_content(chunk_size=chunksize):
                    fh.write(chunk)
                    if chunk_callback(fh):
                        break

        logger.debug(
            "Downloaded resource %s (%s) from %s in %ss",
            resource["name"],
            resource["id"],
            self.client.address,
            t.elapsed.total_seconds(),
        )

        return return_fh

    def mkdir(self, path, parents=True, raise_if_exists=False):
        if raise_if_exists and self.exists(path):
            raise FileAlreadyExists()

        if not parents and not self.exists(os.path.dirname(path)):
            raise MissingParentDirectory()

        dataset = self.get_dataset_from_path(path, create_folders=True)
        return dataset

    def remove(self, path):
        ckan_type, ckan_object = self.get_object_from_path(path)
        # Removing individual resources is very slow (15+ seconds in some cases), so if this resource is the only one
        # in the dataset, remove the dataset instead (which takes < 1 second). The extra calls to check the package
        # are still much quicker than waiting for resource_delete.  However, if the path contains the dataset id
        # then we assume the target expects that dataset to always exist, and accept the slow resource_delete
        with timeit() as t:
            if ckan_type == "resource" and ckan_object["package_id"] not in path:
                dataset = self.client.action.package_show(id=ckan_object["package_id"])
                if dataset["num_resources"] == 1:
                    ckan_type = "package"
                    ckan_object = dataset
            self.client.call_action(f"{ckan_type}_delete", {"id": ckan_object["id"]})
        logger.debug(
            "Deleted %s %s (%s) from %s in %ss",
            ckan_type,
            ckan_object["name"],
            ckan_object["id"],
            self.client.address,
            t.elapsed.total_seconds(),
        )

    def purge_organization(self, organization_name):
        """
        Purge CKAN organization.

        Deleting an organization only markes it as deleted, but it will still exist.
        We can use an API endpoint to completely purge organizations from the system.

        We need to recursively delete all the child organizations and datasets before we can delete the parent,
        because if we delete the parent first then CKAN will convert the children to top-level organizations.
        """
        organization = self.organization_hierarchy[organization_name]
        for child_organization_name in organization["children"]:
            self.purge_organization(child_organization_name)
        logger.debug(
            "Trying to purge organization %s from %s",
            organization_name,
            self.client.address,
        )
        # Purge from CKAN
        self.client.call_action("organization_purge", {"id": organization["id"]})
        # Remove from the cached hierarchy
        del self.organization_hierarchy[organization_name]
        logger.debug(
            "Purged organization %s from %s",
            organization_name,
            self.client.address,
        )

    def move(
        self, source_path: str, destination_path: str, raise_if_exists: bool = False
    ):
        """
        CKAN doesn't support moving a resource from one package to another
        """
        raise NotImplementedError

    def copy(
        self,
        source_path: str,
        destination_path: str,
        raise_if_exists: bool = False,
    ):
        """
        CKAN doesn't support a server-side copy of packages.
        """
        raise NotImplementedError


class AtomicCkanFile(NamedAtomicLocalFile):
    """
    A file that writes to a temp file and puts to CKAN on close.
    """

    def __init__(
        self,
        path,
        ckan_client,
        mimetype: Optional[str] = None,
    ):
        self.ckan_client = ckan_client
        self.mimetype = mimetype
        super().__init__(path)

    def move_to_final_destination(self):
        self.ckan_client.put(
            self.tmp_path,
            self.path,
            mimetype=self.mimetype,
        )
        self.directory.cleanup()


class CkanTarget(FileSystemTarget):
    """
    A Target that stores data in a file in CKAN.

    path: The path to the file in CKAN, typically consisting of a root organization and optionally
          one or more suborganizations and a dataset name and/or the name of a file, for example:

        The components in the path map to an organization hierarchy in CKAN, with the final one or two segments mapping
        to the dataset id and the resource id. The dataset and the resource can be represented as either the name of
        the object or the dataset or resource id.

        Consequently, the following are all valid paths:

        * https://data.kimetrica.com/Kimetrica/Data Lab/Agriculture/Ethiopia Data/Cereal Yields.csv: A resource for a
        file called Cereal Yields, in a dataset called Ethiopia Data that belongs to the Agriculture organization that
        is a child of the Data Lab organization that is a child of the Kimetrica organization on the server at
        data.kimetrica.com

        * /Kimetrica/Data Lab/Agriculture/Ethiopia Data/Cereal Yields.csv: A resource for a file called Cereal Yields,
        in a dataset called Ethiopia Data that belongs to the Agriculture organization that is a child of the Data Lab
        organization that is a child of the Kimetrica organization. There is no server in the path, so either the
        server address must be passed as a parameter or a default server must be set in luigi.cfg.

        * /Kimetrica/Data Lab/Agriculture/Ethiopia Data/: A dataset called Ethiopia Data that belongs to the
        Agriculture organization that is a child of the Data Lab organization within the Kimetrica organization.

        * /Kimetrica/Data Lab/Agriculture/Ethiopia Data/: A dataset called Ethiopia Data that belongs to the
        Agriculture organization that is a child of the Data Lab organization within the Kimetrica organization.

        * /841f5d52-04cc-4fbc-aee6-57864d8b9e63/Cereal Yields.csv: A resource for a file called Cereal Yields,
        in a dataset with the id 841f5d52-04cc-4fbc-aee6-57864d8b9e63. The dataset id can only be used if the dataset
        already exists, and therefore the organization hierarchy is not required.

        * /841f5d52-04cc-4fbc-aee6-57864d8b9e63/c8f43dd2-5034-44ef-8a90-d992a2505269: A resource for a file. Typically
        a resource id can only be used if the file is an ExternalTarget that will be read but not written to.

        * /c8f43dd2-5034-44ef-8a90-d992a2505269: A resource for a file. Typically
        a resource id can only be used if the file is an ExternalTarget that will be read but not written to.

    format: (Optional) The Luigi Format to apply to the file
    client: (Optional) A CKANClient instance representing an authenticated connection to the CKAN server
    address: (Optional) A string containing the address of the CKAN server, e.g. https://data.kimetrica.com:8080
    token: (Optional) A string containing a valid API token for accessing the CKAN server
    mimetype: (Optional)
    dataset: (Optional) A dict containing an id key that contains the package id for an existing dataset. This
    approach is deprecated and is included for legacy support only.
    resource: (Optional) A dict containing an id key that contains the resource id for an existing dataset. This
    approach is deprecated and is included for legacy support only.

    """

    fs = None

    def __init__(
        self,
        path: Optional[str] = None,
        format: Optional[luigi.format.Format] = None,
        client: Optional[CKANClient] = None,
        address: Optional[str] = None,
        token: Optional[str] = None,
        mimetype: Optional[str] = None,
        dataset=None,
        resource=None,
    ):
        if format is None:
            format = luigi.format.get_default_format()

        # If we have legacy dataset and resource components, then convert them to a path
        if dataset and resource:
            if path:
                raise ValueError(
                    "Cannot create CkanTarget for a dataset and resource if a path is also specified"
                )
            path = f'/{dataset["id"]}/{resource["id"] if "id" in resource else resource["name"]}'
        elif dataset:
            if path:
                raise ValueError(
                    "Cannot create CkanTarget for a dataset if a path is also specified"
                )
            path = f'/{dataset["id"]}/'
        elif resource:
            if path:
                raise ValueError(
                    "Cannot create CkanTarget for a resource if a path is also specified"
                )
            path = f'/{resource["id"]}'

        # The path could contain the server address, e.g. https://data.kimetrica.com
        components = urlsplit(path)
        if components.netloc:
            server = urlunsplit(components[:2] + ("", "", ""))
            # The server is specified by the path, so the server or the client must match
            if client and client.client.address != server:
                raise ValueError(
                    "Cannot create CkanTarget with target address %s and client address %s"
                    % (server, client.client.address)
                )
            elif address and address != server:
                raise ValueError(
                    "Cannot create CkanTarget with target address %s and server %s"
                    % (server, address)
                )
            else:
                address = server

        self.path = path
        self.format = format

        self.fs = client or CKANClient(address=address, token=token)
        self.mimetype = mimetype

    @property
    def last_modified_datetime(self):
        """
        Gets the last modified date of a CkanTarget
        """
        resource_id = self.fs.get_id_from_path(self.path)[1]
        metadata = self.fs.client.action.resource_show(id=resource_id)
        date_last_modified = (
            metadata["last_modified"]
            if "last_modified" in metadata and metadata["last_modified"]
            else metadata["created"]
        )
        return pytz.UTC.localize(
            datetime.datetime.strptime(date_last_modified, "%Y-%m-%dT%H:%M:%S.%f")
        )

    def open(self, mode="r"):
        if mode == "r":
            return self.format.pipe_reader(
                FileWrapper(
                    io.BufferedReader(
                        self.fs.download(self.path, mimetype=self.mimetype)
                    )
                )
            )
        elif mode == "w":
            return self.format.pipe_writer(
                AtomicCkanFile(self.path, self.fs, self.mimetype)
            )
        else:
            raise ValueError("Unsupported open mode '{}'".format(mode))

    def copy(self, new_path, raise_if_exists=False):
        self.fs.copy(self.path, new_path, raise_if_exists)

    def move(self, new_path, raise_if_exists=False):
        self.fs.move(self.path, new_path, raise_if_exists=raise_if_exists)

    @contextmanager
    def temporary_path(self):
        """
        A context manager that enables a reasonably short, general and
        magic-less way to solve the :ref:`AtomicWrites`.
         * On *entering*, it will create a local temporary directory and return a path
           in that directory that can be used as a file or as a subdirectory
         * On *exiting*, it will move the temporary file or directory to Google Drive and then rename it to the final
           name.
        """
        with tempfile.TemporaryDirectory() as tmpdirname:
            _temp_path = os.path.join(tmpdirname, posixpath.basename(self.path))
            yield _temp_path

            # We won't reach here if there was an user exception.
            self.fs.put(_temp_path, self.path)

    def get(self, filename=None):
        """
        Support the legacy get/put interface
        """
        if not filename:
            # Legacy CkanTarget returns the location of the local copy of the file
            # Note that we don't know how the calling process will handle the file, so we leave it to the
            # calling process to delete the file when it is finished with it.
            fh = self.fs.download(self.path)
            fh._delete_on_close = False
            return fh.name
        else:
            # Regular Luigi get/put Targets accept the name of a local file to save the contents to
            fh = self.fs.download(self.path, filename)
            return fh

    def put(self, filename):
        """
        Support the legacy get/put interface
        """
        self.fs.put(filename, self.path)
