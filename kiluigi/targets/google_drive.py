"""
Google Drive File System and Luigi Task Target to be used directly as one option of Output from Tasks
Mainly adopted approach from the GCS (Google Cloud Service) luigi.contrib.gcs module
"""

import io
import json
import logging
import mimetypes
import os
import posixpath
import random
import tempfile
from contextlib import contextmanager
from typing import Optional

import luigi
from dateutil import parser
from google.oauth2 import service_account
from luigi.contrib import gcp
from luigi.format import FileWrapper
from luigi.target import (
    FileAlreadyExists,
    FileSystem,
    FileSystemException,
    FileSystemTarget,
    MissingParentDirectory,
)

from ..utils import get_config_value
from .local_target import NamedAtomicLocalFile

logger = logging.getLogger("luigi-interface")

try:
    import httplib2
    from googleapiclient import discovery, http
    from googleapiclient.errors import HttpError
except ImportError:
    logger.warning(
        "Loading GD module without the python packages googleapiclient & google-auth. \
        This will crash at runtime if GD functionality is used."
    )
else:
    # Retry transport and file IO errors.
    RETRYABLE_ERRORS = (httplib2.HttpLib2Error, IOError)

# Number of times to retry failed downloads.
NUM_RETRIES = 5

# Number of bytes to send/receive in each request.
CHUNKSIZE = 10 * 1024 * 1024

# Mimetype to use if one can't be guessed from the file extension.
DEFAULT_MIMETYPE = "application/octet-stream"


class InvalidDeleteException(FileSystemException):
    pass


class _DeleteOnCloseFile(io.FileIO):
    def close(self):
        super().close()
        try:
            os.remove(self.name)
        except OSError:
            # Catch a potential threading race condition and also allow this
            # method to be called multiple times.
            pass

    def readable(self):
        return True

    def writable(self):
        return False

    def seekable(self):
        return True


class GoogleDriveClient(FileSystem):
    """
    An implementation of a FileSystem over Google Drive.

    Credentials for authentication can be supplied when the class is instantiated. If credentials are not supplied then
    this class will look for credentials in a [google_drive] section in the global configuration (e.g. luigi.cfg). The
    class will fall back to credentials identified by google.auth, e.g. using the GOOGLE_APPLICATION_CREDENTIALS
    environment variable. Note that GOOGLE_APPLICATION_CREDENTIALS must contain the path to a file that contains the
    credentials, rather than the credentials themselves. If the credentials themselves are available in an environment
    variable (which must include the required line breaks in the private key, e.g. using \n)), then they can be made
    available via the luigi.cfg with a section like:

    [google_drive]
    oauth_credentials = ${GOOGLE_CREDENTIALS}
    """

    def __init__(
        self,
        oauth_credentials=None,
        descriptor="",
        http_=None,
        chunksize=CHUNKSIZE,
        **discovery_build_kwargs,
    ):
        self.chunksize = chunksize

        # Default configuration from environment and override as necessary
        config = get_config_value("google_drive")

        # Use config values if nothing was passed in
        oauth_credentials = oauth_credentials or config.get("oauth_credentials")
        http_ = http_ or config.get("http_")

        # Convert a credentials string to a scoped credentials object
        if isinstance(oauth_credentials, str) and oauth_credentials:
            scopes = [
                "https://spreadsheets.google.com/feeds",
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/drive.file",
                "https://www.googleapis.com/auth/drive",
            ]
            oauth_credentials = service_account.Credentials.from_service_account_info(
                json.loads(oauth_credentials)
            ).with_scopes(scopes)

        # Fall back to environment variables if nothing else is set
        authenticate_kwargs = gcp.get_authenticate_kwargs(oauth_credentials, http_)

        # Initialize the parameter for Google Client discovery
        build_kwargs = authenticate_kwargs.copy()

        # Add other config parameters
        build_kwargs.update(
            {k: v for k, v in config.items() if k not in ["oauth_credentials", "http_"]}
        )
        # And overrides
        build_kwargs.update(discovery_build_kwargs)

        if descriptor:
            self.client = discovery.build_from_document(descriptor, **build_kwargs)
        else:
            build_kwargs.setdefault("cache_discovery", False)
            self.client = discovery.build("drive", "v3", **build_kwargs)

    def exists(self, path):
        return bool(self.get_id_from_path(path))

    def get_id_from_path(self, path: str, create_folders: bool = False):
        """
        Convert a path potentially containing subfolders to a Google Drive folder or file id.

        By default find an existing folder id, but optionally (if create_folders=True) create the subfolders.
        """
        components = path.strip(posixpath.sep).split(posixpath.sep)
        # The first component must be an actual Google Folder id
        file_or_folder_id = components.pop(0)
        while components:
            name = components.pop(0)
            # Escape ' in the name
            name = name.replace("'", "\\'")
            # Find an object in this parent directory with the correct name
            query = f"name = '{name}' and parents='{file_or_folder_id}'"
            # If there are still components left, then this component must be a directory
            if components:
                query += "and mimeType='application/vnd.google-apps.folder'"
            try:
                response = (
                    self.client.files()
                    .list(
                        q=query, includeItemsFromAllDrives=True, supportsAllDrives=True
                    )
                    .execute()
                )
                if response and response.get("files"):
                    assert (
                        len(response.get("files")) == 1
                    ), f"There are multiple files called {name} in folder {file_or_folder_id}"  # NOQA: E501
                    file_or_folder_id = response.get("files")[0].get("id")
                else:
                    if create_folders:
                        metadata = {
                            "name": name,
                            "parents": [file_or_folder_id],
                            "mimeType": "application/vnd.google-apps.folder",
                        }
                        folder = (
                            self.client.files()
                            .create(
                                body=metadata,
                                fields="id",
                                supportsAllDrives=True,
                            )
                            .execute()
                        )
                        file_or_folder_id = folder.get("id")
                    else:
                        return None
            except HttpError as http_error:
                if http_error.status_code == 404 and create_folders:
                    metadata = {
                        "name": name,
                        "parents": [file_or_folder_id],
                        "mimeType": "application/vnd.google-apps.folder",
                    }
                    folder = (
                        self.client.files()
                        .create(
                            body=metadata,
                            fields="id",
                            supportsAllDrives=True,
                        )
                        .execute()
                    )
                    file_or_folder_id = folder.get("id")
                else:
                    raise
        return file_or_folder_id

    def put(
        self,
        local_path: str,
        destination_path: str,
        mimetype: Optional[str] = None,
        chunksize: Optional[int] = None,
        as_google_doc: bool = False,
    ):
        if os.path.isdir(local_path):
            # Upload directory with a temporary name and then rename to ensure atomicity
            if self.exists(destination_path):
                raise FileAlreadyExists()
            destination_path = destination_path.strip("/")
            temp_path = destination_path
            temp_path += "-luigi-tmp-%09d" % random.randint(0, 999999999)  # NOQA: S311
            try:
                self._put_directory(local_path, temp_path)
                # Renaming the directory in place guarantees atomicity
                # Check again in case the destination was created while the upload was happening
                self.move(temp_path, destination_path, raise_if_exists=True)
            except Exception:
                self.remove(temp_path)
                raise
        else:
            return self._put_file(
                local_path, destination_path, mimetype, chunksize, as_google_doc
            )

    def _put_directory(
        self,
        local_path: str,
        destination_path: str,
        mimetype: Optional[str] = None,
        chunksize: Optional[int] = None,
        as_google_doc: bool = False,
    ):
        for root, dirs, files in os.walk(local_path):
            for name in files:
                # Remove the root to find the subfolder path, and then strip the leading /
                # because otherwise posixpath.join treats it as an absolute path
                prefix_length = len(local_path)
                subfolders = root[prefix_length:].strip(posixpath.sep)
                self._put_file(
                    os.path.join(root, name),
                    posixpath.join(destination_path, subfolders, name),
                    mimetype,
                    chunksize,
                    as_google_doc,
                )

    def _put_file(
        self,
        local_path: str,
        destination_path: str,
        mimetype: Optional[str] = None,
        chunksize: Optional[int] = None,
        as_google_doc: bool = False,
    ):
        """
        Performs the actual upload as used by the AtomicGDFile
        """
        chunksize = chunksize or self.chunksize
        resumable = os.path.getsize(local_path) > 0
        destination_folder, name = posixpath.split(destination_path)
        destination_folder_id = self.get_id_from_path(
            destination_folder, create_folders=True
        )
        mimetype = mimetype or mimetypes.guess_type(name)[0] or DEFAULT_MIMETYPE
        media = http.MediaFileUpload(
            local_path, mimetype=mimetype, chunksize=chunksize, resumable=resumable
        )
        if destination_folder_id:
            query = f"name = '{name}' and parents='{destination_folder_id}'"
            response = (
                self.client.files()
                .list(q=query, includeItemsFromAllDrives=True, supportsAllDrives=True)
                .execute()
            )
            if response and response.get("files"):
                assert (
                    len(response.get("files")) == 1
                ), f"There are multiple files called {name} in folder {destination_folder_id}"  # NOQA: E501
                file_id = response.get("files")[0]["id"]
            else:
                file_id = None
        else:
            # If the destination_folder_id is empty, then the path may be a file id.
            response = (
                self.client.files()
                .get(
                    fileId=destination_path,
                    supportsAllDrives=True,
                )
                .execute()
            )
            file_id = response["id"]
        # Update in Google Drive to create a new revision and keep the old one.
        if file_id:
            file = (
                self.client.files()
                .update(
                    fileId=file_id,
                    media_body=media,
                    fields="id",
                    supportsAllDrives=True,
                )
                .execute()
            )
        else:
            file_metadata = {"name": name, "parents": [destination_folder_id]}
            if as_google_doc:
                # Currently we only support uploading to Google Sheets
                file_metadata["mimeType"] = "application/vnd.google-apps.spreadsheet"
            file = (
                self.client.files()
                .create(
                    body=file_metadata,
                    media_body=media,
                    fields="id",
                    supportsAllDrives=True,
                )
                .execute()
            )
        return file

    def download(
        self,
        path,
        chunksize: Optional[int] = None,
        chunk_callback=lambda _: False,
        mimetype: Optional[str] = None,
    ):
        """
        Downloads a file from Google Drive given the file_id to a temporary local file
        and returns the local file for processing
        """
        chunksize = chunksize or self.chunksize
        file_id = self.get_id_from_path(path)
        if not file_id:
            raise Exception("File %s not found" % path)
        # As per https://developers.google.com/drive/api/v3/manage-downloads#download_a_document
        # export_media method should be used if the file is Google Workspace Document
        file_obj = (
            self.client.files().get(fileId=file_id, supportsAllDrives=True).execute()
        )
        # Assuming we only need to deal with spreadsheet types from google workspace docs
        if file_obj.get("mimeType") == "application/vnd.google-apps.spreadsheet":
            mimetype = mimetype or "application/x-vnd.oasis.opendocument.spreadsheet"
            request = self.client.files().export_media(
                fileId=file_id,
                mimeType=mimetype,
            )
        else:
            request = self.client.files().get_media(fileId=file_id)

        done = False
        with tempfile.NamedTemporaryFile(delete=False) as fp:
            downloader = http.MediaIoBaseDownload(fp, request, chunksize=chunksize)
            return_fp = _DeleteOnCloseFile(fp.name, "r")
            while done is False:
                _, done = downloader.next_chunk()
                if chunk_callback(fp):
                    done = True
        return return_fp

    def mkdir(self, path, parents=True, raise_if_exists=False):
        if raise_if_exists and self.exists(path):
            raise FileAlreadyExists()

        if not parents and not self.exists(os.path.dirname(path)):
            raise MissingParentDirectory()

        folder_id = self.get_id_from_path(path, create_folders=True)
        return folder_id

    def isdir(self, path):
        """
        Return ``True`` if the location at ``path`` is a directory. If not, return ``False``.

        :param str path: a path within the FileSystem to check as a directory.
        """
        file_or_folder_id = self.get_id_from_path(path)
        file_or_folder = (
            self.client.files()
            .get(fileId=file_or_folder_id, fields="mimeType")
            .execute()
        )
        if file_or_folder["mimeType"] == "application/vnd.google-apps.folder":
            return True
        else:
            return False

    def listdir(self, path):
        folder_id = self.get_id_from_path(path)
        page_token = None
        while True:
            response = (
                self.client.files()
                .list(
                    q=f"'{folder_id}' in parents",
                    fields="nextPageToken, files(id, name, mimeType)",
                    pageToken=page_token,
                    pageSize=1000,
                    includeItemsFromAllDrives=True,
                    supportsAllDrives=True,
                )
                .execute()
            )

            for f in response.get("files", []):
                if f["mimeType"] != "application/vnd.google-apps.folder":
                    yield f'{path}/{f["name"]}'
                else:
                    yield from self.listdir(f'{path}/{f["name"]}')

            page_token = response.get("nextPageToken", None)
            if page_token is None:
                break

    def remove(self, path):
        try:
            file_id = self.get_id_from_path(path)
            self.client.files().delete(fileId=file_id, supportsAllDrives=True).execute()
        except Exception as exception:
            logger.warning(f"An error occurred: {exception}")

    def move(
        self, source_path: str, destination_path: str, raise_if_exists: bool = False
    ):
        """
        Rename/move an object from one Google Drive location to another.

        :param source_path: The path of the directory or file to copy from
        :param destination_path: The path of the directory or file to copy to. If the destination_path is the
                                 containing folder rather than the full path to the new location (i.e. including
                                 the new name of the item being moved) then it must have a trailing slash.
        :param raise_if_exists: Raise an exception if the destination_path already exists
        """
        if raise_if_exists and self.exists(destination_path):
            raise FileAlreadyExists()

        source_id = self.get_id_from_path(source_path)
        destination_folder, name = posixpath.split(destination_path)
        # Keep the original name if the destination is a directory
        name = name or posixpath.basename(source_path)
        destination_folder_id = self.get_id_from_path(
            destination_folder, create_folders=True
        )

        try:

            # Retrieve the existing parents to remove
            previous_parents = ",".join(
                self.client.files()
                .get(
                    fileId=source_id,
                    fields="parents",
                    supportsAllDrives=True,
                )
                .execute()
                .get("parents")
            )
            # Move the file to the new folder, changing the name if necessary
            self.client.files().update(
                fileId=source_id,
                addParents=destination_folder_id,
                removeParents=previous_parents,
                body={"name": name},
                fields="id, parents",
                supportsAllDrives=True,
            ).execute()
        except Exception as e:
            raise Exception(
                "An error occurred moving %s to %s" % (source_path, destination_path)
            ) from e

    def copy(
        self,
        source_path: str,
        destination_path: str,
        raise_if_exists: bool = False,
    ):
        if raise_if_exists and self.exists(destination_path):
            raise FileAlreadyExists()
        source_id = self.get_id_from_path(source_path)
        destination_folder, name = posixpath.split(destination_path)
        # Keep the original name if the destination is a directory
        name = name or posixpath.basename(source_path)
        destination_folder_id = self.get_id_from_path(
            destination_folder, create_folders=True
        )
        try:
            return (
                self.client.files()
                .copy(
                    fileId=source_id,
                    body={"name": name, "parents": [destination_folder_id]},
                    supportsAllDrives=True,
                )
                .execute()
            )
        except Exception as e:
            raise Exception(
                "An error occurred copying %s to %s" % (source_path, destination_path)
            ) from e

    def get_last_modified_datetime(self, path):
        """
        Gets the last modified date of a resource by self.oauth_credentials.

        Google Drive returns an RFC 3339 date-time such as '2022-03-29T16:06:16.674Z'
        """
        file_id = self.get_id_from_path(path)
        response = (
            self.client.files()
            .get(
                fileId=file_id,
                fields="modifiedByMe,modifiedByMeTime",
                supportsAllDrives=True,
            )
            .execute()
        )

        # We can check whether or not a file has ever been modified by an account
        # by looking at the `modifiedByMe` field, which returns True if the file
        # has been modified by the account in the past or False otherwise. See
        # https://developers.google.com/drive/api/v3/reference/files for the list
        # of fields that can be returned for a file and their description.
        if response["modifiedByMe"]:
            dt = parser.isoparse(response["modifiedByMeTime"])
            return dt


class AtomicGDFile(NamedAtomicLocalFile):
    """
    A Google Drive file that writes to a temp file and puts to Google Drive on close.
    """

    def __init__(
        self,
        path,
        gd_client,
        mimetype: Optional[str] = None,
        as_google_doc: bool = False,
    ):
        self.gd_client = gd_client
        self.mimetype = mimetype
        self.as_google_doc = as_google_doc
        super().__init__(path)

    def move_to_final_destination(self):
        self.gd_client.put(
            self.tmp_path,
            self.path,
            mimetype=self.mimetype,
            as_google_doc=self.as_google_doc,
        )
        self.directory.cleanup()


class GoogleDriveTarget(FileSystemTarget):
    """
    A Target that stores data in a file in Google Drive.

    path: The path to the file in Google Drive, typically consisting of a root folder and optionally
          one or more subfolders and/or the name of a file, for example:

          * /1uGwGWsOx1c1cpazTJeI8zy1FQYF is a reference to a folder
          * /1uGwGWsOx1c1cpazTJeI8zy1FQYF/test.csv is a reference to a file in that folder
          * /1uGwGWsOx1c1cpazTJeI8zy1FQYF/output/data/ is a reference to a subfolder below that folder
          * /1uGwGWsOx1c1cpazTJeI8zy1FQYF/output/data/test.csv is a reference to a file in a subfolder below that folder

          It is also possible for the path to be the Google Drive file id for an existing file:
          i.e. path="1uGwGWsOx1c1cpazTJeI8zy1FQYF" could be the id of an existing file we wish to download or update.

    format: (Optional) The Luigi Format to apply to the file
    client: (Optional) A configured/authenticated GoogleDriveClient to use to access Google Drive
    oauth_credentials: (Optional) A JSON string containing valid credentials for accessing Google Drive, such as:
                       {"type": "service_account", "project_id": "kiuigi-access", "private_key_id": "0a<snip>", "private_key": "-----BEGIN PRIVATE KEY-----MIIEv<snip>Aw=-----END PRIVATE KEY-----", "client_email": "xxx@yyy.iam.gserviceaccount.com", "client_id": "1XXXXXXXXXX", "auth_uri": "https://accounts.google.com/o/oauth2/auth", "token_uri": "https://oauth2.googleapis.com/token", "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs", "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/XXXX%40YYYY.iam.gserviceaccount.com"}  # NOQA: E501

    """

    fs = None

    def __init__(
        self,
        path: str,
        format: Optional[luigi.format.Format] = None,
        client: Optional[GoogleDriveClient] = None,
        oauth_credentials: Optional[str] = None,
        mimetype: Optional[str] = None,
        as_google_doc: bool = False,
    ):
        super().__init__(path)
        if format is None:
            format = luigi.format.get_default_format()

        self.path = path
        self.format = format

        self.fs = client or GoogleDriveClient(oauth_credentials=oauth_credentials)
        self.mimetype = mimetype
        self.as_google_doc = as_google_doc

    @property
    def last_modified_datetime(self):
        return self.fs.get_last_modified_datetime(self.path)

    def open(self, mode="r"):
        if mode == "r":
            return self.format.pipe_reader(
                FileWrapper(
                    io.BufferedReader(
                        self.fs.download(self.path, mimetype=self.mimetype)
                    )
                )
            )
        elif mode == "w":
            return self.format.pipe_writer(
                AtomicGDFile(self.path, self.fs, self.mimetype, self.as_google_doc)
            )
        else:
            raise ValueError("Unsupported open mode '{}'".format(mode))

    def copy(self, new_path, raise_if_exists=False):
        self.fs.copy(self.path, new_path, raise_if_exists)

    def move(self, new_path, raise_if_exists=False):
        self.fs.move(self.path, new_path, raise_if_exists=raise_if_exists)

    @contextmanager
    def temporary_path(self):
        """
        A context manager that enables a reasonably short, general and
        magic-less way to solve the :ref:`AtomicWrites`.
         * On *entering*, it will create a local temporary directory and return a path
           in that directory that can be used as a file or as a subdirectory
         * On *exiting*, it will move the temporary file or directory to Google Drive and then rename it to the final
           name.
        """
        with tempfile.TemporaryDirectory() as tmpdirname:
            _temp_path = os.path.join(tmpdirname, "luigi-tmp")
            yield _temp_path

            # We won't reach here if there was an user exception.
            self.fs.put(_temp_path, self.path)
