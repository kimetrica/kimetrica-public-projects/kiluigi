import mimetypes
import os

import luigi
from luigi.task import TASK_ID_TRUNCATE_HASH

from ..format import Pickle
from ..utils import class_from_name, name_from_class, path_from_task
from .expiring import ExpiringLocalTarget
from .local_target import LocalTarget

# Add additional mimetypes to ensure we recognize file extensions
# accurately. Note that the default mimetypes are system-dependent.
# For example, .csv returns `text/csv` on Linux but `application/vnd.ms-excel`
# on Windows
ADDITIONAL_MIMETYPES = [
    ("text/csv", ".csv"),
    ("text/plain", ".log"),
    ("application/geo+json", ".geojson"),
    ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ".xlsx"),
    ("application/x-dbase", ".dbf"),
    ("application/x-pickle", ".pck"),
    ("application/x-pickle", ".pkl"),
    ("application/x-pickle", ".pickle"),
    ("application/x-qgis", ".shp"),
]
for mimetype, ext in ADDITIONAL_MIMETYPES:
    mimetypes.add_type(mimetype, ext, strict=True)


# The default output path to to use if one isn't specified
# either in the task or in the luigi.cfg
DEFAULT_OUTPUT_DIR = os.path.abspath("output")


class DelegatingTarget(luigi.target.Target):
    """
    A Target that acts as a front end for Tasks but which delegates
    the actual storage to a pluggable backend Target class.

    This allows Luigi pipelines to be written and deployed on different
    infrastructures without alteration. For example, pipelines can be
    written and tested on an individual workstation and then later deployed
    to a distributed cluster with only configuration changes.

    The backend_class is normally configured in luigi.cfg although it can
    be specified directly when the Target is instantiated.

    The developer is responsible for making sure that the backend(s)
    used have the methods required to match the way that the instance
    is used in Tasks.

    The Target will guess a suitable output format (text or binary)
    depending on the extension of the `path` parameter and will default
    to `pickle` if no path, or a path without an extension, is specified.

    If a :py:class:luigi.Task instance is passed to the `task` parameter
    it will be used to ensure that the output location is unique for the
    :py:attr:luigi.Task.task_id. (i.e. for that unique set of significant
    parameters for the Task).

    """

    backend_class = LocalTarget
    root_path = None
    relative_path = None

    def __init__(
        self, path=None, format=None, task=None, **kwargs  # noqa: A002
    ) -> None:
        # The first argument should be a path, but might be a Task
        if isinstance(path, luigi.Task) and task is None:
            task = path
            path = None

        # Make sure we have a path or a Task
        assert (
            path or task
        ), f"{name_from_class(self)} requires a relative path and/or a Task instance"

        if not path:
            # There user didn't provide a path, so specify one from the Task
            path = path_from_task(task)
        elif task:
            # We have a path (i.e. a preferred filename) and a Task instance
            # Make sure that the task_id hash is part of the path so that we
            # don't accidentally reuse results for different sets of significant
            # parameters.
            # This will also make it easier to identify which outputs were produced
            # by which task run
            task_hash = task.task_id[-TASK_ID_TRUNCATE_HASH:]
            path, ext = os.path.splitext(path)
            path = f"{path}_{task_hash}{ext}"

        # Make sure we have a relative path we can join to root_path
        assert not os.path.isabs(
            path
        ), f"{name_from_class(self)} requires a relative path, not '{path}'"

        # Make sure we have a Posix path
        assert (
            "\\" not in path
        ), f"'{path}' contains a forbidden Windows directory separator - use / to separate directories"

        # Pick a format based on the extension, and default to Pickle.
        if not format:
            mimetype = mimetypes.guess_type(path)[0]

            if mimetype:
                if mimetype == "application/x-pickle":
                    format = Pickle  # noqa: A001
                elif (
                    mimetype.startswith("text/")
                    or mimetype == "application/json"
                    or mimetype == "application/geo+json"
                ):
                    format = luigi.format.Text  # noqa: A001
                elif mimetype.startswith("image/") or mimetype.startswith(
                    "application/"
                ):
                    format = luigi.format.Nop  # noqa: A001
                else:
                    format = luigi.format.Text  # noqa: A001
            elif not path[-1] == "/":
                format = Pickle  # noqa: A001

        # Default the extension if one wasn't provided
        if format == Pickle and mimetypes.guess_type(path)[0] != "application/x-pickle":
            path += ".pickle"

        self.relative_path = path

        if "root_path" in kwargs and kwargs["root_path"]:
            self.root_path = kwargs.pop("root_path")
        else:
            # If the Target doesn't specify the root_path directly, then check the
            # local configuration before falling back to the default path
            self.root_path = luigi.configuration.get_config().get(
                name_from_class(self), "root_path", self.root_path
            )

        if "backend_class" in kwargs and kwargs["backend_class"]:
            self.backend_class = kwargs.pop("backend_class")
        else:
            # If the Target doesn't specify the backend_class directly, then check the
            # local configuration before falling back to the default class
            self.backend_class = luigi.configuration.get_config().get(
                name_from_class(self), "backend_class", self.backend_class
            )

        # Convert str backend_class to actual Python class if necessary
        if isinstance(self.backend_class, str):
            self.backend_class = class_from_name(self.backend_class)

        # Instantiate a backend
        self._backend = self.backend_class(path=self.path, format=format, **kwargs)

    def __getattr__(self, name):
        return getattr(self._backend, name)

    def __str__(self):
        return f"{name_from_class(self).split('.')[-1]}({self._backend})"

    @property
    def path(self):
        return os.path.join(self.root_path, self.relative_path)

    def exists(self):
        return self._backend.exists()


class IntermediateTarget(DelegatingTarget):
    """
    A Luigi Target that stores intermediate pipeline outputs with per item expiry.

    Normally this Target is used for outputs that are likely to be required by downstream
    pipelines, but which are not direct deliverables of the pipelines that will be
    reviewed by end users.

    Backends for this Target must implement a similar API to a LocalTarget, with the
    addition of a timeout that is used to determine how long an existing Target is valid
    for before it must be recreated.

    Note that the path must be relative rather than absolute.

    The Target will guess a suitable output format (text or binary)
    depending on the extension of the `path` parameter and will default
    to `pickle` if no path, or a path without an extension, is specified.

    If a :py:class:luigi.Task instance is passed to the `task` parameter
    it will be used to ensure that the output location is unique for the
    :py:attr:luigi.Task.task_id. I.e. for that unique set of significant
    parameters for the Task.
    """

    backend_class = ExpiringLocalTarget
    root_path = os.path.join(DEFAULT_OUTPUT_DIR, "intermediate_targets")


class FinalTarget(DelegatingTarget):
    """
    A Luigi Target that stores final pipeline outputs.

    Normally this Target is used for outputs that are direct deliverables of the pipelines
    and that will be viewed by end users.

    Backends for this Target must implement a similar API to a LocalTarget.

    Note that the path must be relative rather than absolute.

    The Target will guess a suitable output format (text or binary)
    depending on the extension of the `path` parameter and will default
    to `pickle` if no path, or a path without an extension, is specified.

    If a :py:class:luigi.Task instance is passed to the `task` parameter
    it will be used to ensure that the output location is unique for the
    :py:attr:luigi.Task.task_id. I.e. for that unique set of significant
    parameters for the Task.
    """

    root_path = os.path.join(DEFAULT_OUTPUT_DIR, "final_targets")
