import datetime
import logging
import os
from contextlib import contextmanager
from typing import Optional

import dateutil.parser
import luigi

from ..utils import name_from_class
from .local_target import LocalTarget
from .memory import MemoryTarget
from .s3 import TaggableS3Target

logger = logging.getLogger("luigi-interface")


class ExpiringTargetMixin(luigi.Target):
    """
    A mixin that provides expiry and pruning functionality to Targets.

    A timeout in seconds can be provided by the calling Task.

    Note that this Mixin must come first in the list of base classes
    so exists() can check the file actually exists before working out whether
    it has expired.
    """

    def __init__(
        self,
        path: str,
        format: Optional[luigi.format.Format] = None,
        timeout: Optional[int] = None,
        **kwargs,
    ):
        self.timeout = timeout or luigi.configuration.get_config().get(
            name_from_class(self), "default_timeout", 60 * 60 * 24
        )
        assert isinstance(self.timeout, int) and self.timeout > 0, (
            "timeout is the number of seconds the Target is valid for "
            "before being recalculated, and must be a positive integer, "
            "not %r" % self.timeout
        )

        super().__init__(path, format, **kwargs)

    def _expire(self, path: Optional[str] = None):
        path = path or self.path
        expiry = self.get_expiry(path)
        if expiry and expiry < datetime.datetime.now(datetime.timezone.utc):
            ago = datetime.datetime.now(datetime.timezone.utc) - expiry
            logger.debug(
                f"{name_from_class(self).split('.')[-1]} '{path}' expired "
                f"{int(ago.total_seconds())}s ago at {expiry} and has been pruned"
            )
            self.fs.remove(path)
        elif expiry:
            logger.debug(f"{path} does not expire until {expiry}")
        else:
            logger.debug(f"{path} is not an expiring target")

    def set_expiry(self, path: Optional[str] = None):
        """
        Set the expiry time as an extended attribute on the file
        """
        path = path or self.path
        if self.timeout:
            expiry = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(
                seconds=self.timeout
            )
            self.set_metadata(path, "expiry", expiry.isoformat())

    def get_expiry(self, path: Optional[str] = None):
        """
        Get the expiry time from an extended attribute on the file
        """
        path = path or self.path
        try:
            expiry = self.get_metadata(path, "expiry")
            if not expiry:
                return None
            # Fix return values like "2024-02-17T20:39:38.486013 00:00" because some backends strip the "+"
            return dateutil.parser.parse(expiry.replace(" ", "+"))
        except (IOError, TypeError):
            return None

    def prune(self):
        """
        Remove any expired files from the directory that this Target is in,
        based on an expiry timestamp stored in an extended attribute in the file.
        """
        base_dir = os.path.dirname(self.path)
        logger.debug(
            f"Pruning {base_dir} for {name_from_class(self).split('.')[-1]} '{self.path}'"
        )
        try:
            for path in self.fs.listdir(base_dir):
                assert path.startswith(base_dir)
                self._expire(path)
        except FileNotFoundError:
            # The output directory doesn't exist yet, so there aren't any other files to prune
            logger.debug(f"{base_dir} does not exist yet")

    def open(self, mode="r"):  # noqa: A003
        logger.debug("Getting parent pipe")
        pipe = super().open(mode)
        if "w" in mode:
            # Remove any expired files
            self.prune()
            # Get the file object and then set the expiry time on it
            self.set_expiry(pipe)
        return pipe

    def exists(self):
        exists = super().exists()
        if exists:
            expiry = self.get_expiry(self.path)
            if expiry and expiry < datetime.datetime.now(datetime.timezone.utc):
                ago = datetime.datetime.now(datetime.timezone.utc) - expiry
                logger.debug(
                    f"{name_from_class(self).split('.')[-1]} '{self.path}' expired "
                    f"{int(ago.total_seconds())}s ago at {expiry} and has been removed"
                )
                exists = False
                self.remove()
        return exists

    @contextmanager
    def temporary_path(self):
        with super().temporary_path() as _temp_path:
            yield _temp_path

        # After we return, then set the expiry metadata on the final path
        self.set_expiry(self.path)


# Note that ExpiringTargetMixin must be listed first
class ExpiringMemoryTarget(ExpiringTargetMixin, MemoryTarget):
    """
    A Luigi Target that stores data in a thread-safe in-memory cache with per item expiry.
    """

    def prune(self):
        """
        Remove expired keys from the memory store.

        Build a list and then remove them in a separate loop to avoid:
            RuntimeError: dictionary changed size during iteration
        """
        to_remove = []
        for path in self.fs.get_all_data().keys():
            expiry = self.get_expiry(path)
            if expiry and expiry < datetime.datetime.now(datetime.timezone.utc):
                logger.debug("ExpiringMemoryTarget '%s' has expired" % path)
                to_remove.append(path)
        for path in to_remove:
            del self.fs.get_all_data()[path]


# Note that ExpiringTargetMixin must be listed first
class ExpiringLocalTarget(ExpiringTargetMixin, LocalTarget):
    """
    A LocalTarget that automatically expires.

    A timeout in seconds can be provided by the calling Task.
    """

    def prune(self):
        """
        Remove any expired ExpiringLocalTarget files or directories from the directory that this Target is in,
        based on an expiry timestamp stored in an extended attribute in the file or directory
        """
        super().prune()
        # luigi.target.FileSystem can only list files (using listdir),
        # so we need to enumerate and expire directories separately.
        base_dir = os.path.dirname(self.path)
        for path in os.scandir(base_dir):
            if path.is_dir():
                assert path.path.startswith(base_dir)
                self._expire(path.path)


# Note that ExpiringTargetMixin must be listed first
class ExpiringS3Target(ExpiringTargetMixin, TaggableS3Target):
    """
    An S3Target that automatically expires.

    A timeout in seconds can be provided by the calling Task.
    """

    def prune(self):
        """
        Remove any expired ExpiringS3Target files from the directory that this Target is in,
        based on an expiry timestamp stored in a tag on the S3 object.
        """
        base_dir = os.path.dirname(self.path)
        logger.debug(
            f"Pruning {base_dir} for {name_from_class(self).split('.')[-1]} '{self.path}'"
        )
        for path in self.fs.listdir(base_dir):
            assert path.startswith(base_dir)
            if path.endswith("_METADATA"):
                # S3 doesn't have directory objects, so treat the _METADATA
                # file as a placeholder for the folder
                path = path[:-9]
            self._expire(path)
