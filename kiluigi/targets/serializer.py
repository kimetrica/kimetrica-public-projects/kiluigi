import contextlib
import datetime
import hashlib
import logging
import os
import pickle  # NOQA: S403
import shutil
from inspect import getsource

import luigi

from .base import Target

logger = logging.getLogger("luigi-interface")

config = luigi.configuration.get_config()


class SerializerTarget(Target):
    """
    A Target class that stores Python objects pickled in an expiring file.

    Example usage:

    .. code-block:: python
       :linenos:
       :emphasize-lines: 10,14,24
       :caption: this.py

        from kiluigi.targets import SerializerTarget
        from pprint import pprint
        import luigi


        class WriteToSerializerTarget(luigi.Task):

            def run(self):
                print('Implements same interface as LocalMemoryTarget. WriteToSerializerTarget putting a string:')
                self.output().put('a string')

            def output(self):
                print('WriteToSerializerTarget outputting a SerializerTarget.'
                      'timeout and base_dir are read from luigi.cfg SerializerTarget or core sections,'
                      'and can be overridden here.'
                      'If you pass the task, then cached files will be deleted when the source task code is changed.')
                return SerializerTarget(name='ChrisTest', timeout=300, task=self)

        class PrintSerializerTarget(luigi.WrapperTask):

            def requires(self):
                print('PrintSerializerTarget requiring a WriteToPickleTask')
                return WriteToSerializerTarget()

            def run(self):
                print('PrintSerializerTarget pprinting its input')
                pprint(self.input().get())


        if __name__ == '__main__':
            luigi.build([PrintSerializerTarget()], workers=1, local_scheduler=True)

    """

    def __init__(
        self,
        name: str = None,
        timeout: int = None,
        base_dir: str = None,
        task: luigi.Task = None,
    ) -> None:

        assert isinstance(name, str), (
            "name is the unique identifier the Target, and must be a string, not %r"
            % name
        )

        self.name = name

        self.timeout = (
            timeout
            or config.get("SerializerTarget", "timeout", None)
            or config.get("core", "timeout", 60 * 60)
        )  # default 1hr

        # If no base_dir is specified at instantiation or in settings, and stores in subdirectory
        # `SerializerTarget` of core.cache_dir if present, otherwise of current dir.
        self.base_dir = os.path.abspath(
            base_dir
            or config.get("SerializerTarget", "base_dir", None)
            or os.path.join(config.get("core", "cache_dir", "."), type(self).__name__)
        )

        self.src_task = task
        self._cache = None

    @property
    def src_task_hash(self):
        return (
            hashlib.md5(  # NOQA: S303
                getsource(self.src_task.__class__).encode()
            ).hexdigest()
            if self.src_task
            else None
        )

    @classmethod
    def _pickle_to_file(cls, path, data):
        normpath = os.path.normpath(path)
        folder = os.path.dirname(normpath)
        with contextlib.suppress(OSError):  # TOCTTOU
            os.makedirs(folder)
        try:
            with contextlib.suppress(FileNotFoundError):
                os.remove(path)
            with open(path, "wb") as file_ref:
                for el in data:
                    pickle.dump(el, file_ref)
        except IOError:
            logger.warning("Save failed: %s to %s", str(data), path, exc_info=1)
        # If you need to see the metadata, add: else: logger.info('Saved state %s in %s', str(data), path)

    @classmethod
    def _unpickle_from_file(cls, path, index):
        # We could `while True: yield` from pickles rather than specify an index, but then
        # file handles will be left open until garbage collection, or a counter
        # and non-deterministic behaviour if called from more than one place. This method
        # is too general purpose to add this complexity.
        if os.path.exists(path):
            logger.info("Attempting to load from %s", path)
            try:
                with open(path, "rb") as file_ref:
                    for _ in range(index):
                        item = pickle.load(file_ref)  # NOQA: S301
                return item
            except BaseException:
                logger.exception("Error when loading data from %s", path)
                return
        else:
            logger.info("No prior data file exists at %s", path)

    PICKLE_INDEX_HASH = 1
    PICKLE_INDEX_VALUE = 2

    @property
    def cache(self):
        if not self._cache:
            self._prune()
            filename = self.get_cache_file()
            if not filename:
                self._cache = None
                return self._cache
            src_task_hash = self._unpickle_from_file(filename, self.PICKLE_INDEX_HASH)
            if src_task_hash != self.src_task_hash:
                logging.debug(
                    "SerializerTarget %s expired because source task has changed.",
                    self.name,
                )
                self._cache = None
                self.remove()
                return self._cache
            self._cache = self._unpickle_from_file(filename, self.PICKLE_INDEX_VALUE)
        return self._cache

    def get_cache_file(self):
        # This iterates through the directory checking the dates of each file and returning the latest.
        # In theory there should only be one file, but you also get eg, `.DS_Store` on macOS.
        # And as we're iterating anyway we may as well cope with multiple cache files for whatever reason, or missing
        # file. File systems should be treated as 'wild west'.
        if not os.path.isdir(self.file_dir):
            return None
        max_date_so_far = datetime.datetime(1900, 1, 1)
        cache_file = None
        for file in os.listdir(self.file_dir):
            filename_as_date = self.try_parse_filename(file)
            if filename_as_date and filename_as_date > max_date_so_far:
                cache_file = file
                max_date_so_far = filename_as_date
        return os.path.join(self.file_dir, cache_file) if cache_file else None

    @property
    def file_dir(self):
        return os.path.join(self.base_dir, self.name)

    def get(self):
        return self.cache

    def exists(self):
        try:
            exists = bool(self.cache)
        except ValueError:
            exists = True
        return exists

    FILENAME_DATE_FORMAT = "%Y%m%d%H%M%S"
    FILENAME_EXT = ".pickle"

    def try_parse_filename(self, file):
        try:
            filename_as_date = datetime.datetime.strptime(
                file[: -len(self.FILENAME_EXT)], self.FILENAME_DATE_FORMAT
            )
        except ValueError:
            logging.info("Ignoring non-cache file %s", file)
            return None
        return filename_as_date

    def _prune(self):
        for subdir, _dirs, files in os.walk(self.base_dir):
            for file in files:
                filename_as_date = self.try_parse_filename(file)
                if not filename_as_date:
                    continue
                if filename_as_date < datetime.datetime.utcnow():
                    logging.debug(
                        "SerializerTarget %s expired %s.",
                        self.name,
                        filename_as_date.strftime("%Y-%m-%d %H:%M"),
                    )
                    shutil.rmtree(subdir)

    def put(self, value):
        expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=self.timeout)
        filename = os.path.join(
            self.file_dir,
            expiry.strftime(self.FILENAME_DATE_FORMAT) + self.FILENAME_EXT,
        )
        # If the source task is not passed, task_hash will be None.
        # Pickle serializes and deserializes this correctly.
        # Pickling separately means we can deserialize and test the hash without deserializing the object too.
        self._pickle_to_file(filename, [self.src_task_hash, value])

    def remove(self):
        shutil.rmtree(self.file_dir)
