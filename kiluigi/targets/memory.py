import hashlib
import logging
import random
import sys
import warnings
from collections import defaultdict
from copy import deepcopy
from inspect import getsource
from io import BytesIO, TextIOWrapper
from threading import Condition
from typing import Optional

import luigi
from luigi.format import BaseWrapper
from luigi.mock import MockFileSystem, MockTarget

from kiluigi.mixins import TagMixin
from kiluigi.utils import name_from_class

logger = logging.getLogger("luigi-interface")


class ThreadSafeMemoryStore:
    """
    Thread-safe memory store implemented using an in-memory dict and a lock.

    Each item in the dict is a dict with a "value" key containing the actual value
    and additional metadata items as required.
    """

    _lock = Condition()
    _store: dict = defaultdict(dict)

    def __contains__(self, key):
        return key in self._store and "value" in self._store[key]

    def __setitem__(self, key, value):
        with self._lock:
            self._store[key]["value"] = value

    def __getitem__(self, key):
        return deepcopy(self._store[key].get("value"))

    def __delitem__(self, key):
        with self._lock:
            del self._store[key]

    def items(self):
        for key, value in self._store.items():
            yield key, deepcopy(value.get("value"))

    def keys(self):
        return self._store.keys()

    def values(self):
        for value in self._store.values():
            yield deepcopy(value.get("value"))

    def pop(self, key):
        with self._lock:
            return deepcopy(self._store.pop(key).get("value"))

    def clear(self):
        with self._lock:
            return self._store.clear()

    def set_metadata(self, key: str, name: str, value: str):
        """
        Set the metadata
        """
        assert isinstance(key, str), f"key '{key}' must be a string, not {type(key)}"
        with self._lock:
            self._store[key][name] = value

    def get_metadata(self, key: str, name: str) -> Optional[str]:
        """
        Get the metadata
        """
        assert isinstance(key, str), f"key '{key}' must be a string, not {type(key)}"
        try:
            return self._store[key][name]
        except KeyError:
            return None


class MemoryFileSystem(MockFileSystem):
    """
    A Luigi File System that stores data in a thread-safe in-memory cache.

    Note that this File System only works within a single Luigi worker because it cannot share data across processes.

    It is required for pipelines that need to pass data between Luigi Tasks running inside Celery Tasks, where using
    MockTarget and MockFileSystem fails with "AssertionError: daemonic processes are not allowed to have children".
    See https://github.com/celery/celery/issues/1709
    """

    _data = None

    def get_all_data(self):
        """
        Return the data from the class variable, initializing it if required.
        """
        if MemoryFileSystem._data is None:
            MemoryFileSystem._data = ThreadSafeMemoryStore()
        return MemoryFileSystem._data

    def exists(self, path):
        return path in self.get_all_data()


class Target(TagMixin, luigi.Target):
    @property
    def description(self):
        return self.__class__.__name__


class MemoryTarget(Target, MockTarget):
    """
    A Luigi Target that stores data in a thread-safe in-memory cache.

    Therefore it is a good choice for passing pure Python objects such as Pandas DataFrames between Tasks. It is the
    responsibility of the pipeline author to consider the memory implications of using the shared memory store.

    Note that this Target only works within a single Luigi worker because it cannot share data across processes.

    This Target supports storing additional metadata alongside the value.

    This Target supports the legacy get/put interface as well as the preferred open/read/write/close interface.

    Using the preferred open/read/write/close typically works best with `format=Pickle`.

    Using the get/put interface doesn't require objects to be serialized and will be slightly faster.
    """

    fs = MemoryFileSystem()

    def __init__(
        self,
        path: Optional[str] = None,
        format: Optional[luigi.format.Format] = None,
        is_tmp=None,
        name: Optional[str] = None,
        task: Optional[luigi.Task] = None,
        **kwargs,
    ) -> None:
        if path is None and name is not None:
            path = name
            warnings.warn(
                f"Use of the `name` parameter with {name_from_class(self)} is deprecated, use `path=` or a positional parameter instead",  # NOQA: E501
                DeprecationWarning,
                stacklevel=2,
            )

        if path is None and is_tmp:
            path = "luigi-tmp-%09d" % random.randint(0, 999999999)  # NOQA: S311

        assert isinstance(
            path, str
        ), f"path '{path}' is the unique identifier the Target, and must be a string, not {type(path)}"

        assert task is None or isinstance(
            task, luigi.Task
        ), f"task '{task}' must be an instance of luigi.Task, not {type(task)}"

        # If a Task is provided, include a hash of the source code in the path so that
        # code changes in the Task automatically cause a cache miss
        if task:
            source = getsource(task.__class__).encode()
            source_hash = hashlib.md5(source).hexdigest()  # NOQA: S303
            path += "_" + source_hash

        self.is_tmp = is_tmp

        super().__init__(path, format=format, is_tmp=is_tmp, **kwargs)

    def open(self, mode="r"):
        """
        Return an object that can read or write the value from the memory store.

        Overrides MockTarget.open to store the value in a dict, allowing additional metadata to stored as well.
        """
        fn = self.path
        mock_target = self

        # Support incorrectly ordered file modes
        if mode == "br":
            mode = "rb"
        elif mode == "bw":
            mode = "wb"

        class Buffer(BytesIO):
            # Just to be able to do writing + reading from the same buffer

            _write_line = True

            def set_wrapper(self, wrapper):
                self.wrapper = wrapper

            def write(self, data):
                if mock_target._mirror_on_stderr:
                    if self._write_line:
                        sys.stderr.write(fn + ": ")
                    if isinstance(data, bytes):
                        sys.stderr.write(data.decode("utf8"))
                    else:
                        sys.stderr.write(data)
                    if (data[-1]) == "\n":
                        self._write_line = True
                    else:
                        self._write_line = False
                super(Buffer, self).write(data)

            def close(self):
                if mode[0] == "w":
                    try:
                        mock_target.wrapper.flush()
                    except AttributeError:
                        pass
                    mock_target.fs.get_all_data()[fn] = self.getvalue()
                super(Buffer, self).close()

            def __exit__(self, exc_type, exc_val, exc_tb):
                if not exc_type:
                    self.close()

            def __enter__(self):
                return self

            def readable(self):
                return mode[0] == "r"

            def writeable(self):
                return mode[0] == "w"

            def seekable(self):
                return False

        if mode in ["w", "wb"]:
            wrapper = self.format.pipe_writer(Buffer())
            wrapper.set_wrapper(wrapper)
            return wrapper
        elif mode in ["r", "rb"]:
            return self.format.pipe_reader(Buffer(self.fs.get_all_data()[fn]))
        else:
            raise Exception("mode must be 'r', 'rb', 'w' or 'wb' (got: %s)" % mode)

    def copy(self, new_path, raise_if_exists=False):
        self.fs.copy(self.path, new_path, raise_if_exists)

    def get(self):
        """
        Support the legacy get/put interface
        """
        return self.fs.get_all_data()[self.path]["value"]

    def put(self, value):
        """
        Support the legacy get/put interface

        Stores the value in the cache as a tuple where the first entry is the current time,
        and the second is the actual value.
        """
        self.fs.get_all_data()[self.path] = {"value": value}

    def __del__(self):
        if self.is_tmp and self.exists():
            self.remove()

    def set_metadata(self, path, name: str, value: str):
        """
        Set the metadata
        """
        if isinstance(path, (BaseWrapper, TextIOWrapper)):
            path = path._stream
        if isinstance(path, BytesIO):
            path = self.path
        self.fs.get_all_data().set_metadata(path, name, value)

    def get_metadata(self, path: str, name: str) -> Optional[str]:
        """
        Get the metadata
        """
        return self.fs.get_all_data().get_metadata(path, name)
