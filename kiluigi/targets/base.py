import luigi

from kiluigi.mixins import TagMixin


class Target(TagMixin, luigi.Target):
    @property
    def description(self):
        return self.__class__.__name__
