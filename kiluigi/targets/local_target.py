import datetime
import io
import logging
import os
import platform
import random
import tempfile

import luigi
import pytz
from luigi.format import FileWrapper
from luigi.target import AtomicLocalFile

from ..utils import name_from_class
from .base import Target

# Extended Attributes are only available on macOS and Linux
# On Windows we can substitute NTFS Alternate Data Streams for Extented Attributes
try:
    from xattr import xattr
except ImportError:
    if platform.system() == "Windows":
        from ..utils import xattr_ads as xattr
    else:
        raise

logger = logging.getLogger("luigi-interface")


class NamedAtomicLocalFile(AtomicLocalFile):
    """
    Abstract class to create a Target that creates
    a temporary file in the local filesystem before
    moving it to its final destination.

    This class is just for the writing part of the Target. See
    :class:`luigi.local_target.LocalTarget` for example

    The class enhances luigi.target.AtomicLocalFile to ensure that
    the temporary file has the correct name and extension, because
    some remote Targets, e.g. CkanTarget, rely on it when uploading,
    and DataFrame.to_excel requires it to infer the engine.

    It uses a tempfile.TemporaryDirectory(), which will be automatically
    removed from the filesystem when the Python object is garbage-collected.
    """

    def generate_tmp_path(self, path):
        self.directory = tempfile.TemporaryDirectory()
        return os.path.join(self.directory.name, os.path.basename(path))

    def __del__(self):
        if os.path.exists(self.directory.name):
            self.directory.cleanup()


class atomic_file(NamedAtomicLocalFile):
    """
    Also cleans up the temp file if close is not invoked.

    Uses a subdirectory in the location of the final file so that we can rename
    in situ without crossing a filesystem boundary, which will be quicker than a
    move if the temporary file and the final location are on different devices.
    """

    def generate_tmp_path(self, path):
        directory, filename = os.path.split(path)
        directory = os.path.join(
            directory,
            "luigi-tmp-%09d" % random.randrange(0, 1000000000),  # NOQA: S311
        )
        os.mkdir(directory)
        return os.path.join(directory, filename)

    def __del__(self):
        if os.path.exists(self.tmp_path):
            os.remove(self.tmp_path)
        if os.path.exists(os.path.dirname(self.tmp_path)):
            os.rmdir(os.path.dirname(self.tmp_path))

    def move_to_final_destination(self):
        os.rename(self.tmp_path, self.path)


class LocalTarget(Target, luigi.LocalTarget):
    """
    A better Luigi LocalTarget that reports the full path and supports additional metadata.
    """

    PREFIX = "user.kiluigi"

    def __init__(self, path=None, format=None, is_tmp=False, **kwargs):  # noqa: A002
        """
        Normalize path as part of initialization, including converting Posix directory separators
        to Windows ones, if required.
        """
        if path:
            path = os.path.normpath(os.path.expanduser(path))
        super().__init__(path, format, is_tmp)

    @property
    def last_modified_datetime(self):
        """
        Gets the last modified date of a LocalTarget.

        The file time is returned from the filesystem as seconds since the epoch, and
        does not have any tzinfo. We assume that the filesystem is in UTC, because the
        definition of the epoch is January 1, 1970, 00:00:00 (UTC) on all platforms.
        (see https://docs.python.org/3/library/time.html#time.gmtime). However, if the
        system clock is set to a different timezone, the time will be wrong. Similarly,
        if the local operating system is not set to UTC, then the timestamp returned
        from the operating system to Python may be the local time, not UTC. Therefore,
        you should check the times returned from this method carefully, on the system
        where the code is running.
        """
        return datetime.datetime.fromtimestamp(os.path.getmtime(self.path), pytz.UTC)

    def __str__(self):
        return f"{name_from_class(self).split('.')[-1]}('{self.path}')"

    def exists(self):
        """
        Returns ``True`` if the path for this FileSystemTarget exists; ``False`` otherwise.

        This method is implemented by using :py:attr:`fs`.
        """
        result = super().exists()
        if result:
            logger.debug("Found local file or directory %s", self.path)
        else:
            logger.debug("Cannot find local file or directory %s", self.path)
        return result

    def open(self, mode="r"):
        rwmode = mode.replace("b", "").replace("t", "")
        if rwmode == "w":
            self.makedirs()
            return self.format.pipe_writer(atomic_file(self.path))

        elif rwmode == "r":
            fileobj = FileWrapper(io.BufferedReader(io.FileIO(self.path, mode)))
            return self.format.pipe_reader(fileobj)

        else:
            raise Exception("mode must be 'r' or 'w' (got: %s)" % mode)

    def get_metadata_name(self, name: str):
        """
        Get the full name of an extended attribute.

        Note that on most filesystems, extended attributes must begin `user.`
        """
        name = ".".join([self.PREFIX, name])
        assert name.startswith("user.")
        return name

    def set_metadata(self, file, name: str, value: str):
        """
        Set the metadata as an extended attribute on the file
        """
        xattr(file).set(self.get_metadata_name(name), value.encode())

    def get_metadata(self, file: str, name: str) -> str:
        """
        Get the metadata value from an extended attribute on the file
        """
        try:
            return xattr(file).get(self.get_metadata_name(name)).decode()
        except IOError:
            return None
