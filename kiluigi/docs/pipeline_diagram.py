# -*- coding: utf-8 -*-
r"""
    ext.pipeline_diagram
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Defines a docutils directive for inserting pipeline task DAG (Directed Acyclic Graph) diagrams.

    Provide the directive with the ultimate downstream sub-class of Task in the pipeline.

    Example::

       Given the following classes:

       class A: pass
       class B: def requires(self): return A
       class C: def requires(self): return A
       class D: def requires(self): return B, C
       class E: def requires(self): return B

       .. pipeline-diagram:: mod.D

       Produces a graph like the following:

                   A
                  / \
                 B   C
                  \ /
                   D

    The graph is inserted as a PNG+image map into HTML and a PDF in
    LaTeX.

    :copyright: Copyright 2018 Kimetrica, see AUTHORS.
    :license: See LICENSE for details.
"""
import datetime
import re
from hashlib import md5
from itertools import chain

import sphinx
from docutils import nodes
from docutils.parsers.rst import Directive, directives  # type: ignore
from sphinx.ext.graphviz import (
    figure_wrapper,
    render_dot_html,
    render_dot_latex,
    render_dot_texinfo,
)

from kiluigi.docs.dot_renderer import Renderer

module_sig_re = re.compile(
    r"""^(?:([\w.]*)\.)?  # module names
    (\w+)  \s* $          # class/final module name
    """,
    re.VERBOSE,
)


class PipelineException(Exception):
    pass


class PipelineGraph:
    def __init__(
        self,
        class_names,
        currmodule,
        tags=None,
        display_tasks=True,
        display_outputs=True,
        bases=None,
        page_url=None,
    ):
        """
        *class_names* is a list of task classes to include in the diagram.
        """
        self.class_names = class_names
        self.tags = tags
        self.display_tasks = display_tasks
        self.display_outputs = display_outputs
        self.bases = bases
        self.page_url = page_url

    def generate_dot(
        self, guid, env=None, graph_attrs=None, node_attrs=None, edge_attrs=None
    ):

        renderer = Renderer(
            tags=self.tags,
            display_tasks=self.display_tasks,
            display_outputs=self.display_outputs,
            bases=self.bases,
            page_url=self.page_url,
            graph_attrs=graph_attrs,
            node_attrs=node_attrs,
            edge_attrs=edge_attrs,
        )

        return renderer.render(self.class_names)

    @property
    def default_title(self):
        return "Pipeline diagram of {classes} on {utc}".format(
            classes=", ".join(self.class_names),
            utc=datetime.datetime.utcnow().strftime("%a %d %b %H:%M"),
        )  # formal: ('%Y-%m-%d %H:%M')


class pipeline_diagram(nodes.General, nodes.Element):  # type: ignore
    """
    A docutils node to use as a placeholder for the pipeline diagram.
    """

    pass


class PipelineDiagram(Directive):
    """
    Run when the pipeline_diagram directive is first encountered.
    """

    has_content = False
    required_arguments = 1
    optional_arguments = 99
    final_argument_whitespace = True
    option_spec = {
        "tags": directives.unchanged,
        "omit_tasks": directives.flag,
        "omit_outputs": directives.flag,
        "bases": directives.unchanged,
        "caption": directives.unchanged,
        "page_url": directives.unchanged,
    }

    def run(self):
        node = pipeline_diagram()
        node.document = self.state.document
        env = self.state.document.settings.env
        class_names = list(chain.from_iterable([a.split() for a in self.arguments]))
        # Store the original content for use as a hash

        # Create a graph starting with the list of classes
        try:
            graph = PipelineGraph(
                class_names,
                env.ref_context.get("py:module"),
                tags=self.options.get("tags", "").split(),
                display_tasks="omit_tasks" not in self.options,
                display_outputs="omit_outputs" not in self.options,
                bases=self.options.get("bases", "").split(),
                page_url=self.options.get("page_url", ""),
            )

        except PipelineException as err:
            return [node.document.reporter.warning(err.args[0], line=self.lineno)]

        # Store the graph object so we can use it to generate the
        # dot file later
        node["graph"] = graph
        node["caption"] = self.options.get("caption", "")

        # wrap the result in figure node
        if node["caption"]:
            node = figure_wrapper(self, node, node["caption"])
        return [node]


def get_graph_hash(node):
    encoded = "".join(
        [
            ", ".join(node["graph"].class_names),
            str(node["graph"].tags),
            str(node["graph"].bases) + node["caption"],
        ]
    ).encode("utf-8")
    return md5(encoded).hexdigest()[-10:]  # noqa S303


def html_visit_pipeline_diagram(self, node):
    """
    Output the graph for HTML. If graphviz_output_format=svg,
    this will insert a PNG with clickable, hoverable image map.
    """
    graph = node["graph"]

    graph_hash = get_graph_hash(node)

    # For narrow templates pass `graph_attrs={"size": "7.5,3000"}` to get_task_dag_as_dot
    dotcode = graph.generate_dot(graph_hash, env=self.builder.env)

    render_dot_html(
        self, node, dotcode, {}, "pipeline", "pipeline", alt=graph.default_title
    )
    raise nodes.SkipNode


def latex_visit_pipeline_diagram(self, node):
    """
    Output the graph for LaTeX.  This will insert a PDF.
    """
    graph = node["graph"]

    graph_hash = get_graph_hash(node)

    dotcode = graph.generate_dot(graph_hash, env=self.builder.env)
    # Fix width, otherwise Sphinx shrinks image to column width, but not image map, so hovers and hyperlinks misalign
    render_dot_latex(self, node, dotcode, {}, "pipeline")
    raise nodes.SkipNode


def texinfo_visit_pipeline_diagram(self, node):
    """
    Output the graph for Texinfo.  This will insert a PNG.
    """
    graph = node["graph"]

    graph_hash = get_graph_hash(node)

    dotcode = graph.generate_dot(
        graph_hash, env=self.builder.env, graph_attrs={"size": '"6.0,6.0"'}
    )
    render_dot_texinfo(self, node, dotcode, {}, "pipeline")
    raise nodes.SkipNode


def skip(self, node):
    raise nodes.SkipNode


def setup(app):
    app.setup_extension("sphinx.ext.graphviz")
    app.add_node(
        pipeline_diagram,
        latex=(latex_visit_pipeline_diagram, None),
        html=(html_visit_pipeline_diagram, None),
        text=(skip, None),
        man=(skip, None),
        texinfo=(texinfo_visit_pipeline_diagram, None),
    )
    app.add_directive("pipeline-diagram", PipelineDiagram)
    return {"version": sphinx.__display_version__, "parallel_read_safe": True}
