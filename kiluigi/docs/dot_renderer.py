import abc
import datetime
from copy import deepcopy
from inspect import getdoc, isclass

import six
from luigi.task import flatten

from kiluigi.docs.utils import construct_url, sanitize_for_tooltip
from kiluigi.utils import MockConfig, class_from_name, name_from_class


class Renderer:
    def __init__(
        self,
        tags=None,
        display_tasks=True,
        display_outputs=True,
        bases=None,
        page_url=None,
        graph_attrs=None,
        node_attrs=None,
        edge_attrs=None,
    ):
        self.tags = tags or []
        self.display_tasks = display_tasks
        self.display_outputs = display_outputs
        if isclass(bases) or isinstance(bases, str):
            bases = (bases,)
        self.bases = bases or []
        self.page_url = page_url
        self.graph_attrs = {
            "rankdir": "TB"
        }  # for narrow column docs templates: ,"size": "7.5,200"
        self.graph_attrs.update(graph_attrs or {})
        self.node_attrs = {"style": "filled", "fontname": "tahoma"}
        self.node_attrs.update(node_attrs or {})
        self.edge_attrs = {
            "color": "#4a4c55",
            "splines": "curved",
            "arrowhead": "normal",
        }
        self.edge_attrs.update(edge_attrs or {})

    def is_displayed(self, obj):
        # If tag filter specified, only show tasks/targets if their tags intersect with filter tags.
        tag_match = not self.tags or (
            hasattr(obj, "get_tags") and not obj.get_tags().isdisjoint(self.tags)
        )
        base_match = not self.bases or isinstance(obj, self.bases)
        return tag_match and base_match

    @classmethod
    def extract_dag_for_dot(cls, tasks, depth=0, done=None):
        tasks = tasks if hasattr(tasks, "__iter__") else [tasks]
        done = done or {}
        task_nodes = []
        for task in tasks:
            if isinstance(task, str):
                task = class_from_name(task)
            if isclass(task):
                task = task()
            sources = flatten(task.requires())
            task_node = cls.TaskNode(
                task, sources, depth, Renderer.OutputNode(flatten(task.output()))
            )
            task_nodes.append(task_node)
            done[task.task_id] = task_node
            sources_not_yet_traversed = [t for t in sources if t.task_id not in done]
            task_nodes += Renderer.extract_dag_for_dot(
                sources_not_yet_traversed, depth + 1, done
            )
            for t in sources:
                task_node.source_output_nodes.append(done[t.task_id].output_node)
        return task_nodes

    def render(self, tasks):
        tasks = tasks if hasattr(tasks, "__iter__") else [tasks]

        dot = ["digraph schedule {"]

        # DEBUG: self.graph_attrs['label'] = tasks
        if "tooltip" not in self.graph_attrs:

            graph_tooltip = sanitize_for_tooltip(
                "{task_list}\n{utc}".format(
                    task_list="\n".join(str(t) for t in tasks),
                    utc=datetime.datetime.utcnow().strftime("%a %d %b %H:%M"),
                )
            )  # formal: ('%Y-%m-%d %H:%M')

            dot.append('\ttooltip="%s"' % graph_tooltip)

        dot += ['\t%s="%s" ' % (key, val) for key, val in self.graph_attrs.items()]

        with MockConfig():
            task_nodes = self.extract_dag_for_dot(tasks)
            self.bases = tuple(
                class_from_name(b) if isinstance(b, str) else b for b in self.bases
            )

            assert self.bases is None or all(
                (isclass(b) for b in self.bases)
            ), "The base classes to be displayed must be a list of non-instantiated classes."

            task_nodes.sort(key=lambda x: x.rank)

            dot.append("\n// Task and Output Nodes\n")
            for task_node in task_nodes:
                dot.append(task_node.render_node(self))
                dot.append(task_node.output_node.render_node(self))

            dot.append("\n// Edges Task to Output\n")
            for task_node in task_nodes:
                dot.append(task_node.render_task_to_output_edge(self))

            dot.append("\n// Edges Output to Dependent Task\n")
            for task_node in task_nodes:
                dot.append(task_node.render_targets_to_task_edges(self))

        dot.append("}")

        return " \n".join(dot)

    @six.add_metaclass(abc.ABCMeta)
    class Node:
        @property
        def rank(self):
            return self.depth

        # @property
        # def node_id(self):
        #     # Override this and loop over each unique id to merge nodes
        #     return re.sub('[^0-9a-zA-Z]+', '_', self.label)

        def render_node(self, renderer=None):
            self.renderer = renderer or self.renderer
            assert isinstance(self.renderer, Renderer), "Renderer not configured."
            node_attrs = {
                "color": self.color,
                "fillcolor": self.fillcolor,
                "shape": self.shape,
                "label": self.label if self.is_displayed else "",
                "href": self.url,
                "tooltip": self.tooltip,
            }
            node_attrs.update(self.renderer.node_attrs)
            dot = " ".join(['%s="%s"' % (key, val) for key, val in node_attrs.items()])
            return "{id} [{node_attrs}];\n".format(id=self.node_id, node_attrs=dot)

    class OutputNode(Node):
        def __init__(self, targets):
            self.targets = targets
            self.task_node = None
            self.renderer = None

        @property
        def node_id(self):
            return "Outputs_Of_%s" % self.task_node.node_id

        @property
        def color(self):
            return "dodgerblue4"

        @property
        def fillcolor(self):
            return "#d1efff"

        @property
        def rank(self):
            # Add half to depth for rank, so outputs appear after their targets
            return self.task_node.depth + 0.5

        @property
        def label(self):
            return "".join(  # noqa: W605
                r"%s:%s\l" % (target.__class__.__name__, str(target))
                for target in self.targets
                if self.renderer.is_displayed(target)
            )

        @property
        def shape(self):
            return "note" if self.is_displayed else "point"

        @property
        def is_displayed(self):
            return self.renderer.display_outputs and any(
                (self.renderer.is_displayed(target) for target in self.targets)
            )

        @property
        def url(self):
            # We are displaying several targets in one node, and a single node can't hyperlink to more than one URL
            return self.task_node.url

        @property
        def tooltip(self):
            # doc = '\n'.join(getdoc(target) for target in self.targets if getdoc(target))
            doc = getdoc(self.task_node.task.output)
            return sanitize_for_tooltip(doc)

    class TaskNode(Node):
        def __init__(self, task, source_tasks, depth, output_node):
            self.task = task
            self.source_tasks = source_tasks
            self.depth = depth
            output_node.task_node = self
            self.output_node = output_node
            self.renderer = None
            self.source_output_nodes = []

        @property
        def node_id(self):
            return self.task.task_id.replace(".", "__")

        @property
        def fillcolor(self):
            return "#e4f6d7"

        @property
        def color(self):
            return "#487327"

        @property
        def tags(self):
            return self.task.get_tags() if hasattr(self.task, "get_tags") else []

        @property
        def is_displayed(self):
            return self.renderer.is_displayed(self.task) and self.renderer.display_tasks

        @property
        def label(self):
            return name_from_class(self.task)

        @property
        def shape(self):
            return "box" if self.is_displayed else "point"

        @property
        def url(self):
            return construct_url(self.task, documentation_url=self.renderer.page_url)

        @property
        def tooltip(self):
            doc = getdoc(self.task)
            return sanitize_for_tooltip(doc)

        def render_task_to_output_edge(self, renderer=None):
            self.renderer = renderer or self.renderer
            assert isinstance(self.renderer, Renderer), "Renderer not configured."
            edge_attrs = deepcopy(self.renderer.edge_attrs)
            if not self.output_node.is_displayed:
                edge_attrs["arrowhead"] = "none"
            dot = " ".join(['%s="%s"' % (key, val) for key, val in edge_attrs.items()])
            return "{src_node} -> {dst_node} [{edge_attrs}];".format(
                src_node=self.node_id, dst_node=self.output_node.node_id, edge_attrs=dot
            )

        def render_targets_to_task_edges(self, renderer=None):
            self.renderer = renderer or self.renderer
            assert isinstance(self.renderer, Renderer), "Renderer not configured."
            dot = []
            for src_output_node in self.source_output_nodes:
                edge_attrs = deepcopy(self.renderer.edge_attrs)
                if not self.is_displayed:
                    edge_attrs["arrowhead"] = "none"
                attrs = " ".join(
                    ['%s="%s"' % (key, val) for key, val in edge_attrs.items()]
                )
                dot.append(
                    "{src_node} -> {dst_node} [{edge_attrs}];".format(
                        src_node=src_output_node.node_id,
                        dst_node=self.node_id,
                        edge_attrs=attrs,
                    )
                )
            return " \n".join(dot)
