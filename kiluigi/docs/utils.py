# black insists on no lines at the top, flake8 insists on 2. With this comment here both are happy.


def get_task_dag_as_dot(
    tasks, tags=None, display_tasks=True, display_outputs=True, bases=None
):
    """
    Usage example:

        dot = get_task_dag_as_dot(['models.darpa_economic_model.economic_model_pipeline.CutRasterForTrainingData',
                            'models.darpa_economic_model.economic_model_pipeline.CreateSurfacePlot',
                            'models.darpa_economic_model.economic_model_pipeline.UploadToCkan'], )
                                                                        # bases=[Task, CkanTarget],
                                                                        # display_tasks=False
                                                                        # display_outputs=False
                                                                        # tags='this,another'.split(',')

    To render (requires that ``dot`` is installed):

        dir = os.path.abspath('/path/to/dags/dir')
        filename_base = os.path.join(dir, 'dot_%s' % datetime.datetime.utcnow().strftime('%Y%m%d%H%M'))
        if not os.path.exists(dir):
            os.makedirs(dir)
        with open('%s.dot' % filename_base, 'w+') as dot_file:
            print(dot, file=dot_file)

        from subprocess import check_call
        check_call(['dot','-Tpng','%s.dot' % filename_base,'-o','%s.png' % filename_base])
        check_call(['dot','-Tps','%s.dot' % filename_base,'-o','%s.ps' % filename_base])
        check_call(['dot','-Tsvg','%s.dot' % filename_base,'-o','%s.svg' % filename_base])
    """
    from kiluigi.docs.dot_renderer import Renderer

    renderer = Renderer(tags, display_tasks, display_outputs, bases)
    return renderer.render(tasks)


HTML_ENTITIES_NEWLINE = "&#13;&#10;"  # have to use entities for tooltip new lines


def sanitize_for_tooltip(text):
    """Cleans text for rendering in an HTML page tooltip."""
    return text.replace('"', '\\"').replace("\n", HTML_ENTITIES_NEWLINE) if text else ""


def construct_url(task_instance, documentation_url=None):
    return "{page_url}#{module}.{class_name}".format(
        page_url=documentation_url or "",
        module=type(task_instance).__module__,
        class_name=type(task_instance).__name__,
    )
