"""
Specific Exception subclasses for Luigi pipelines.
"""


class PipelineError(Exception):
    """
    Exception raised for errors in Luigi pipelines.

    Allows for a list of error messages to be passed.
    """

    def __init__(self, message, errors=None):
        self.message = message
        self.errors = errors

    def __str__(self):
        message = [self.message]
        if self.errors:
            message.append("Errors:")
            message = message + self.errors
        return "\n".join(message)
