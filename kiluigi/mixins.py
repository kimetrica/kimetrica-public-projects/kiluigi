from typing import Set


class TagMixin:
    """
    Adds the ability to set tags on targets and tasks. These are then used by documentation
    tools to filter or identify nodes (be they targets or tasks) to display or hide. They
    may also be useful for searching or browsing tasks via the API.

    Examples include thematic area (eg, demography), analysis type (eg, regression),
    data structure (eg, tabular or spatial), data state (eg, normalized or final),
    ontological area, etc.

    Nb.
    Task tags are stored on a class-level attribute, as all instances of eg,
    economic_pipeline.MergeRasterEconomicClusters will be tagged the same.
    Target tags are stored in an instance-level attribute, as one instance of eg,
    CkanTarget won't want the tags of every other instance of CkanTarget.
    """

    def __init__(self, *args, tags=None, **kwargs):

        from kiluigi import Task  # kiluigi.tasks imports TagMixin

        if not isinstance(self, Task):
            self._tags = tags or set()  # Targets require an instance-level set of tags
        super().__init__(*args, **kwargs)

    # A set of tags at class level for Tasks, as opposed to instance level for Targets
    _tags: Set[str] = set()  # type: ignore

    def get_tags(self):
        tags = set()
        for klas in self.__class__.__mro__[:-1]:
            if hasattr(klas, "_tags") and klas._tags:
                # Tags are inherited from parent classes
                tags |= klas._tags if isinstance(klas._tags, set) else set(klas._tags)
        if hasattr(self, "_tags"):
            tags |= self._tags if isinstance(self._tags, set) else set(self._tags)
        return tags
