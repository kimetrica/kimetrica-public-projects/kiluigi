import os
import re
from json.decoder import JSONDecodeError
from tempfile import TemporaryDirectory

import geojson
import geojson.utils
import kml2geojson
from luigi import Parameter


class GeoParameter(Parameter):
    """Parameter whose value is a ``GeoJSON`` object. Parses KML data into GeoJSON."""

    def __init__(self, *args, always_in_help=True, **kwargs):
        """Override __init__ to pass always_in_help=True as default.

        Otherwise signature is like base class.
        """
        super().__init__(*args, always_in_help=always_in_help, **kwargs)

    def parse(self, s):
        """Parse input to GeoJSON.

        Input is:

        - GeoJSON string or GeoJSON file path
        - KML string or KML file path
        """
        s = self.get_geojson_from_input(s)
        return s

    def serialize(self, x):
        """Return a string representation for a GeoJSON object."""
        return geojson.dumps(x)

    def get_geojson_from_kml_file(self, input_file_path):
        """Use kml2geojson to get GeoJSON from KML file."""
        kml_filename = os.path.basename(input_file_path)
        output_dir = os.path.dirname(input_file_path)
        kml2geojson.main.convert(input_file_path, output_dir)
        output_filename = kml_filename.replace(".kml", ".geojson").replace(" ", "_")
        geo_json_file_path = os.path.join(output_dir, output_filename)
        with open(geo_json_file_path) as geo_json_file:
            geo_json = geojson.load(geo_json_file)
        return geo_json

    def get_name_of_kml_data(self, kml_str):
        """Retrieve the name of a KML data file. This will be found in <name></name> tag."""
        # match will be None if no `<name>` tag, or it will contain the value
        name = None
        match = re.search(r"<name>(?P<kml_name>.*)</name>", kml_str, re.MULTILINE)
        try:
            name = match.groupdict()["kml_name"]
        except AttributeError:
            # regex didn't match, so match is None
            pass
        return name

    def get_geojson_from_kml_string(self, input_string):
        """Convert KML string to GeoJSON.

        kml2geojson library works with files, i.e KML file into GeoJSON file so we
        need to write the input into a file and read the resulting GeoJSON file.

        We use a TemporaryDirectory so we can use it both for the temporary KML
        file and as output directory for kml2geojson.
        kml2geojson will replace spaces in the filename with underscores.
        """
        # directory is automatically removed once the with block exits
        with TemporaryDirectory() as temp_dir:
            # fall back to kml_input, if we don't have a <name> tag
            # using a static filename here is safe because of TemporaryDirectory
            kml_file_name = "{}.kml".format(
                self.get_name_of_kml_data(input_string) or "kml_input"
            )
            kml_file_path = os.path.join(temp_dir, kml_file_name)
            with open(kml_file_path, "w") as kml_file:
                kml_file.write(input_string)
            geo_json = self.get_geojson_from_kml_file(kml_file_path)
        return geo_json

    def get_geojson_from_input(self, input_path_or_str):
        """Convert input (path to file or KML/GeoJSON string) to GeoJSON.

        Try to open the argument to parse as a file. If it fails, it might be a string.
        If it works, you will get a string.
        So either way you have a string then. Then just try to parse it as GeoJSON (geojson.loads).
        If it works, then all is good. If not, you can try to run kml2geojson on it.
        """
        try:
            with open(input_path_or_str) as input_file:
                input_str = input_file.read()
        # OSError will be raised if the filename is too long, which will be the case
        # if we are dealing with a string with GeoJSON/KML
        except (FileNotFoundError, OSError):
            # our input is not a file, so it's a string with either KML or GeoJSON
            input_str = input_path_or_str

        try:
            geo_json = geojson.loads(input_str)
        except JSONDecodeError:
            # we couldn't parse a string, probably we are dealing with KML
            geo_json = self.get_geojson_from_kml_string(input_str)
        return geo_json
