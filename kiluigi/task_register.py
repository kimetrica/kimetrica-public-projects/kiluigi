import datetime
import logging
import tempfile
from inspect import getdoc
from typing import Iterator

import networkx as nx
from luigi import Task, task_register
from luigi.parameter import MissingParameterException
from networkx.drawing.nx_agraph import write_dot

from kiluigi.docs.utils import construct_url, sanitize_for_tooltip

logger = logging.getLogger("luigi-interface")


class Register(task_register.Register):
    """kiluigi.Task metaclass singleton for task DAG-wide functionality.
    Tracks all Task instances and parameters, extracts variable CAG.
    """

    @classmethod
    def get_all_user_tasks(cls) -> Iterator[tuple]:
        """
        Returns all :py:class:`Task` classes of interest to users (so excluding luigi.Task, etc).

        :return: a generator of tuples
        """
        for task_name, task_cls in cls._get_reg().items():
            if task_cls == cls.AMBIGUOUS_CLASS or "." not in task_cls.get_task_family():
                continue
            yield task_name, task_cls

    @classmethod
    def get_all_user_params(cls) -> Iterator[tuple]:
        """
        Returns all parameters for all :py:class:`Task` of interest to users (so excluding luigi.Task, etc).
        We may want to add a property on kiluigi.Parameter that determines whether it is a GUI/user parameter.

        :return: a generator of tuples
        """
        for task_name, task_cls in cls.get_all_user_tasks():
            for param_name, param_obj in task_cls.get_params():
                yield task_name, (
                    not task_cls.use_cmdline_section
                ), param_name, param_obj

    INFLUENCE_GRID_CACHE_TIMEOUT = 60 * 15  # 15 mins

    @classmethod
    def get_variable_influence_grids(cls):
        if (
            not hasattr(cls, "cached_variable_grids")
            or not cls.cached_variable_grids
            or not cls.cached_variable_grids[0]  # dict of grids
            or cls.cached_variable_grids[1]  # if warnings, recalculate
            or cls.cached_variable_grids[2] < datetime.datetime.utcnow()  # expiry time
        ):
            grids = {}
            warnings = []
            for task in cls._reg:
                if hasattr(task, "get_influence_grid"):
                    task = task()
                    influences = task.get_influence_grid()
                    if not influences:
                        warnings.append(
                            f"Influence grid for {type(task).__name__} missing, "
                            "so the vCAG omits that task's linkages."
                        )
                        logger.debug(warnings[-1])
                        continue
                    influenced_variable_name, influencer_grid = influences
                    grids[influenced_variable_name] = influencer_grid
            if warnings:
                # If there were warnings, don't cache
                return (grids, warnings)
            expiry = datetime.datetime.utcnow() + datetime.timedelta(
                seconds=cls.INFLUENCE_GRID_CACHE_TIMEOUT
            )
            cls.cached_variable_grids = (grids, warnings, expiry)
        return cls.cached_variable_grids[0:2]

    @classmethod
    def get_variable_metadata(cls):
        if (
            not hasattr(cls, "cached_variable_metadata")
            or not cls.cached_variable_metadata
            # first element is a dict of metadata keyed on variable name
            or not cls.cached_variable_metadata[0]
        ):
            # Cache without expiry, all data is static, ie, from code, so won't change without server restart
            from kiluigi import VariableTask

            all_vars_metadata = {}
            warnings = []
            for _task_name, task in cls.get_all_user_tasks():
                try:
                    task = task()
                except MissingParameterException:
                    warnings.append(
                        f"Task {task.__name__} has parameters which are missing default values. Skipping task."
                    )
                    logger.debug(warnings[-1])
                    continue
                task_meta = (
                    task.get_variable_metadata()
                    if hasattr(task, "get_variable_metadata")
                    else {}
                )
                if not task_meta or not isinstance(task_meta, dict):
                    warnings.append(
                        f"Variable metadata for {type(task).__name__} missing or not a dict. Skipping task."
                    )
                    logger.debug(warnings[-1])
                    continue
                for var_name, var_meta in task_meta.items():
                    if not var_meta or not isinstance(var_meta, dict):
                        warnings.append(
                            f"Variable metadata invalid for variable {var_name} on task {type(task).__name__}."
                        )
                        logger.debug(warnings[-1])
                        var_meta = (
                            {}
                        )  # can at least attach task for extracting its docstring
                    if VariableTask.SOURCE_TASK not in var_meta or not isinstance(
                        var_meta[VariableTask.SOURCE_TASK], Task
                    ):
                        var_meta[VariableTask.SOURCE_TASK] = task
                    if var_name in all_vars_metadata:
                        warnings.append(
                            f"Tasks {type(task).__name__} and "
                            f"{type(all_vars_metadata[var_name][VariableTask.SOURCE_TASK]).__name__} "
                            f"both provide metadata for variable name {var_name}. "
                            f"Using version on {type(all_vars_metadata[var_name][VariableTask.SOURCE_TASK]).__name__}."
                        )
                        logger.debug(warnings[-1])
                        continue
                    all_vars_metadata[var_name] = var_meta
            cls.cached_variable_metadata = (all_vars_metadata, warnings)

        return cls.cached_variable_metadata

    @classmethod
    def get_variable_cag(cls):
        grids, warnings = cls.get_variable_influence_grids()
        if warnings:
            logger.info("\n".join(warnings))
        if not grids:
            warnings.append(
                "Variable CAG requested but required influence grids haven't yet been processed."
            )
            logger.info(warnings[-1])
            return None, None, warnings

        # set of variable names (strings)
        nodes = set()

        # set of 3-element tuples containing (src_var_name (str), dest_var_name (str), influence_percent (int))
        edges = set()

        for dest_var, influences in grids.items():
            nodes.add(dest_var)
            nodes |= set(influences["source_variable"].tolist())
            # tuples are hashable for sets, dicts are not (as they're not ordered)
            edges |= {
                (influence["source_variable"], dest_var, influence["influence_percent"])
                for influence in influences.to_records()
            }

        return nodes, edges, warnings

    @classmethod
    def get_variable_cag_as_nx(
        cls, graph_attrs=None, node_attrs=None, edge_attrs=None, documentation_url=None
    ):
        from kiluigi import VariableTask

        graph_attrs = graph_attrs or {}
        node_attrs = node_attrs or {}
        edge_attrs = edge_attrs or {}

        nodes, edges, warnings = cls.get_variable_cag()
        if not nodes:
            warnings.append(
                "NetworkX Variable CAG requested but required influence grids haven't been processed."
            )
            logger.info(warnings[-1])
            return None, warnings

        var_metadata, var_meta_warnings = cls.get_variable_metadata()
        if var_meta_warnings:
            # Logged but not added to warnings, as CAG warnings are displayed to the user.
            logger.info("\n".join(var_meta_warnings))

        G = nx.DiGraph(**graph_attrs)
        G.add_nodes_from(
            [
                (
                    var_name,
                    {
                        "tooltip": sanitize_for_tooltip(
                            "\n".join(
                                [
                                    getdoc(
                                        var_metadata.get(var_name, {}).get(
                                            VariableTask.SOURCE_TASK, None
                                        )
                                    )
                                ]
                                + [
                                    f"{key}: {val}"
                                    for key, val in var_metadata.get(
                                        var_name, {}
                                    ).items()
                                ]
                            )
                        )
                        if var_metadata.get(var_name, {}).get(
                            VariableTask.SOURCE_TASK, None
                        )
                        else "",
                        # If we omit documentation_url this could jump to the section of the form on the
                        # trigger page with the input controls for the appropriate task (if we can insert
                        # <a name=""></a> links inside the Django admin form.
                        # If we include it it could jump to our Gitlab Pages docs page.
                        "href": construct_url(
                            task_instance=var_metadata.get(var_name, {}).get(
                                VariableTask.SOURCE_TASK, None
                            ),
                            documentation_url=documentation_url,
                        )
                        if var_metadata.get(var_name, {}).get(
                            VariableTask.SOURCE_TASK, None
                        )
                        else "",
                    },
                )
                for var_name in nodes
                if isinstance(var_name, str)
            ],
            **node_attrs,
        )
        G.add_edges_from(
            [
                (
                    edge[0],  # src_var_name
                    edge[1],  # dest_var_name
                    {
                        # label and penwidth are DOT syntax, edge[2] is influence_percent
                        "label": f"{int(round(edge[2] * 100))}%",
                        "penwidth": round(edge[2] * 10),
                    },
                )
                for edge in edges
            ],
            **edge_attrs,
        )
        return G, warnings

    @classmethod
    def get_variable_cag_as_dot(
        cls, graph_attrs=None, node_attrs=None, edge_attrs=None
    ):
        # for narrow column docs templates, add: ,"size": "7.5,200" to default_graph_attrs
        default_graph_attrs = {"rankdir": "TB"}
        default_graph_attrs.update(graph_attrs or {})
        default_node_attrs = {
            "style": "filled",
            "fontname": "tahoma",
            "color": "dodgerblue4",
            "color": "dodgerblue4",
            "fillcolor": "#d1efff",
            "shape": "note",
        }
        default_node_attrs.update(node_attrs or {})
        default_edge_attrs = {
            "color": "#4a4c55",
            "splines": "curved",
            "arrowhead": "normal",
            "fontname": "tahoma",
        }
        default_edge_attrs.update(edge_attrs or {})

        # If we stop using networkx, the code to render this as dot without networkx is on
        # commit 46c5906827dd2a267088f4abc53176deb40144a3
        G, warnings = cls.get_variable_cag_as_nx(
            default_graph_attrs, default_node_attrs, default_edge_attrs
        )

        if not G or len(G.nodes) == 0:
            warnings.append(
                "Variable CAG requested as DOT, but a required influence grids haven't yet been processed."
            )
            logger.info(warnings[-1])
            return None, warnings

        with tempfile.TemporaryFile() as f:
            write_dot(G, f)
            f.seek(0)
            return f.read().decode(), warnings
