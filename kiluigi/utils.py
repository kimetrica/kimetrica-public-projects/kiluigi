"""
Various utlity functions to help with Data Pipeline development
"""
import configparser
import contextlib
import datetime
import functools
import importlib
import inspect
import itertools
import logging
import os
import unittest
from ast import literal_eval
from configparser import NoSectionError
from contextlib import ContextDecorator, contextmanager
from io import StringIO
from typing import IO, Optional

import luigi
from luigi import rpc, scheduler, worker
from luigi.cmdline_parser import CmdlineParser
from luigi.date_interval import DateInterval
from luigi.execution_summary import LuigiStatusCode
from luigi.interface import _schedule_and_run
from luigi.parameter import (
    BoolParameter,
    ChoiceParameter,
    DateIntervalParameter,
    DateParameter,
    DictParameter,
    EnumParameter,
    FloatParameter,
    IntParameter,
    ListParameter,
    NumericalParameter,
    Parameter,
    TaskParameter,
    TimeDeltaParameter,
    TupleParameter,
    YearParameter,
    _no_value,
)
from luigi.task import flatten
from luigi.task_register import Register

import kiluigi
from kiluigi.exceptions import PipelineError
from kiluigi.parameter import GeoParameter

CONFIG = luigi.configuration.get_config()

logger = logging.getLogger("luigi-interface")


def name_from_class(obj):
    """
    Get the fully qualified dot-notation string name for an object or class
    """
    if not isinstance(obj, type):
        # o is not a class, so we need to get the class from the object
        obj = obj.__class__
    module = "" if obj.__module__ in (None, str.__module__) else obj.__module__ + "."
    return module + obj.__qualname__


def class_from_name(full_name):
    """
    Load a class from a dot-notation string name
    """
    try:
        module_name, class_name = full_name.rsplit(".", 1)
    except ValueError as e:
        raise ValueError(
            f"Can't extract separate module and class names from '{full_name}'"
        ) from e
    # load the module, will raise ImportError if module cannot be loaded
    m = importlib.import_module(module_name)
    # get the class, will raise AttributeError if class cannot be found
    c = getattr(m, class_name)
    return c


@contextlib.contextmanager
def local_logging(logger=None, level=logging.DEBUG):
    """
    Context handler that logs messages to a StringIO object
    """
    if not isinstance(logger, logging.Logger):
        logger = logging.getLogger(logger)

    buffer = StringIO()
    handler = logging.StreamHandler(buffer)
    handler.setLevel(level)
    formatter = logging.Formatter(
        "%(asctime)s - %(message)s", datefmt="%d/%b/%Y %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    try:
        yield buffer

    finally:
        # StreamHandler doesn't close the stream (because it could be stdout), so reset so that read() will work
        buffer.seek(0)
        logger.removeHandler(handler)


class xattr_ads:
    """
    A Windows-compatible implementation of the xattr API implemented
    using Alternate Data Streams (ADS).

    See https://github.com/xattr/xattr for the macOS / Linux equivalent

    Note that we read and write UTF-8 encoded bytes to the ADS file for
    compatibility with xattr.
    """

    def __init__(self, file_obj: IO) -> None:
        self.file_obj = file_obj

    def ads_filename(self, name: str) -> str:
        filename = (
            self.file_obj if isinstance(self.file_obj, str) else self.file_obj.name
        )
        return f"{filename}:{name}"

    def set(self, name: str, value: bytes):  # noqa: A003
        with open(self.ads_filename(name), "wb") as fd:
            fd.write(value)

    def get(self, name: str) -> Optional[bytes]:
        try:
            with open(self.ads_filename(name), "rb") as fd:
                return fd.read()
        except FileNotFoundError:
            return None


def path_from_task(task: luigi.Task):
    """
    Returns a valid relative path based on a task.

    Paths are organized into a folder hierarchy based on the task family, with a final component based on the
    significant parameters.

    For example, a task with task_id `survey.pipelines.fewsnet_crop_ingestion.FetchRemoteData___SO_AG_2017_321aa7df46`
    would return a path like:

        survey/pipelines/fewsnet_crop_ingestion/FetchRemoteData/FetchRemoteData___SO_AG_2017_321aa7df46

    Note that unlike a naive `task.task_id.replace(".", os.path.sep)` this will gather files for the same Task under
    a single directory, rather than grouping files for all the Tasks in the module in a single directory.
    """
    # Use the dotted path of the task family as a directory hierarchy
    path = task.get_task_family().replace(".", os.path.sep)
    # And use the final part of the task_id as the file name
    name = task.task_id.split(".")[-1]
    path = os.path.join(path, name)
    return path


def date_label(
    start: Optional[datetime.date] = None, end: Optional[datetime.date] = None
) -> str:
    """
    Get a label that describes the validity period of a date range.

    Useful for labelling Targets for Tasks with start and end as significant parameters
    """
    if start and not end:
        date_text = f'_from_{start.strftime("%Y%m%d")}'
    elif end and not start:
        date_text = f'_until_{end.strftime("%Y%m%d")}'
    elif start and end:
        date_text = f'_{start.strftime("%Y%m%d")}-{end.strftime("%Y%m%d")}'
    else:
        date_text = ""
    return date_text


class timeit(ContextDecorator):
    def __enter__(self):
        self.start = datetime.datetime.now(datetime.timezone.utc)
        return self

    def __exit__(self, *exc):
        self.stop = datetime.datetime.now(datetime.timezone.utc)
        return False

    @property
    def elapsed(self):
        try:
            return self.stop - self.start
        except AttributeError:
            return datetime.datetime.now(datetime.timezone.utc) - self.start

    def total_seconds(self, decimal_places: Optional[int] = None):
        if isinstance(decimal_places, int):
            return round(self.elapsed.total_seconds(), decimal_places)
        return self.elapsed.total_seconds()


class with_config(ContextDecorator):
    """
    Decorator to override config settings for a test method or test class.

    Based on luigi/test/helper.py but extended to allow decorating a unittest.TestCase subclass.

    Usage:
    .. code-block: python
        >>> import luigi.configuration
        >>> @with_config({'foo': {'bar': 'baz'}})
        ... def my_test():
        ...     print(luigi.configuration.get_config().get("foo", "bar"))
        ...
        >>> my_test()
        baz
        >>> @with_config({'hoo': {'bar': 'buz'}})
        ... @with_config({'foo': {'bar': 'baz'}})
        ... def my_test():
        ...     print(luigi.configuration.get_config().get("foo", "bar"))
        ...     print(luigi.configuration.get_config().get("hoo", "bar"))
        ...
        >>> my_test()
        baz
        buz
        >>> @with_config({'foo': {'bar': 'buz'}})
        ... @with_config({'foo': {'bar': 'baz'}})
        ... def my_test():
        ...     print(luigi.configuration.get_config().get("foo", "bar"))
        ...
        >>> my_test()
        baz
        >>> @with_config({'foo': {'bur': 'buz'}})
        ... @with_config({'foo': {'bar': 'baz'}})
        ... def my_test():
        ...     print(luigi.configuration.get_config().get("foo", "bar"))
        ...     print(luigi.configuration.get_config().get("foo", "bur"))
        ...
        >>> my_test()
        baz
        buz
        >>> @with_config({'foo': {'bur': 'buz'}})
        ... @with_config({'foo': {'bar': 'baz'}}, replace_sections=True)
        ... def my_test():
        ...     print(luigi.configuration.get_config().get("foo", "bar"))
        ...     print(luigi.configuration.get_config().get("foo", "bur", "no_bur"))
        ...
        >>> my_test()
        baz
        no_bur
    """

    def __init__(self, config, replace_sections=False):
        self.config = config
        self.replace_sections = replace_sections

    def _make_dict(self, old_dict):
        if self.replace_sections:
            old_dict.update(self.config)
            return old_dict

        def get_section(sec):
            old_sec = old_dict.get(sec, {})
            new_sec = self.config.get(sec, {})
            old_sec.update(new_sec)
            return old_sec

        all_sections = itertools.chain(old_dict.keys(), self.config.keys())
        return {sec: get_section(sec) for sec in all_sections}

    def __enter__(self):
        self.enable()
        return self

    def __exit__(self, *exc):
        self.disable()

    def enable(self):
        import luigi.configuration

        self.orig_conf = luigi.configuration.LuigiConfigParser.instance()
        new_conf = luigi.configuration.LuigiConfigParser()
        luigi.configuration.LuigiConfigParser._instance = new_conf
        orig_dict = {
            k: dict(self.orig_conf.items(k)) for k in self.orig_conf.sections()
        }
        new_dict = self._make_dict(orig_dict)
        for section, settings in new_dict.items():
            new_conf.add_section(section)
            for name, value in settings.items():
                new_conf.set(
                    section,
                    name,
                    value.replace("%", "%%") if isinstance(value, str) else value,
                )

    def disable(self):
        luigi.configuration.LuigiConfigParser._instance = self.orig_conf

    def __call__(self, func_or_class):
        if not isinstance(func_or_class, type):
            # Just decorate a single method
            @functools.wraps(func_or_class)
            def wrapper(*args, **kwargs):
                self.enable()
                try:
                    return func_or_class(*args, **kwargs)
                finally:
                    self.disable()

            return wrapper

        if issubclass(func_or_class, unittest.TestCase):
            # TestCase subclass, so apply to all test methods via setupClass and tearDownClass
            original_setUpClass = func_or_class.setUpClass
            original_tearDownClass = func_or_class.tearDownClass

            def setUpClass():
                self.enable()
                try:
                    original_setUpClass()
                except Exception:
                    self.disable()
                    raise

            def tearDownClass():
                original_tearDownClass()
                self.disable()

            func_or_class.setUpClass = setUpClass
            func_or_class.tearDownClass = tearDownClass
            return func_or_class

        raise TypeError("Can only decorate subclasses of unittest.TestCase")


def get_config_value(
    section: str, key: Optional[str] = None, include_global_defaults=False
):
    """
    Retrieve configuration for a section or individual key

    Based on luigi.contrib.s3.S3Client._get_s3_config and typically used to retrieve
    configuration from the global Luigi configuration that can be used to instantiate
    an upstream component, such as a connection to AWS or Google Drive.
    """
    try:
        config = dict(luigi.configuration.get_config().items(section))
    except (NoSectionError, KeyError):
        return {}
    # Return things that are probably integers, such as ports, as integers
    for k, v in config.items():
        try:
            config[k] = int(v)
        except ValueError:
            pass

    # Single key requested so return it
    if key:
        return config.get(key)

    # No key specified, and global defaults allowed, so return the whole section
    if include_global_defaults:
        return config

    # If the section will be used to configure an upstream library, then we normally need to ignore
    # defaults that apply to all sections (e.g. those in the [DEFAULT] section of a ini file) because
    # the upstream library will complain about unrecognized parameters
    defaults = dict(luigi.configuration.get_config().defaults())
    config = {k: v for k, v in config.items() if k not in defaults or v != defaults[k]}
    return config


@contextmanager
def complete_config():
    """
    Create a temporary Luigi Config that has defaults for all variables
    """

    def get_default(param_obj):
        if isinstance(param_obj, luigi.IntParameter):
            return "1"
        elif isinstance(param_obj, luigi.DateParameter):
            return datetime.date.today().isoformat()
        elif isinstance(param_obj, luigi.Parameter):
            return ""

    config = luigi.configuration.get_config()

    # Make sure every parameter has a default value
    sections_to_remove = []
    options_to_remove = []
    for (
        task_name,
        is_without_section,
        param_name,
        param_obj,
    ) in Register.get_all_params():
        if param_obj._default == _no_value:
            if is_without_section:
                sections_to_remove.append(task_name)
            options_to_remove.append((task_name, param_name))
            config.set(task_name, param_name, get_default(param_obj))

    # Yield the completed config
    yield

    # Remove any config we set up
    for section, option in options_to_remove:
        config.remove_option(section, option)
    for section in sections_to_remove:
        config.remove_section(section)


def print_dag(task, level=0):
    """
    Print the DAG for a Task
    """

    def print_task(task, level):
        padding = " " * 8 * level
        print(padding + str(task))
        params = task.get_params()
        values = task.get_param_values(params, [], {})
        if params:
            print(padding + "    Parameters:")

        for name, value in values:
            print(padding + "        " + name + ": " + (str(value) or "''"))

        deps = task.requires()
        if deps:
            print(padding + "    Requires:")
        if isinstance(deps, luigi.Task):
            print_dag(deps, level + 1)
        elif isinstance(deps, dict):
            for task in deps.values():
                print_dag(task, level + 1)
        elif isinstance(deps, (list, tuple)):
            for task in deps:
                print_dag(task, level + 1)
        else:
            # Remaining case: assume struct is iterable...
            try:
                for task in deps:
                    print_dag(task, level + 1)
            except TypeError:
                raise Exception("Cannot determine dependencies for %s" % str(deps))

    if level == 0:
        with complete_config():
            if inspect.isclass(task):
                task = task()
            print_task(task, level)
    else:
        print_task(task, level)


def remove_outputs(task, cascade=False):
    """Remove the outputs from a Task, and possibly its parents."""

    # Don't remove outputs from External Tasks
    if not isinstance(task, luigi.ExternalTask):
        for target in flatten(task.output()):
            if target.exists():
                logger.info(
                    "Removing Target %s from Task %s" % (str(target), str(task))
                )
                target.remove()

        # Remove any upstream dependencies too
        if cascade:
            for task in flatten(task.requires()):
                remove_outputs(task, cascade)


class LocalHistoryWorkerSchedulerFactory:
    """
    Custom factory that enables task history for the local scheduler too
    """

    def create_local_scheduler(self):
        try:
            record_task_history = bool(CONFIG.get("task_history", "db_connection"))
        except configparser.NoSectionError:
            record_task_history = False

        return scheduler.Scheduler(
            prune_on_get_work=True, record_task_history=record_task_history
        )

    def create_remote_scheduler(self, url):
        return rpc.RemoteScheduler(url)

    def create_worker(self, scheduler, worker_processes, assistant=False):
        return worker.Worker(
            scheduler=scheduler, worker_processes=worker_processes, assistant=assistant
        )


def submit_task(task: luigi.Task, force: bool = False, cascade: bool = False, **params):
    """
    Run a Luigi Task using a flexible interface and return the result.

    task: A luigi.Task instance or a luigi.Task subclass or a str containing
    the name of the Task class, e.g. mypackage.mymodule.MyTask
    force: Force the Task to run, even if the Luigi Scheduler thinks it is already complete, by removing its
    outputs if they exist
    cascade: If force=True, then also force upstream Tasks to run by removing their outputs too. This results in
    a complete pipeline run.
    params: Dict of parameters for run as {param_name: deserialized_param_value}
    """
    logger.debug(f'submit_task called with params: "{params}" for task "{task}"')

    # Load the class if necessary, which ensures that the Tasks in the DAG are in the Task Registry
    if isinstance(task, str):
        task = class_from_name(task)

    # We can't use any of the higher level command line interfaces (e.g. luigi_run, build, etc.)
    # because we want to mix parameters for the task with parameters for upstream tasks and core
    # parameters (workers, local_scheduler, etc.). luigi.build expects already configured Task instances,
    # and doesn't support setting parameters for upstream tasks

    # Build a luigi command line from the task and parameters
    argv = build_argv_for_task(task, params)
    logger.debug(f"Running Luigi Task {' '.join(argv)}")
    start = datetime.datetime.now(datetime.timezone.utc)
    with CmdlineParser.global_instance(argv, allow_override=True) as cp:
        if not isinstance(task, luigi.Task):
            task = cp.get_task_obj()
        # Remove the output for re-triggering tasks when force is passed as True
        if force:
            remove_outputs(task, cascade=cascade)
        # Use a custom Worker Scheduler Factory so we can store Task History from the Local Scheduler too
        luigi_run_result = _schedule_and_run(
            [task], worker_scheduler_factory=LocalHistoryWorkerSchedulerFactory()
        )

    result = {
        "worker": luigi_run_result.worker,
        "summary_text": luigi_run_result.summary_text,
        "status": luigi_run_result.status,
        "one_line_summary": luigi_run_result.one_line_summary,
        "scheduling_succeeded": luigi_run_result.scheduling_succeeded,
        "command": " ".join(argv),
        "start": start,
        "end": datetime.datetime.now(datetime.timezone.utc),
        "elapsed": datetime.datetime.now(datetime.timezone.utc) - start,
        # Include the task_id of the called task, so that we check against the Task History later
        # to see the final status, because if there are multiple workers, then this task might return
        # from the worker it is scheduled on while there are still other workers running upstream tasks
        # from the DAG.
        "task_id": task.task_id,
        "output": task.output(),
    }

    logger.info(
        f"Finished Luigi Task {result['task_id']} with status '{result['one_line_summary']}' "
        f"for command '{result['command']}'"
    )

    return result


def submit_task_from_celery(task, **params):
    """
    Submit a Luigi task using the supplied parameters and return the result.

    Params:
    * task: a configured Luigi Task instance initialized with the correct parameters

    Returns a JSON-serializable version of a LuigiRunResult
    """
    # Use a single worker unless otherwise specified, so that we avoid multi-processing,
    # which means that the task inherits the current logging configuration. When using
    # multi-processing each task runs in a new process, which starts with a new default
    # logging setup, and this makes it harder to collect all the logging output for a
    # pipeline run in a single file.
    defaults = {
        "workers": 1,
    }
    params = {**defaults, **params}

    # Collect the log output from all the tasks (assuming we are running in a single Worker)
    with local_logging() as buffer:
        result = submit_task(task, **params)

    # If the Luigi task was not successful then raise an exception so that the calling Celery task is also
    # marked as a Failure.
    if result["status"] not in [
        LuigiStatusCode.SUCCESS,
        LuigiStatusCode.SUCCESS_WITH_RETRY,
    ]:
        message = [
            f'{task} failed with status {result["status"]}\n'
        ] + buffer.readlines()
        raise PipelineError("".join(message))

    # Include the collected log output in the result
    result["log"] = buffer.read()

    # We can't include unserializable objects in the Celery result
    result["elapsed"] = str(result["elapsed"])
    result["status"] = result["status"].name
    del result["output"]
    del result["worker"]

    return result


def run(task, force=False, cascade=False, **params):
    """Run a Task using a flexible interface and return the output Target(s)"""
    return submit_task(task, force, cascade, **params)["output"]


def patch_parameter_get_value(original_method):
    """Takes the _get_value method on Parameter and adds a default if no value is found."""

    def _patched_parameter_get_value(self, task_name, param_name):
        with contextlib.suppress(ValueError):
            value = original_method(self, task_name, param_name)
            if value and value != _no_value:
                return value

        if isinstance(self, IntParameter):
            return int()
        if isinstance(self, BoolParameter):
            return bool()
        if isinstance(self, FloatParameter):
            return float()
        if isinstance(self, DateIntervalParameter):
            return DateInterval(datetime.datetime.utcnow(), datetime.datetime.utcnow())
        if isinstance(self, TimeDeltaParameter):
            return "1 week 2 days"
        if isinstance(self, TaskParameter):
            return kiluigi.Task
        if isinstance(self, EnumParameter):
            return self._enum(1)
        if isinstance(self, DictParameter):
            return {}
        if isinstance(self, ListParameter):
            return []
        if isinstance(self, TupleParameter):
            return ()
        if isinstance(self, NumericalParameter):
            return self._min_value
        if isinstance(self, ChoiceParameter):
            return next(iter(self._choices))  # get random element from choices set
        if isinstance(self, DateParameter):
            return datetime.date(2012, 5, 10)
        if isinstance(self, YearParameter):
            return datetime.date(2012, 2, 1)
        if isinstance(self, GeoParameter):
            return "<?xml version='1.0' encoding='UTF-8'?><kml><Document><Folder><name>A Folder</name><Placemark><name>A Polygon</name><Polygon><outerBoundaryIs><LinearRing><coordinates>-111.68457224965096,40.25116680553771 -111.6846151649952,40.2490377737844 -111.67594626545906,40.24900501892662 -111.67581751942635,40.25237868600809 -111.68332770466805,40.25237868600809 -111.68332770466805,40.25237868600809 </coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark></Document></kml>"  # NOQA

        return "%s.%s" % (task_name, param_name)

    return _patched_parameter_get_value


def patch_config_get_with_default(original_method):
    """Takes the _get_with_default method on config and adds a default if no value is found."""

    def _patched_config_get_with_default(
        method, section, option, default, expected_type=None, **kwargs
    ):
        with contextlib.suppress(
            configparser.NoOptionError, configparser.NoSectionError
        ):
            val = original_method(
                method, section, option, default, expected_type, **kwargs
            )
            if val:
                return val
        if expected_type is datetime:
            return datetime.datetime.utcnow()
        if expected_type is str or expected_type is None:
            return "%s.%s" % (section, option)
        return expected_type()

    return _patched_config_get_with_default


class MockConfig:
    # @TODO Refactor this into kiluigi.Register?

    def __init__(self):
        self.config = luigi.configuration.LuigiConfigParser.instance()
        self.original_config_get_with_default = self.config._get_with_default
        self.original_parameter_get_value = Parameter._get_value

    def __enter__(self):
        self.apply_patch()

    def apply_patch(self):
        # config._get_with_default patched for `config.get(section, option)` calls
        self.config._get_with_default = patch_config_get_with_default(
            self.original_config_get_with_default
        )
        # Parameter._get_value patch, for typed `x = <Type>Parameter()` settings
        # luigi.Parameter subclasses don't set the _expected_type so we patch _get_value instead
        Parameter._get_value = patch_parameter_get_value(
            self.original_parameter_get_value
        )

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.remove_patch()

    def remove_patch(self):
        self.config._get_with_default = self.original_config_get_with_default
        Parameter._get_value = self.original_parameter_get_value


def get_all_outputs(task):
    """Lists all output targets by recursively walking all tasks."""
    outputs = flatten(task.output())
    for dep in flatten(task.requires()):
        outputs += get_all_outputs(dep)
    return outputs


def get_flag_value(param_obj, param_value):
    """This will parse the value from Frontend and serialize it for Luigi cli.

    The parsed value will be compared to `param_obj._default`, and discarded if
    equal, to keep the Luigi cli simpler.
    """
    if isinstance(param_obj, luigi.DateParameter):
        # we do not need to serialize this it's already a string representing a date,
        # when retrieved from the browser (e.g. `2019-01-31`)
        if param_obj._default == param_value:
            return None
        return f"={param_value}"
    if isinstance(param_obj, luigi.BoolParameter):
        # we have the BoolParameter in params, that means the checkbox was checked.
        # We can't have a value here, or Luigi will throw an error like:
        # `--adjust-for-slope: ignored explicit argument 'true'`
        # (luigi.BoolParameter.IMPLICIT_PARSING didn't seem to work correctly)
        # so we only use the key, without the equals sign

        # @TODO: check if BoolParameter with default=True still get used by Luigi
        # when they aren't passed to cli
        if param_obj._default is True:
            return None
        return ""
    # none of the above
    # we try to serialize the value as appropriate for the parameter type
    # Since the data submitted from the browser (formData)  might be something like
    # `"['1','2','3']"` ), we parse it before serializing it again, otherwise
    # the param_obj might not accept the value. For example the above list wouldn't
    # work with param_obj.parse, because parse is using `json.loads`.
    # ast.literal_eval works
    try:
        parsed_value = literal_eval(param_value)
    except (ValueError, SyntaxError):
        parsed_value = param_value
        logger.debug(
            '''literal_eval couldn't parse %s, type: "%s"''',
            param_value,
            type(param_value),
        )
    if param_obj._default == parsed_value:
        return None
    return f"={param_obj.serialize(parsed_value)}"


def build_argv_for_task(task, params):
    """
    Create a command Luigi command line from a Task and a dict of deserialized parameters
    """
    # If task is an already instantiated task, then include its parameters in the command line
    if isinstance(task, luigi.Task):
        for param in task.param_kwargs:
            if param not in params:
                params[param] = task.param_kwargs[param]

    # Create the command line arguments for Luigi, starting with the root Task
    argv = [task.get_task_family()]

    # As with `luigi.interface.build()` we need to disable locking as
    # we will be calling build repeatedly from within the same process
    if "no_lock" not in params:
        argv.append("--no-lock")

    # Turn Python parameters into string parameters suitable for Luigi's CmdlineParser
    # See luigi.cmdline_parser.CmdlineParser._build_parser() for the logic we are inverting.
    # Use a temporary command line parser so that we don't create a CmdlineParser
    # singleton instance before we call _run
    with CmdlineParser.global_instance(argv, allow_override=True):
        # Loop over all the parameters for all of the registered Tasks
        # (including global parameters like `workers` and `retry_delay`)
        for (
            task_name,
            is_without_section,
            param_name,
            param_obj,
        ) in Register.get_all_params():
            # There are basically 3 cases we need to handle:
            #  1) is_without_section is True:
            #    e.g. `task_name=core`, `param_name=local_scheduler`
            #  2) is_without_section is False, parameter is global:
            #    e.g. `task_name=ssl`, `param_name=timeout` => `--ssl_timout=300`
            #  3) parameter is for current task (is_without_section will be False
            #     though, for Luigi the parameter belongs to a task):
            #    e.g. `task_name=models.[...].TerminalTask`, `param_name=timeout`
            #    We can use `--timeout=300` without prepending the task, because
            #    we can assume that all parameters have been inherited by the
            #    task that was triggered from its upstreams.
            #    Without mandatory inheritance, this assumption wouldn't be valid
            #    But for now the frontend only provides params without prefix

            # Add the section/task name to the parameter name if appropriate:
            # Parameters that don't have `is_without_section==True` need to have
            # their "task_name" as prefix (might be something like `ssl`), unless
            # they belong to the task that was triggered.
            # normalized_param_name will take care of that
            current_task_family = task.get_task_family()
            normalized_param_name = (
                param_name
                if (is_without_section or task_name == current_task_family)
                else task_name + "_" + param_name
            )

            flag_name = "--" + normalized_param_name.replace("_", "-")

            # flag_name_underscore will either be param_name or "task"_param_name
            # (task could also be a key like "ssl")
            if normalized_param_name in params:
                # We have an argument passed in that matches a valid parameter,
                # so format it as a Luigi command line parameter
                # (--task-param - e.g. --ssl-timeout or --param e.g. --timeout)
                # and add it to the list of command line parameters
                # get_flag_value will prepend the `=` to the value if appropriate
                logger.debug('normalized_param_name: "%s', normalized_param_name)
                param_value = params.pop(normalized_param_name)
                flag_value = get_flag_value(param_obj, param_value)
                if flag_value is not None:
                    argv.append(flag_name + flag_value)
                else:
                    logger.debug(
                        'Skipping flag "%s" because value "%s" is the same as param_obj._default',
                        flag_name,
                        param_value,
                    )

    # Make sure that we consumed all of the passed in arguments
    assert not params, f"Unrecognized parameter(s): {params}"
    return argv


# @TODO: Think about if it's OK to use task_family here, it might not work if
# luigi.auto_namespace isn't used everywhere, or does it still work?
def get_luigi_task_id(argparse_argv):
    """We need to return a Luigi task_id after triggering the task.

    The Luigi task_id is built from the task name, the first 3 arguments and
    a hash of all arguments. To be able to get it, we need to initialize a task
    class, that we pass all the values for the parameters the task is expecting.

    argparse_argv might look like this:
    [
        "tests.integration.linear.tasks.Dest2",
        "--no-lock",
        "--worker-timeout=120",
        "--workers=0",
        "--src-num=6",
        "--mid-num=2",
    ]

    In contrast to the command line interface that takes serialized values for the
    parameters, the task_class needs to be passed the correct types for each parameter,
    e.g. int("23") for IntParameter. We can use param_obj.parse, but we need to know
    what type a certain parameter is of. We can look that up using
    Register.get_all_params

    @TODO it is not clear whether this command is required, or whether we can just store the task_id returned from
    submit_task.
    It may be that it is required by Valve (a REST API for interacting with KiLuigi that is now a separate project).
    """
    task_family = argparse_argv.pop(0)
    arg_name_value_dict = {}
    for arg in argparse_argv:
        # have to use for loop here because some parameters have no values,
        # they are BoolParameter with value True
        stripped = arg.lstrip("-")
        param_name = stripped.split("=")[0]
        # reverting what was done before to prepare param_names for the cli
        param_name = param_name.replace("-", "_")
        try:
            arg_value = stripped.split("=")[1]
        except IndexError:
            arg_value = True
        arg_name_value_dict[param_name] = arg_value

    # @TODO: See if it makes sense to refactor all usages of Register.get_all_params,
    # and possibly common code with submit_task

    # Register.get_all_params returns tuples with 4 elements:
    # (task_name, _is_without_section, param_name, param_obj)
    luigi_param_name_type_mapping = {
        luigi_param[2]: luigi_param[3]
        for luigi_param in Register.get_all_params()
        # we are only interested in parameters that belong to the current task
        if luigi_param[0] == task_family
    }
    deserialized_params = {}
    for param_name, param_obj in luigi_param_name_type_mapping.items():
        # we only consider params from argparse_argv that belong to the current task.
        # Not all possible params for this Task might have been provided when calling
        # the task. BoolParameters that are False are left out, parameters with a
        # default might not exist, so we might have more params in
        # Register.get_all_params than in arg_name_value_dict
        if param_name in arg_name_value_dict:
            param_value = arg_name_value_dict[param_name]
            deserialized_param_value = param_obj.parse(param_value)
            deserialized_params[param_name] = deserialized_param_value

    task_class = class_from_name(task_family)
    task = task_class(**deserialized_params)
    luigi_task_id = task.task_id
    logger.info('Luigi task_id for "%s": "%s"', task_family, luigi_task_id)
    return luigi_task_id
