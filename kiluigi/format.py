"""
Custom Formats for reading and writing to Luigi Targets.
"""
import io
import pickle  # NOQA: S403

import pandas as pd
import pyarrow as pa
import pyarrow.feather as pf
import pyarrow.parquet as pq
from luigi.format import BaseWrapper, WrappedFormat
from pyarrow.lib import ArrowInvalid


class PickleWrapper(BaseWrapper):
    def __init__(self, stream, protocol=4):
        self.protocol = protocol
        super().__init__(stream)

    def read(self, size=None):
        b = self._stream.read(size)

        return pickle.loads(b)  # NOQA: S301

    def write(self, b):
        self._stream.write(pickle.dumps(b, protocol=self.protocol))


class PickleFormat(WrappedFormat):
    input = "python"  # noqa: A003
    output = "bytes"
    wrapper_cls = PickleWrapper


class FeatherWrapper(BaseWrapper):
    def read(self):
        return pf.read_feather(self._stream)

    def write(self, b):
        pf.write_feather(b, self._stream)


class FeatherFormat(WrappedFormat):
    """
    Stores a Pandas DataFrame to a file using the Feather file format.

    It is an exact representation of the Arrow IPC file format on disk.

    It supports most DataFrame functionality, and offers very fast serialization.
    It is a good choice for passing very large DataFrames between Tasks, where the Target
    doesn't need to be opened by external programs or processes.

    Note that it can fail if the dataframe contains columns with the `object` dtype
    which contain a mixture of strings and integers. Either cast the values to `str`,
    or use PickleFormat as a safer but slower alternative.  Any performance advantage
    that FeatherFormat has over PickleFormat will only be apparent with very large
    dataframes.
    """

    input = "dataframe"  # noqa: A003
    output = "bytes"
    wrapper_cls = FeatherWrapper


class ParquetWrapper(BaseWrapper):
    def read(self):
        try:
            return pq.read_pandas(self._stream).to_pandas()
        except AttributeError:
            # Parquet may need a file-like object that supports tell() and seek(), and some Targets may not provide
            # them, so wrap in BytesIO. This requires reading the whole file into memory, so will prevent fetching
            # subsets of columns, etc. to save memory, but the Format read() interface doesn't support those things
            # anyway. Pipelines can always read the target directly if required.
            buffer = io.BytesIO(self._stream.read())
            try:
                return pq.read_pandas(buffer).to_pandas()
            except ArrowInvalid:
                # If we just have an empty dataframe, then we don't have a problem
                df = pd.read_parquet(buffer)
                if not df.empty:
                    raise
                return df

    def write(self, b):
        table = pa.Table.from_pandas(b, preserve_index=True)
        pq.write_table(table, self._stream)


class ParquetFormat(WrappedFormat):
    """
    Stores a Pandas DataFrame to a file using the Parquet file format.

    Parquet is an open source file format built to handle flat columnar storage data formats.
    Parquet operates well with complex data in large volumes.It is known for its both performant
    data compression and its ability to handle a wide variety of encoding types.

    It supports most DataFrame functionality, and offers very fast serialization.
    It is a good choice for passing very large DataFrames between Tasks, where the Target
    doesn't need to be opened by external programs or processes.

    Parquet files will typically be about 5 times smaller than a Pickle of the same DataFrame.
    """

    input = "dataframe"  # noqa: A003
    output = "bytes"
    wrapper_cls = ParquetWrapper


Parquet = ParquetFormat()
Pickle = PickleFormat()
Feather = FeatherFormat()
