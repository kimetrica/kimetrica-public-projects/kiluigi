import logging
import os
import pickle  # NOQA: S403
from distutils.version import StrictVersion
from io import BytesIO, StringIO
from typing import BinaryIO, Dict, List, Union

import geopandas as gpd
import luigi
import numpy as np
import pandas as pd
from luigi import BoolParameter
from xlrd import open_workbook
from xlrd.biffh import XLRDError

import kiluigi

from .mixins import TagMixin
from .targets import IntermediateTarget, RESTTarget

logger = logging.getLogger("luigi-interface")


class Task(TagMixin, luigi.Task):
    """
    A KiLuigi task.
    Placeholder Kimetrica sub-class for additional functionality, eg, unit tests.
    """

    @property
    def description(self):
        return type(self).__name__


class IndicatorTask(Task):
    """
    A KiLuigi task that processes an Indicator.
    Placeholder Kimetrica sub-class for additional functionality, eg, docstring tests.
    """

    pass


class ExternalTask(Task, luigi.ExternalTask):
    """
    A KiLuigi ExternalTask (i.e., a task with no run() method that pulls its outputs from elsewhere).
    """

    pass


class WrapperTask(Task, luigi.WrapperTask):
    """
    A KiLuigi WrapperTask (i.e., a task with no run() method that just triggers other tasks).
    """

    pass


class RESTExternalTask(ExternalTask):
    """
    Fetch data from an external server using a REST API
    """

    server = luigi.Parameter()
    auth = luigi.Parameter(default="")
    path = luigi.Parameter()
    params = luigi.DictParameter(default={})

    def output(self):
        return RESTTarget(self.path, self.params, server=self.server, auth=self.auth)


class ReadDataFrameTask(Task):
    """
    A Pipeline Task that reads one or more Pandas DataFrames from file(s) and returns them as an IntermediateTarget
    """

    extensions = {
        ".xls": "read_excel",
        ".xlsx": "read_excel",
        ".csv": "read_csv",
        ".h5": "read_hdf",
        ".json": "read_json",
    }

    timeout = luigi.IntParameter(default=3600)
    read_method = luigi.Parameter(default="")
    read_args = luigi.DictParameter(default={})

    def requires(self):
        return NotImplementedError(
            "Concrete subclasses must specify one or more Targets "
            "returning files readable by Pandas"
        )

    def output(self):
        return IntermediateTarget(task=self, timeout=self.timeout)

    def get_read_method(self, filename):
        if self.read_method:
            return self.read_method
        extension = os.path.splitext(filename)[1]
        if extension in self.extensions:
            return self.extensions[extension]
        raise ValueError("Cannot determine read method for file %s" % filename)

    def get_read_args(self):
        return self.read_args

    def read_dataframe(self, path_or_buffer, read_method):

        read_args = self.get_read_args()

        # Pandas < 1.2.0 defaults to using xlrd, but recent versions of xlrd don't support xlsx files,
        # so force the use of openpyxl if necessary
        if read_method.__name__ == "read_excel" and StrictVersion(
            pd.__version__
        ) < StrictVersion("1.2.0"):
            try:
                if not isinstance(
                    path_or_buffer, (str, bytes, os.PathLike)
                ) and hasattr(path_or_buffer, "read"):
                    # If the target isn't a file or buffer, but can be read, then create a buffer
                    path_or_buffer = path_or_buffer.read()
                    wb = open_workbook(file_contents=path_or_buffer, logfile=logger)
                    path_or_buffer = BytesIO(path_or_buffer)
                else:
                    try:
                        wb = open_workbook(path_or_buffer, logfile=logger)
                    except ValueError:
                        # We have been passed a str or bytes object containing the actual
                        # file, rather than a filename.
                        wb = open_workbook(file_contents=path_or_buffer, logfile=logger)
                        path_or_buffer = BytesIO(path_or_buffer)
                logger.debug(
                    "Workbook %s contains sheets %s"
                    % (path_or_buffer, wb.sheet_names())
                )
            except XLRDError:
                # xlrd can't open the file, probably because it is an xlsx, so use openpyxl instead.
                read_args = {
                    k: v for k, v in read_args.items()
                }  # read_args will be frozen, so copy it
                read_args["engine"] = "openpyxl"

        try:
            df = read_method(path_or_buffer, **read_args)
        except FileNotFoundError:
            # path_or_buffer was a string, but not a filename
            path_or_buffer = StringIO(path_or_buffer)
            df = read_method(path_or_buffer, **read_args)
        except IOError:
            # path_or_buffer was a bytes object
            path_or_buffer = BytesIO(path_or_buffer)
            df = read_method(path_or_buffer, **read_args)
        logger.debug(f"Successfully read dataframe with shape {df.shape}")
        return df

    def read_target(self, target):
        read_method = None
        if hasattr(target, "get"):
            path_or_buffer = target.get()
            # Pandas >= 1.4.0 requires a file-like object, rather than actual content, so wrap the content
            # in StringIO or BytesIO as appropriate. An additional check for str objects containing filenames
            # is required because CkanTarget.get() returns the path to a local copy of the target content
            # rather than the target content itself.
            if isinstance(path_or_buffer, str) and not os.path.exists(path_or_buffer):
                path_or_buffer = StringIO(path_or_buffer)
            if isinstance(path_or_buffer, bytes):
                path_or_buffer = BytesIO(path_or_buffer)
        elif hasattr(target, "open"):
            with target.open("r") as f:
                if hasattr(f, "name"):
                    path_or_buffer = f.name
                    read_method = getattr(pd, self.get_read_method(path_or_buffer))
                elif hasattr(f, "read"):
                    # Pandas >= 1.4.0 requires a file-like object, but older versions of Pandas will attempt to `seek`
                    # to the beginning of the file if passed a file-like object, even if the file returns
                    # `seekable() = False`, which raises an Exception when using S3Target and passing Pandas the file
                    # handle directly. Therefore, read the content from the Target and wrap it in a StringIO or
                    # BytesIO as appropriate.
                    path_or_buffer = f.read()
                    if isinstance(path_or_buffer, str):
                        path_or_buffer = StringIO(path_or_buffer)
                    if isinstance(path_or_buffer, bytes):
                        path_or_buffer = BytesIO(path_or_buffer)
                else:
                    path_or_buffer = target
        else:
            raise ValueError("No get or open method on target %s" % target)
        if not read_method:
            # If we have a file-like object then we can't guess the read_method and it must be specified explicitly
            assert (
                self.read_method
            ), "ReadDataFrame requires an explicit read_method unless there is a local file with a recognized extension"  # NOQA: E501
            read_method = getattr(pd, self.read_method)
        return self.read_dataframe(path_or_buffer, read_method)

    def run(self):
        targets = self.input()
        if isinstance(targets, luigi.Target):
            with self.output().open("w") as f:
                f.write(self.read_target(targets))
        elif isinstance(targets, dict):
            output = {key: self.read_target(target) for key, target in targets.items()}
            with self.output().open("w") as f:
                f.write(output)
        elif isinstance(targets, (list, tuple)):
            output = {
                os.path.basename(target.path): self.read_target(target)
                for target in targets
            }
            with self.output().open("w") as f:
                f.write(output)


class ReadGeoDataFrameTask(ReadDataFrameTask):
    """
    A Pipeline Task that reads one or more GeoPandas GeoDataFrames from file(s) and returns them as a MemoryTarget
    """

    extensions = None

    def read_dataframe(self, file_obj):
        with file_obj.open("r") as f:
            filename = f.name
            gdf = gpd.read_file(filename, **self.get_read_args())
            return gdf

    def run(self):
        targets = self.input()

        if isinstance(targets, luigi.Target):
            self.output().put(self.read_dataframe(targets))
        elif isinstance(targets, dict):
            output = {
                key: self.read_dataframe(target) for key, target in targets.items()
            }
            self.output().put(output)
        elif isinstance(targets, (list, tuple)):
            output = {
                os.path.basename(target.path): self.read_dataframe(target)
                for target in targets
            }
            self.output().put(output)


class DataFrameOutputTask(Task):
    """
    Base class for all dataframe outputs.

    Sets up the source DataFrame and an appropriate output file.

    Concrete subclasses must implement a create_output() method.
    """

    filename: str

    timeout: luigi.IntParameter = luigi.IntParameter(default=3600)

    def requires(self) -> Union[luigi.Target, List[luigi.Target]]:
        """
        Return a single Target (or a list of Targets) where each Target
        contains either a single DataFrame or a list or dict of DataFrames
        """
        raise NotImplementedError

    def output(self) -> luigi.Target:
        # Note that format=luigi.format.Nop is required for binary files on Python3
        # see https://github.com/spotify/luigi/issues/1647
        return IntermediateTarget(self.filename, timeout=self.timeout)

    def run(self):
        inputs = self.input()
        # Make sure we have a dict so we can iterate over items
        if isinstance(inputs, luigi.Target):
            inputs = {"Data": inputs}
        elif isinstance(inputs, list):
            inputs = {i: x for i, x in enumerate(inputs)}
        else:
            try:
                inputs = {x.name: x for x in inputs}
            except TypeError:
                inputs = {x.name: x for x in inputs.values()}
        dfs = {}
        for key, target in inputs.items():
            with target.open("r") as f:
                contents = f.read()
            if isinstance(contents, pd.DataFrame):
                dfs[key] = contents
            elif isinstance(contents, dict):
                dfs.update(contents)
            elif isinstance(contents, list):
                for seq, df in enumerate(contents):
                    dfs["%s (%d)" % (key, seq)] = df

        assert all(isinstance(df, pd.DataFrame) for df in dfs.values())
        with self.output().open(mode="w") as f:
            self.build_output(f, dfs)

    def build_output(self, f: BinaryIO, dfs: Dict[str, pd.DataFrame]):
        """
        Build the output from the provided dataframes and output it to the provided file
        """
        raise NotImplementedError


class DataFrameOutputToExcelTask(DataFrameOutputTask):

    index: bool = True  # Write DataFrame row names (index) as a column in the Worksheet

    # Use openpyxl as the default writer so we can support DataFrame.style
    # see https://pandas.pydata.org/pandas-docs/stable/style.html#Export-to-Excel
    engine: str = "openpyxl"

    def prepare_sheet(self, sheet_name, df):
        """
        Return a prepared DataFrame for saving to Excel, e.g. by applying styles
        """
        return df

    def build_output(self, f, dfs):
        with pd.ExcelWriter(f, engine=self.engine) as wb:
            for sheet_name, df in dfs.items():
                df = self.prepare_sheet(sheet_name, df)
                df.to_excel(wb, sheet_name=sheet_name, index=self.index)


class VariableInfluenceMixin:
    """
    A mixin that any task that calculates variable influence can inherit.
    These are not in VariableTask so training tasks can inherit too.
    """

    # TODO: Our @requires decorator will have to ignore this as it'll be added by every VariableInfluenceMixin
    #  instance in the task DAG.
    calculate_influence = BoolParameter(
        default=False,
        description="Set to True to also calculate the variable CAG influence grids.",
    )

    COL_INFLUENCE_PERCENT = "influence_percent"
    COL_SOURCE_VARIABLE = "source_variable"

    def store_influence_grid(self, influenced_variable_name, influence_grid):
        """Store the influence grid data for the named variable.
        This data will be persisted to disk during processing, and loaded from disk for diagramming. Plan is for the
        baseline to be run on deployment and once every 24 hours.
        :param variable_name: Name of variable
        :param influence_grid: The influence grid data for the named variable as a DataFrame, with columns for source
        variable name and influence percent.
        :return: None
        """
        target = self.get_influence_grid_target()
        influence_grid = self.fix_influence_grid(influence_grid)

        data = (influenced_variable_name, influence_grid)
        if target.exists():
            target.remove()
        if hasattr(target, "put"):
            target.put(data)
        else:
            with target.open(mode="wb") as f:
                pickle.dump(data, f)

    def fix_influence_grid(self, influence_grid):

        if not isinstance(influence_grid, pd.DataFrame):
            logger.warning(
                f"Influence grid for task {type(self).__name__} total influence is a {type(influence_grid).__name__} "
                f"not a DataFrame. Attempting fix."
            )
            influence_grid = pd.DataFrame(
                influence_grid,
                columns=[self.COL_SOURCE_VARIABLE, self.COL_INFLUENCE_PERCENT],
            )

        if (
            len(influence_grid.columns) != 2
            or self.COL_SOURCE_VARIABLE not in influence_grid.columns
            or self.COL_INFLUENCE_PERCENT not in influence_grid.columns
        ):
            logger.warning(
                f"Influence grid for task {type(self).__name__} contains columns {', '.join(influence_grid.columns)}. "
                f"It should contain columns {self.COL_SOURCE_VARIABLE} and {self.COL_INFLUENCE_PERCENT}. "
                "Attempting fix."
            )
            influence_grid.columns = [
                self.COL_SOURCE_VARIABLE,
                self.COL_INFLUENCE_PERCENT,
            ]

        if not np.issubdtype(
            influence_grid[self.COL_INFLUENCE_PERCENT].dtype, np.number
        ):
            logger.warning(
                f"Influence grid for task {type(self).__name__} column {self.COL_INFLUENCE_PERCENT} is not numeric. "
                f"Attempting fix."
            )
            influence_grid[self.COL_INFLUENCE_PERCENT] = pd.to_numeric(
                influence_grid[self.COL_INFLUENCE_PERCENT], errors="coerce"
            )

        influence_sum = influence_grid[self.COL_INFLUENCE_PERCENT].sum()
        if influence_sum < 0.99 or influence_sum > 1.01:
            logger.warning(
                f"Influence grid for task {type(self).__name__} total influence is {influence_sum} not 1. "
                f"Attempting fix."
            )
            factor = 1 / influence_sum
            influence_grid[self.COL_INFLUENCE_PERCENT] *= factor

        return influence_grid

    def get_influence_grid_target(self):
        """
        Override this method to directly specify the targets used to store influence grids for each variable_name.
        Otherwise, if the method influence_grid_source_task is implemented, this gets the target from the
        source task. Use this option where there is a training task that generates the influence grid, and this
        is the prediction VariableTask.
        Otherwise, it instantiates default targets for storing the influence grids.

        :return: The target that persists the influence grid for the given variable name.
        """
        return kiluigi.targets.SerializerTarget(
            name=f"influence_grid_{type(self).__name__}",
            timeout=60 * 60 * 24 * 30,
            # 30 days, although baseline probably run nightly
        )

    def get_influence_grid(self):
        """Get the influence grid data for the named variable as a DataFrame.
        :return: Influence grid as a DataFrame, with columns for source variable name and influence percent.
        """
        target = self.get_influence_grid_target()
        if not target.exists():
            # This call could be synchronous from the GUI thread, so don't want to trigger their processing then wait.
            logger.debug(
                f"Influence grid for {type(self).__name__} not found, so the vCAG omits {type(self).__name__}'s "
                f"linkages."
            )
            return None

        if hasattr(target, "get"):
            influenced_variable_name, influence_grid = target.get()
        else:
            with target.open(mode="rb") as f:
                influenced_variable_name, influence_grid = pickle.load(f)  # NOQA: S301

        influence_grid = self.fix_influence_grid(influence_grid)

        return influenced_variable_name, influence_grid


class VariableTask(Task):
    """A KiLuigi task that outputs one or more Variables, in fully cleaned, individual targets
    for client systems to download.
    """

    DATA_TARGET = "Data target"
    ONTOLOGY = "Ontology"
    SYNONYMS = "Synonyms"
    UNIT_OF_MEASURE = "Unit of measure"
    MINIMUM = "Minimum"
    MAXIMUM = "Maximum"
    FILE_FORMAT = "File format"
    SOURCE_TASK = "Source task"
    SOURCE_TASK_DOCUMENTATION = "Source task documentation"
    SOURCE_TASK_API_URL = "Source task API URL"
    RELATIVE_INFLUENCE_CALCULATION_TYPE = "Relative influence calculation type"
    RELATIVE_INFLUENCE_CALCULATION_DOCUMENTATION_URL = (
        "Relative influence calculation documentation URL"
    )
    DESCRIPTION = "Description"
    RESPONSIBLE_PARTY = "Responsible Party"
    INSTITUTION = "Institution"
    CONTACT = "Contact"
    MEASURE = "Measure"
    AREA = "Area"
    TIME_PERIOD = "Time period"
    KEYWORDS = "Keywords"
    PROJECTION = "Projection"
    RESOLUTION = "Resolution"
    USAGE_LIMITATIONS = "Usage limitations"
    NOTES = "Notes"
    SOURCE = "Source"
    DOWNLOAD_URL = "Download URL"
    CREATED = "Created"
    LAST_UPDATED = "Last updated"
    LICENSE = "License"

    def get_variable_metadata(self):
        # A dict with variable name as the key. This allows for multi-variable VariableTasks should the analyst prefer.
        # E.g. return {'variable_name': {self.DATA_TARGET: self.output().['my_output_key'],
        #                                self.MEASURE: 'Kilograms'}}
        return None
